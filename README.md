# FinlayDaG33k's Website
![Pipeline Satus](https://gitlab.com/FinlayDaG33k/Website/badges/master/pipeline.svg)

Welcome to the repo of my personal website :)  
Even though it's already [live](https://www.finlaydag33k.nl), it is still a massive work in progress.  
Feel free to have a look around and help me improve it where you can