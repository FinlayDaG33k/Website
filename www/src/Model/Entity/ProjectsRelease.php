<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * ProjectsRelease Entity
 *
 * @property int $id
 * @property int $project_id
 * @property string $version
 * @property \Cake\I18n\FrozenTime $date
 * @property int $published
 *
 * @property \App\Model\Entity\Project $project
 */
class ProjectsRelease extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'project_id' => true,
        'version' => true,
        'description' => true,
        'date' => true,
        'published' => true,
        'project' => true,
        'projects_changelogs' => true
    ];
}
