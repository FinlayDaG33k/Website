<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * ProjectsChangelogs Model
 *
 * @property \App\Model\Table\ProjectsReleasesTable|\Cake\ORM\Association\BelongsTo $ProjectsReleases
 *
 * @method \App\Model\Entity\ProjectsChangelog get($primaryKey, $options = [])
 * @method \App\Model\Entity\ProjectsChangelog newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\ProjectsChangelog[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\ProjectsChangelog|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\ProjectsChangelog saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\ProjectsChangelog patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\ProjectsChangelog[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\ProjectsChangelog findOrCreate($search, callable $callback = null, $options = [])
 */
class ProjectsChangelogsTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('projects_changelogs');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->belongsTo('ProjectsReleases', [
            'foreignKey' => 'release_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', 'create');

        $validator
            ->requirePresence('change', 'create')
            ->allowEmptyString('change', false);

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['release_id'], 'ProjectsReleases'));

        return $rules;
    }
}
