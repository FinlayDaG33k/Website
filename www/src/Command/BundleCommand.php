<?php
namespace App\Command;

use Cake\Filesystem\File;
use Cake\Console\Arguments;
use Cake\Console\Command;
use Cake\Console\ConsoleIo;
use Symfony\Component\Yaml\Yaml;
use Cake\Core\Exception\MissingPluginException;
use Cake\Core\Plugin;
use MatthiasMullie\Minify;

class BundleCommand extends Command {
  private $config;
  private ConsoleIo $io;

  public function execute(Arguments $args, ConsoleIo $io) {
    $this->io = $io;

    // Load our YAML file
    // Then parse it
    $io->out('Loading YAML...');
    $yaml = new File(CONFIG . 'bundles.yaml', false);
    if(!$yaml->exists()) throw new \Exception('YAML as "' . CONFIG . '/bundles.yaml" could not be found!');
    $this->config = Yaml::parse($yaml->read());

    // Loop over each bundle to create
    foreach($this->config as $name => $data) {
      $io->out('Creating bundle for "' . $name . '" with ' . count($data['sources']) . ' assets...');
      $this->createBundle($name, $data);
    }
    $io->out('Bundling complete!');
  }

  public function createBundle(string $name, array $data) {
    // Create a variable to hold the bundle content
    $content = '';

    // Loop over the assets, adding them to out bundle
    foreach($data['sources'] as $asset) {
      $this->io->out('Adding "' . $asset['source']. '" from "'.(!empty($asset['plugin']) ? $asset['plugin'] : 'app').'" to bundle');
      if(!empty($asset['plugin'])) {
        $absPath = $this->getPluginPath($asset['plugin']) . $asset['source'];
      } else {
        $absPath = WWW_ROOT . $asset['source'];
      }

      // Open our file
      // Throw an exception if it doesn't exist
      $assetFile = new File($absPath, false);
      if(!$assetFile->exists()) throw new \Exception('File "' . $absPath . '" does not exist!');

      // Get the content of the current asset
      $fileContent = $assetFile->read();

      // Check if we need to minify the asset
      // If so, minify it
      // Else, add as-is
      /*
      if(isset($asset['minify']) && $asset['minify']) {
        switch($data['type']) {
          case 'js':
            $minifier = new Minify\JS();
            $minifier->add($fileContent);
            $fileContent = $minifier->minify();
            break;
        }
      }
      */

      // Append our filecontent to the bundle
      $content .= $fileContent;
    }

    // Write out our bundle
    $bundleFile = new File(WWW_ROOT . DS . 'bundles' . DS . $data['type'] . DS . $name . '.' . $data['type'], true);
    $bundleFile->write($content, 'w', true);
  }

  /**
   * Get the absolute path to the given $plugin.
   *
   * @param string $plugin The name of the plugin.
   * @return string
   */
  protected function getPluginPath($plugin) {
    if (!Plugin::isLoaded($plugin)) {
      throw new MissingPluginException('Plugin ' . $plugin . ' is not loaded.');
    }
    $pluginPath = Plugin::path($plugin);

    return $pluginPath . 'webroot' . DS;
  }
}
