<?php
  namespace App\Webhook;

  use Cake\Http\Client;
  use Josegonzalez\CakeQueuesadilla\Queue\Queue;
  use Cake\Orm\TableRegistry;
  use Cake\i18n\Time;
  use Cake\Http\ServerRequest;
  use Cake\Core\Configure;

  class GetGitlabBinary {
    private $repo;
    private $ref;
    private $builds;
    private $tag;
    private $artifact;

    public function run($parent) {
      // Make sure the X-Gitlab-Token header is correct
      if(trim(Configure::read('Security.gitlab_webhook_token')) != $parent->request->getHeader('X-Gitlab-Token')[0]) {
        return [
          'status' => '403',
          'info' => [
            'errors' => [
              'Invalid webhook token'
            ]
          ]
        ];
      }

      // Get the data
      $data = $parent->request->getData();

      // Check if the hook is caused by a pipeline
      if($data['object_kind'] != 'pipeline') {
        return [
          'status' => '403',
          'info' => [
            'errors' => [
              'This webhook may only be triggered from pipelines'
            ]
          ]
        ];
      }
      
      // Check if it's a tag
      if(!$data['object_attributes']['tag']) {
        return [
          'status' => '403',
          'info' => [
            'errors' => [
              'This webhook may only be triggered from tags'
            ]
          ]
        ];
      }

      // Filter out everything that:
      // - isn't the publish stage
      // - isn't finished
      $this->builds = collection($data['builds']);
      $this->builds = $this->builds->filter(function($build) {
        return $build['stage'] == 'publish' && $build['status'] == 'success';
      });

      if($this->builds->count() < 1) {
        return [
          'status' => '500',
          'info' => [
            'errors' => [
              'Please finish the publish stage before running this hook!'
            ]
          ]
        ];
      }

      // Get the information we need
      $this->ref = $data['object_attributes']['ref'];
      $this->repo = [
        'name' => $data['project']['name'],
        'namespace' => $data['project']['path_with_namespace']
      ];
      $this->artifact = 'https://gitlab.com/'.$data['project']['path_with_namespace'].'/-/jobs/'.$this->builds->first()['id'].'/artifacts/download';

      // Get the tag information
      // Then create queue item
      $this->getTag();
      $queue = $this->createQueue();
      
      if(!$queue) {
        return [
          'status' => '500',
          'info' => [
            'errors' => [
              'Could not create queue job'
            ]
          ]
        ];
      }

      return [
        'status' => '200',
        'info' => [
          'message' => 'Webhook executed successfully',
          'errors' => [
          ]
        ]
      ];
    }

    private function getTag() {
      $http = new Client();
      $resp = $http->get('https://gitlab.com/api/v4/projects/'.urlencode($this->repo['namespace']).'/repository/tags/'.$this->ref);
      $this->tag = json_decode($resp->body(),1);
    }

    private function createQueue() {
      // Get the project entry
      $project = TableRegistry::getTableLocator()->get('projects')->findByName($this->repo['name'])->first();
      if(!$project) return false;

      // Parse all the changes
      $changes = [];
      $repoChanges = preg_split('/  \r\n|\r|\n/', $this->tag['release']['description']);
      foreach($repoChanges as $change) {
        // Skip empty changes
        if($change == '') continue;

        // Add the change to our array
        $changes[] = ['change' => $change];
      }

      // Create our entity
      $entity = TableRegistry::getTableLocator()->get('projects_releases')->newEntity([
        'project_id' => $project->id,
        'version' => $this->ref,
        'description' => $this->tag['message'],
        'date' => Time::now(),
        'published' => 0,
        'projects_changelogs' => $changes
      ]);

      // Prevent an issues with the `change` keyword being reserved
      $driver = TableRegistry::getTableLocator()->get('projects_releases')->connection()->getDriver();
      $autoQuouting = $driver->isAutoQuotingEnabled();
      $driver->enableAutoQuoting(true);

      // Save the entity
      $status = TableRegistry::getTableLocator()->get('projects_releases')->save($entity);

      // Set the autoQuoting back to the original value
      $driver->enableAutoQuoting($autoQuouting);

      if(!$status) return false;

      Queue::push('\App\Job\DownloadReleaseJob::run', [
        'app' => [
          'name' => $project->name,
          'id' => $project->id,
          'version_id' => $entity->id,
          'version' => $this->ref
        ],
        'source' => $this->artifact
      ]);

      return true;
    }
  }