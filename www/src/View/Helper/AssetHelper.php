<?php
  /**
   * This Helper was originally written by Frank Förster (http://frankfoerster.com) under the MIT license.
   * It was modified as to better suit my needs.
   * Thanks Frank!
   */
  namespace App\View\Helper;

  use App\Util\H2Push;
  use App\Util\Preload;

  use Cake\Filesystem\File;
  use Cake\Core\Exception\MissingPluginException;
  use Cake\Core\Plugin;
  use Cake\Core\Configure;
  use Cake\Utility\Inflector;
  use Cake\View\Helper;
  use Cake\Cache\Cache;
  use Symfony\Component\Yaml\Yaml;

  class AssetHelper extends Helper {
      public $helpers = [
        'Url'
      ];

      protected $_defaultConfig = [
        'fullBase' => false
      ];

      /**
       * Output a link stylesheet tag for a specific css file and optionally
       * append a last modified hash to clear the browser cache.
       *
       * @param string $path The path to the css file relative to WEBROOT
       * @param bool $plugin Either false or the name of a plugin.
       * @param bool $appendHash Whether to append a hash to the url.
       * @param array $attributes Additional html attributes to render on the link tag.
       * @return string
       */
      public function css($path, array $options = [], array $attributes = []) {
        // Get the plugin name (default: false)
        $plugin = $options['plugin'] ?? false;

        // Get whether we need to append the hash (default: false)
        $appendHash = $options['hash'] ?? false;

        // Get our url
        $href = $this->getUrl($path, $plugin, $appendHash);

        // Get whether we need to use server push
        // If so, call the H2Push
        if(!empty($options['push']) && $options['push'] === true) H2Push::add($href, 'style');

        // Check if we want to preload
        // If so, add this asset to our Preload assets
        // else, return a regular HTML tag
        if(!empty($options['preload']) && $options['preload'] === true) {
          Preload::add($href, 'style');
          return '<link rel="preload" href="' . $href . '" as="style">';
        } else {
          return '<link rel="stylesheet" type="text/css" href="' . $href . '"' . $this->_renderAttributes($attributes) . '>';
        }
      }

      /**
       * Output a script tag for a specific js file and optionally
       * append a last modified hash to clear the browser cache.
       *
       * @param string $path The path to the css file relative to the app or plugin webroot.
       * @param bool|string $plugin Either false or the name of a plugin.
       * @param bool $appendHash Whether to append a hash to the url.
       * @param array $attributes Additional html attributes to render on the script tag.
       * @return string
       */
      public function js($path, array $options = [], array $attributes = []) {
        // Get the plugin name (default: false)
        $plugin = $options['plugin'] ?? false;

        // Get whether we need to append the hash (default: false)
        $appendHash = $options['hash'] ?? false;

        // Get our url
        $src = $this->getUrl($path, $plugin, $appendHash);

        // Get whether we need to use server push
        // If so, call the H2Push
        if(!empty($options['push']) && $options['push'] === true) H2Push::add($src, 'script');

        // Check if we want to preload
        // If so, add this asset to our Preload assets
        // else, return a regular HTML tag
        if(!empty($options['preload']) && $options['preload'] === true) {
          Preload::add($src, 'script');
          return '<link rel="preload" href="' . $src . '" as="script">';
        } else {
          return '<script type="text/javascript" src="' . $src . '"' . $this->_renderAttributes($attributes) . '></script>';
        }
      }

      public function img($path, array $options = [], array $attributes = []) {
        // Get the plugin name (default: false)
        $plugin = $options['plugin'] ?? false;

        // Get whether we need to append the hash (default: false)
        $appendHash = $options['hash'] ?? false;

        // Get our url
        $src = $this->getUrl($path, $plugin, $appendHash);

        // Get whether we need to use server push
        // If so, call the H2Push
        if(!empty($options['push']) && $options['push'] === true) H2Push::add($src, 'image');

        // Check if we want to preload
        // If so, add this asset to our Preload assets
        if(!empty($options['preload']) && $options['preload'] === true) Preload::add($src, 'image');

        // Return a regular HTML img
        return '<img src="' . $src . '"' . $this->_renderAttributes($attributes) . '></script>';
      }

    public function font($path, array $options = [], array $attributes = []) {
      // Get the plugin name (default: false)
      $plugin = $options['plugin'] ?? false;

      // Get whether we need to append the hash (default: false)
      $appendHash = $options['hash'] ?? false;

      // Get our url
      $src = $this->getUrl($path, $plugin, $appendHash);

      // Get whether we need to use server push
      // If so, call the H2Push
      if(!empty($options['push']) && $options['push'] === true) H2Push::add($src, 'font');

      // Check if we want to preload
      // If so, add this asset to our Preload assets
      if(!empty($options['preload']) && $options['preload'] === true) Preload::add($src, 'font', true);
    }

      public function bundle(string $name, array $options = [], array $attributes = []) {
        // Check if we have a cached config
        // If so, Load and parse our bundle config and write it to cache
        // Else (implicit) , use the cached version
        if(($config = Cache::read('Bundle Config')) != true) {
          $yaml = new File(CONFIG . 'bundles.yaml', false);
          if(!$yaml->exists()) throw new \Exception('YAML as "' . CONFIG . '/bundles.yaml" could not be found!');
          $config = Yaml::parse($yaml->read());
          Cache::write('Bundle Config', $config);
        }

        // Check if we're in debug
        // If so, return each file seperately
        if(Configure::read('debug')) return $this->bundleDebug($config[$name], $options, $attributes);

        // Get the HTML links required
        switch($config[$name]['type']) {
          case 'css':
            return $this->css("/bundles/css/" . $name . '.css', $options, $attributes);
          case 'js':
            return $this->js("/bundles/js/" . $name . '.js', $options, $attributes);
        }
      }

      public function bundleDebug(array $bundle, array $options = [], array $attributes = []) {
        $output = '';
        foreach($bundle['sources'] as $asset) {
          $assetOptions = $options;
          // Check if a plugin is set
          // If so, add it to the options
          if(!empty($asset['plugin'])) $assetOptions['plugin'] = $asset['plugin'];
          $output .= $this->{$bundle['type']}($asset['source'], $assetOptions, $attributes);
        }
        return $output;
      }

      /**
       * Get the asset url for a specific file.
       *
       * @param string $path The path to the css file relative to the app or plugin webroot.
       * @param bool|string $plugin Either false or the name of a plugin.
       * @param bool $appendHash Whether to append a hash to the url.
       * @return string
       */
      public function getUrl($path, $plugin, $appendHash = true) {
        $pathParts = explode('/', $path);
        $isAssetPath = ($pathParts[0] === 'ASSETS');

        if ($isAssetPath) {
          $absPath = $this->_getBaseAssetPath($plugin) . join('/', array_slice($pathParts, 1));
        } else {
          $absPath = $this->_getBasePath($plugin) . $path;
        }

        $hash = $appendHash ? $this->_getFileHash($absPath) : '';
        $pathPrefix = '';
        if ($plugin !== false) {
          $pluginParts = explode('/', $plugin);
          foreach ($pluginParts as $key => $part) {
              $pluginParts[$key] = Inflector::underscore($part);
          }
          $pathPrefix .= join('/', $pluginParts) . '/';
        } else {
          $pathPrefix = '';
        }
        $path = $pathPrefix . $path;

        $options = [
          'fullBase' => (bool)$this->getConfig('fullBase', false)
        ];

        return $this->Url->assetUrl($path, $options) . $hash;
      }

      /**
       * Get the path to /src/Assets either of the app or the provided $plugin.
       *
       * @param bool|string $plugin The name of a plugin or false.
       * @return string
       */
      protected function _getBaseAssetPath($plugin = false) {
        if ($plugin !== false) {
          return $this->_getPluginPath($plugin) . 'src' . DS . 'Assets' . DS;
        }

        return ROOT . DS . 'src' . DS . 'Assets' . DS;
      }

      /**
       * Get the base path to the app webroot or a plugin webroot.
       *
       * @param bool|string $plugin Either false or the name of a plugin.
       * @return string
       */
      protected function _getBasePath($plugin = false) {
        if ($plugin !== false) {
          return $this->_getPluginPath($plugin) . 'webroot' . DS;
        }

        return WWW_ROOT;
      }

      /**
       * Get the absolute path to the given $plugin.
       *
       * @param string $plugin The name of the plugin.
       * @return string
       */
      protected function _getPluginPath($plugin) {
        if (!Plugin::loaded($plugin)) {
            throw new MissingPluginException('Plugin ' . $plugin . ' is not loaded.');
        }
        $pluginPath = Plugin::path($plugin);

         return $pluginPath;
      }

      /**
       * Get the ?h=abc123def has part built from the content of the file.
       *
       * @param string $absPath The absolute path to the file.
       * @return string
       */
      protected function _getFileHash($absPath) {
        // Check if we already have the hash in our cache
        // If so, return the first 9 characters
        if(($hash = Cache::read('asset hash ' . $absPath)) == true) {
          return '?h=' . substr($hash, 0,9);
        }

        // No hash found
        // Check if the file exists
        if (file_exists($absPath)) {
          // Get the SHA256 hash of the file
          $hash = \hash_file('sha256', $absPath);

          // Store the hash in our cache
          Cache::write('asset hash ' . $absPath, $hash);

          // Get the first 9 characters of our hash
          // Then return our hash string
          return '?h=' .  substr($hash, 0,9);
        }

        // It appears the file doesn't exist here
        // Just return an empty string
        return '';
      }

      /**
       * Render attribute key value pairs as html attributes.
       *
       * @param array $attributes Key value pairs of html attributes.
       * @return string
       */
      protected function _renderAttributes(array $attributes = []) {
        $attributeStrings = [];
        foreach ($attributes as $attribute => $value) {
          $attributeStrings[] = $attribute . '="' . htmlentities($value) . '"';
        }

        if (empty($attributeStrings)) {
          return '';
        }

        return ' ' . join(' ', $attributeStrings);
      }
  }
