<?php
  namespace App\View\Helper;

  use Cake\View\Helper;

  class ProjectHelper extends Helper {
    public function releaseStatus($status) : string {
      switch($status) {
        case -1:
          return "Pre-release";
          break;
        case 0:
          return "Private";
          break;
        case 1:
          return "Public";
          break;
      }
    }
  }