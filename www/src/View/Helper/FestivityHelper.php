<?php
  namespace App\View\Helper;

  use Cake\I18n\Time;
  use Cake\View\Helper;

  class FestivityHelper extends Helper {
    private $festivities = [
      ['name' => 'Birthday', 'date' => '11-08'],
      ['name' => 'Saint Patricks Day', 'date' => '03-17']
    ];

    public function getFestivity(): string {
      // Turn festivities into a collection
      $this->festivities = collection($this->festivities);

      // Get the current festicity
      $date = Time::now()->i18nFormat('MM-dd');
      $festivity = $this->festivities->firstMatch(['date' => $date]);

      // Check if a festivity was found
      // If not, return an empty string
      // Else, return the name of the festivity
      if(!$festivity) return '';
      return $festivity['name'];
    }
  }
