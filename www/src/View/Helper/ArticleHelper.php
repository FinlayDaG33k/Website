<?php
  namespace App\View\Helper;

  use Cake\View\Helper;
  use Cake\View\View;
  use Cake\Filesystem\File;

  class ArticleHelper extends Helper {

    public function stripTags($article) {
      return strip_tags($article);
    }

    public function getExcerpt($article, int $wordLimit = 50) {
      $article = $this->getView()->Shortcode->stripShortcodes($article);
      $article = $this->stripTags($article);

      if (str_word_count($article, 0) > $wordLimit) {
        $words = str_word_count($article, 2);
        $pos = array_keys($words);
        $article = substr($article, 0, $pos[$wordLimit]) . '...';
      }

      return $article;
    }

    public function getTitle($article) {
      switch($article->published) {
        case 0:
          return "Private: " . $article->title;
          break;
        case 1:
          return $article->title;
          break;
        case 2:
          return "Unlisted: " . $article->title;
          break;
      }
    }

    public function getBody($article) {
      // Open a file handle
      $file = new File(ROOT . DS . 'blog-posts' . DS . $article->hash . '.txt');
      return $file->read();
    }
  }