<?php
namespace App\View\Cell;

use Cake\View\Cell;

class DisclaimerShortcodeCell extends Cell {
  public function display(array $params) {
    $this->viewBuilder()->setTemplate($params['type']);
  }
}