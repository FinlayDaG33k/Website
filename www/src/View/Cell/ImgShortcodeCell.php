<?php
namespace App\View\Cell;

use Cake\View\Cell;
use Cake\Routing\Router;
use Cake\Filesystem\File;
use Josegonzalez\CakeQueuesadilla\Queue\Queue;
use \Imagick;

class ImgShortcodeCell extends Cell {
  private string $cache_dir = WWW_ROOT . "img/cache/";
  private string $watermark_file = WWW_ROOT . 'img/logo-FDG-300-01-300x300.png';
  private array $params = [];
  private array $thumbnailWidths = [250, 450];
  private string $content = '';
  private string $srcUrlHash = '';
  private string $ipfsHost = '';

  public function display(array $params, string $content) {
    $this->params = $params;
    $this->ipfsHost = env('IPFS_PUBLIC_ROOT', 'https://ipfs.finlaydag33k.nl/ipfs/');
    
    // Check if the content is empty
    // If not, store that in a property
    // If so, use the "src" parameter
    if(!empty($content)) {
      $this->content = $content;
    } else {
      $this->content = $params['src'];
    }

    // Hash our src url
    $this->srcUrlHash = sha1($this->content);

    // Make sure the cachedir exists
    // If not, create it
    if(!file_exists($this->cache_dir)) mkdir($this->cache_dir);

    // Check whether a source is set
    // If so, don't watermark our image
    // If not, watermark it
    if(!empty($this->params['source'])) {
      if(!file_exists($this->cache_dir . $this->srcUrlHash . ".png")) $this->store();
    } else {
      if(!file_exists($this->cache_dir . $this->srcUrlHash . ".png")) $this->watermarkImage();
    }

    // Check whether this image exists on IPFS
    // If not, upload it and store a reference
    if(!file_exists($this->cache_dir . $this->srcUrlHash . ".png.ipfs")) {
      Queue::push('\App\Job\IPFSImageUploadJob::run', [
        'src' => $this->srcUrlHash . ".png",
        'outDir' => $this->cache_dir,
      ]);
    }

    // Get the dimensions of the image
    list($width, $height) = getimagesize($this->cache_dir . $this->srcUrlHash . ".png");

    // Calculate the max-height of the image
    $maxHeight = ($height / $width) * 450;

    // Calculate the max-width of the card
    $maxWidth = ($width > 450) ? 450 : $width;

    // Create thumbnails
    $this->createThumbnails();

    // Check if the user enabled IPFS
    $userSettings = json_decode($this->request->getCookie('userSettings'), true);
    $this->useIpfs = $userSettings['ipfs'];

    // Pass our view variables
    $this->set('caption', !empty($this->params['caption']) ? $this->params['caption'] : null);
    $this->set('urls', $this->buildImageUrls());
    $this->set('source', !empty($this->params['source']) ? $this->params['source'] : null);
    $this->set('linkonly', isset($this->params['linkonly']) ? $this->params['linkonly'] : false);
    $this->set('dimensions', [
      'width' => $width,
      'height' => $height,
      'max-height' => $maxHeight,
      'max-width' => $maxWidth,
    ]);
  }

  private function buildImageUrls(){
    // Check if we have a hash of the file
    // If not, return a regular URL
    if(!file_exists($this->cache_dir . $this->srcUrlHash . ".png.ipfs")) {
      return [
        'full' => Router::fullBaseUrl() . "/img/cache/" . $this->srcUrlHash . ".png",
        'set' => $this->getSrcSet(),
      ];
    }

    // Get the url from IPFS
    // If no url is found, use a regular URL for now
    // Do check the srcset for IPFS urls
    $ipfsUrl = $this->getIpfsUrl($this->cache_dir . $this->srcUrlHash . ".png.ipfs");
    if(!$ipfsUrl) {
      return [
        'full' => Router::fullBaseUrl() . "/img/cache/" . $this->srcUrlHash . ".png",
        'set' => $this->getSrcSet(),
      ];
    }

    // Mark the file for cleaning up
    Queue::push('\App\Job\IPFSCleanupJob::run', [
      'file' => $this->cache_dir . $this->srcUrlHash . ".png",
    ]);

    // Return the IPFS url
    return [
      'full' => $this->ipfsHost . $ipfsUrl,
      'set' => $this->getSrcSet(),
    ];
  }

  private function getSrcSet() {
    $set = '';
    $last = end($this->thumbnailWidths);
    foreach($this->thumbnailWidths as $width) {
      // Check we have an IPFS hash
      // If so, add it to the srcset, mark for cleanup and continue
      $ipfsUrl = $this->getIpfsUrl($this->cache_dir . $this->srcUrlHash . "-thumb-".$width.".webp.ipfs");
      if($ipfsUrl) {
        $set .= $this->ipfsHost;
        $set .= $ipfsUrl;
        $set .= ' ';
        $set .= $width;
        $set .= 'w';
        if($width !== $last) $set .= ', ';

        // Mark the files for cleanup
        Queue::push('\App\Job\IPFSCleanupJob::run', [
          'file' => $this->cache_dir . $this->srcUrlHash . "-thumb-".$width.".webp",
        ]);

        continue;
      }

      // Check if a regular webp exists
      // If so, upload it to IPFS and add regular to the srcset
      if(file_exists($this->cache_dir . $this->srcUrlHash . "-thumb-".$width.".webp")) {
        Queue::push('\App\Job\IPFSImageUploadJob::run', [
          'src' => $this->srcUrlHash . "-thumb-".$width.".webp",
          'outDir' => $this->cache_dir,
        ]);

        $set .= Router::fullBaseUrl() . "/img/cache/" . $this->srcUrlHash . "-thumb-".$width.".webp";
        $set .= ' ';
        $set .= $width;
        $set .= 'w';
        if($width !== $last) $set .= ', ';
      }
    }
    
    return $set;
  }

  private function getIpfsUrl(string $ref) {
    // Make sure the reference exists
    if(!file_exists($ref)) return null;

    // Get the hash from the file
    $hash = file_get_contents($ref);
    
    // Check whether there is a hash
    // If not, remove the reference
    if(empty($hash)) {
      unlink($ref);
      return null;
    }

    return $hash;
  }

  private function watermarkImage(){
    // Create a temporary file
    $tmpfname = tempnam("/tmp", "img_");

    // Download our image into the temporary file
    file_put_contents($tmpfname, file_get_contents($this->content));

    // Load our temporary file
    $im = new Imagick($tmpfname);

    // Load the watermark
    $watermark = new Imagick($this->watermark_file);
    
    // Get the height and width of the image
    $imHeight = $im->getImageHeight();
    $imWidth = $im->getImageWidth();

    // Get the height and width of the watermark
    $markHeight = $watermark->getImageHeight();
    $markWidth = $watermark->getImageWidth();

    // Calculate the sizes for the watermark
    // If the width of the main image is bigger than the height:
    // - Divide the mark width by 100
    // - Multiply by 6.25
    // Else:
    // - Divide the mark height by 100
    // - Multiply by 12.5
    if($imWidth > $imHeight){
      $markDimensions = [
        'height' => ($imWidth / 100) * 6.25,
        'width' => ($imWidth / 100) * 6.25
      ];
    }else{
      $markDimensions = [
        'height' => ($imHeight / 100) * 12.5,
        'width' => ($imHeight / 100) * 12.5
      ];
    }
    
    // Resize the watermark
    // Then update the object
    $watermark->scaleImage($markDimensions['width'], $markDimensions['height']);
    $markHeight = $watermark->getImageHeight();
    $markWidth = $watermark->getImageWidth();
    
    // Calculate the watermark position
    $x = $imWidth - $markWidth;
    $y = $imHeight - $markHeight;

    // Add the watermark to the image based on everything we've just calculated
    $im->compositeImage($watermark, Imagick::COMPOSITE_OVER, $x, $y);

    // Save the image in our cache dir
    file_put_contents($this->cache_dir . $this->srcUrlHash . ".png", $im);
  }

  private function createThumbnail(int $targetWidth) {
    // Load the image
    $im = new Imagick($this->cache_dir . $this->srcUrlHash . ".png");

    // Turn into Webp
    $im->setImageFormat('webp');

    // Set image quality
    $im->setImageCompressionQuality(60);

    // Check if our width is higher than our target
    // If so, rescale
    if($im->getImageWidth() > $targetWidth) $im->thumbnailImage($targetWidth, 0);

    // Save the image in our cache dir
    file_put_contents($this->cache_dir . $this->srcUrlHash . "-thumb-".$targetWidth.".webp", $im);
  }

  private function createThumbnails() {
    foreach($this->thumbnailWidths as $width) {
      if(!file_exists($this->cache_dir . $this->srcUrlHash . "-thumb-".$width.".webp")) {
        $this->createThumbnail($width);

        Queue::push('\App\Job\IPFSImageUploadJob::run', [
          'src' => $this->srcUrlHash . "-thumb-".$width.".webp",
          'outDir' => $this->cache_dir,
        ]);
      }
    }
  }

  public function store() {
    // Create a temporary file
    // Store our image in it
    $tmpfname = tempnam("/tmp", "img_");
    file_put_contents($tmpfname, file_get_contents($this->content));

    // Turn the image into a PNG
    $im = new Imagick($tmpfname);

    // Save the image in our cache dir
    file_put_contents($this->cache_dir . $this->srcUrlHash . ".png", $im);
  }
}