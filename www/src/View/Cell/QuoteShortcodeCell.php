<?php
namespace App\View\Cell;

use Cake\View\Cell;

class QuoteShortcodeCell extends Cell {
  public function display(array $params) {
    $this->set('source',!empty($params['src']) ? $params['src'] : null);
  }
}