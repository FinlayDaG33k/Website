<?php
namespace App\View\Cell;

use Cake\View\Cell;

class UrlShortcodeCell extends Cell {
  public function display(array $params, string $content) {
    // Start our output
    $output = '<a';

    // Add our src
    // Default to "#"
    $output .= ' href="';
    if(!empty($params['src'])) {
      $output .= $params['src'];
    } else {
      $output .= "#";
    }
    $output .= '"';

    // Add our target
    // Default to "_blank"
    $output .= ' target="';
    if(!empty($params['target'])) {
      $output .= $params['target'];
    } else {
      $output .= "_blank";
    }
    $output .= '"';

    // Close the opening tag
    $output .= ">";

    // Add our link content
    // Default to the link as text
    if(!empty($content)) {
      $output .= $content;
    } else {
      $output .= $params['src'];
    }

    // Finalize the HTML
    $output .= "</a>";

    $this->set('output', $output);
  }
}