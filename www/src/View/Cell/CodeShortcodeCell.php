<?php
namespace App\View\Cell;

use Cake\View\Cell;

class CodeShortcodeCell extends Cell {
  public function display(array $params, string $content) {
    $this->set('lang', !empty($params['lang']) ? $params['lang'] : 'plaintext');
  }
}