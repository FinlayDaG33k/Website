<?php
namespace App\View\Cell;

use Cake\View\Cell;

class RatingShortcodeCell extends Cell {
  public function display(array $params, string $content) {
    $this->set('rating', intval($content));
  }
}