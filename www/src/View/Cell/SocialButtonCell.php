<?php
namespace App\View\Cell;

use Cake\View\Cell;

class SocialButtonCell extends Cell {
  public function display() {
    $this->set('link',$this->args[0]);
    $this->set('icon',$this->args[1]);
  }
}