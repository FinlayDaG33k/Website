<?php
  namespace App\Job;

  use App\Util\IPFS;
  use Cake\Filesystem\File;

  class IPFSCleanupJob {
    private static $ipfs;

    public static function run($job) {
      if(!file_exists($job->data('file'))) return;
      unlink($job->data('file'));
    }
  }