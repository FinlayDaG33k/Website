<?php
  namespace App\Job;

  use Cake\Filesystem\Folder;
  use Cake\Filesystem\File;

  class ProcessDistfileJob {
    private static $_manifest = [
      'version' => 1,
      'app' => [],
      'files' => []
    ];

    public static function run($job) {
      // Add some info to the manifest
      self::$_manifest['app'] = $job->data('app');

      // Get the file and folder
      $folder = new Folder(WWW_ROOT . "uploads/distfiles/");
      $file = new File($folder->path . $job->data('distfile'));

      // Extract the zipfile
      self::_extractDistfile($file);

      // Calculate the manifest
      self::_calculateManifest($folder->path . $file->name());

      // Write the manifest
      $manifest = new File($folder->path . $file->name() . '.manifest', true);
      $manifest->write(json_encode(self::$_manifest,1), 'w', true);
    }

    private static function _extractDistfile($file) {
      $zip = new \ZipArchive;
      $zip->open(WWW_ROOT . 'uploads/distfiles/' . $file->name);
      $zip->extractTo(WWW_ROOT . 'uploads/distfiles/' . $file->name());
    }

    private static function _calculateManifest($src) {
      // Get a list of all files
      $dir_iterator = new \RecursiveDirectoryIterator($src);
      $iterator = new \RecursiveIteratorIterator($dir_iterator, \RecursiveIteratorIterator::SELF_FIRST);
      
      foreach($iterator as $file) {
        if ($file->isFile()) {
          $filename = str_replace($src, '', $file->getPathname());
          self::$_manifest['files'][$filename] = hash_file('sha256', $file->getPathname());
        }
      }
    }
  }