<?php
  namespace App\Job;

  use Cake\Filesystem\Folder;
  use Cake\Filesystem\File;

  class DownloadReleaseJob {
    private static $tmpDir;
    private static $targetDir = WWW_ROOT . 'uploads' .DS . 'distfiles';

    public static function run($job) {
      // Get the location for our temp dir
      self::$tmpDir = new Folder('/var/www/tmp/uploads/', true);

      // Download the release file
      $target = self::$tmpDir->path . $job->data('app')['name'] . '-' . $job->data('app')['version'] . '-all.zip';
      file_put_contents($target, fopen($job->data('source'), 'r'));

      // Open the zipfile handle
      $file = new File($target);

      // Extract the zipfile
      self::_extractDistfile($file);

      // Repack each version into it's own distfile
      // And move them to the normal location of distfiles
      self::_packDistfiles($job, 'win');
      self::_packDistfiles($job, 'linux');

      // Clean up everything
      $file->delete();
    }

    private static function _extractDistfile($file) {
      $zip = new \ZipArchive;
      $zip->open($file->path);
      $zip->extractTo(self::$tmpDir->path . $file->name());
    }

    private static function _packDistfiles($job, string $platform) {
      // Check if this platform exists
      $rootPath = '/var/www/tmp/uploads/' . $job->data('app')['name'] . '-' . $job->data('app')['version'] . '-all'.DS. 'release'.DS.$platform.'-unpacked';
      if(!file_exists($rootPath)) {
        return false;
      }

      $zip = new \ZipArchive;
      $target ='/var/www/tmp/uploads/' . $job->data('app')['name'] . '-' . $job->data('app')['version'] . '-' . $platform . '.zip';
      $zip->open($target, \ZipArchive::CREATE | \ZipArchive::OVERWRITE);

      $files = new \RecursiveIteratorIterator(
        new \RecursiveDirectoryIterator($rootPath),
        \RecursiveIteratorIterator::LEAVES_ONLY
      );

      foreach ($files as $name => $file) {
        // Skip directories (they would be added automatically)
        if (!$file->isDir()) {
          // Get real and relative path for current file
          $filePath = $file->getRealPath();
          $relativePath = substr($filePath, strlen($rootPath) + 1);

          // Add current file to archive
          $zip->addFile($filePath, $relativePath);
        }
      }

      // Zip it all up
      $zip->close();

      // Move to the distfiles directory
      rename($target, self::$targetDir . DS . $job->data('app')['name'] . '-' . $job->data('app')['version'] . '-' . $platform . '.zip');
    }
  }