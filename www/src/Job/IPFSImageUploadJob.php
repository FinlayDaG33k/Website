<?php
  namespace App\Job;

  use App\Util\IPFS;
  use Cake\Filesystem\File;

  class IPFSImageUploadJob {
    private static $ipfs;

    public static function run($job) {
      // Initialize IPFS
      self::$ipfs = new IPFS(
        env('IPFS_GATEWAY_HOST', 'localhost'),
        env('IPFS_GATEWAY_PORT', 8080),
        env('IPFS_API_PORT', 5001)
      );

      // Read our image
      $data = file_get_contents($job->data('outDir') . $job->data('src'));
      if($data === null) return;

      // Upload the image to IPFS
      $res = static::$ipfs->add($data);

      // Store a reference to the image
      $ref = new File($job->data('outDir') . $job->data('src') . ".ipfs", true);
      $ref->write($res, 'w', true);
    }
  }