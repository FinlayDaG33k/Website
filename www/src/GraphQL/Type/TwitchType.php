<?php
  namespace App\GraphQL\Type;

  use App\GraphQL\Data\Source\TwitchDataSource;

  use Admiral\GraphQL\Types;

  use GraphQL\Type\Definition\ResolveInfo;
  use GraphQL\Type\Definition\ObjectType;

  class TwitchType {
    protected $twitchDataSource;

    public function config() {
      return [
        'name' => 'Twitch',
        'description' => 'Information about my Twitch channel',
        'fields' => function() {
          return [
            'isLive' => [
              'type' => Types::get('boolean'),
              'description' => 'Whether I am currently live on twitch'
            ],
            'game' => [
              'type' => Types::get('string'),
              'description' => 'Name of the game I am currently playing',
            ],
            'start' => [
              'type' => Types::get('string'),
              'description' => 'Timestamp of when this stream started'
            ],
            'viewers' => [
              'type' => Types::get('int'),
              'description' => 'The amount of viewers currently watching'
            ]
          ];
        },
        'resolveField' => function($project, $args, $context, ResolveInfo $info) {
          $method = 'resolve' . ucfirst($info->fieldName);
          if (method_exists($this, $method)) {
            // Initialize the Datasource if not done yet
            if(!$this->twitchDataSource) $this->twitchDataSource = new TwitchDataSource();

            // Resolve the field and return the info
            return $this->{$method}($project, $args, $context, $info);
          } else {
            return $project->{$info->fieldName};
          }
        }
      ];
    }

    public function resolveIsLive() {
      return $this->twitchDataSource->isLive();
    }

    public function resolveGame() {
      return $this->twitchDataSource->game();
    }

    public function resolveStart() {
      return  $this->twitchDataSource->startTime();
    }

    public function resolveViewers() {
      return $this->twitchDataSource->viewers();
    }
  }