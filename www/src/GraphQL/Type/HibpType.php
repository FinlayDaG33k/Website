<?php
  namespace App\GraphQL\Type;

  use Admiral\GraphQL\Types;
  use App\GraphQL\Data\Source\HibpDataSource;

  class HibpType {
    public function config() {
      return [
        'name' => 'hibp',
        'type' => Types::listOf(Types::get('string')),
        'description' => 'Checks a password using the HIBP API',
        'args' => [
          'hash' => Types::get('string'),
        ],
        'resolve' => function($rootValue, $args) {
          $hibp = new HibpDataSource();
          return $hibp->checkPassword($args['hash']);
        }
      ];
    }
  }