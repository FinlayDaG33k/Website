<?php
  namespace App\GraphQL\Type;

  use Admiral\GraphQL\Types;

  class ProjectReleaseChangeType {
    public function config() {
      return [
        'name' => 'ProjectReleaseChange',
        'description' => 'Changes for a project',
        'fields' => function() {
          return [
            'id' => Types::get('id'),
            'type' => Types::get('string'),
            'change' => [
              'type' => Types::get('string'),
              'description' => 'Change entry'
            ]
          ];
        }
      ];
    }
  }