<?php
  namespace App\GraphQL\Type;

  use GraphQL\Type\Definition\ResolveInfo;

  use Admiral\GraphQL\Types;
  use App\GraphQL\Data\Source\ProjectDataSource;
  use App\GraphQL\Data\Source\ProjectReleaseDataSource;

  use Cake\Filesystem\{
    Folder,
    File
  };
  use Cake\Routing\Router;

  class ProjectReleaseType {
    public function config() {
      return [
        'name' => 'Project Release',
        'description' => 'A release for a project',
        'fields' => function() {
          return [
            'id' => Types::get('id'),
            'project' => [
              'type' => Types::get('Project'),
              'description' => 'The project this release belongs to'
            ],
            'version' => [
              'type' => Types::get('string'),
              'description' => 'Release version for humanoid-creatures'
            ],
            'date' => [
              'type' => Types::get('string'),
              'description' => 'Date the release was added'
            ],
            'published' => [
              'type' => Types::get('string'),
              'description' => 'Status of the publish'
            ],
            'description' => [
              'type' => Types::get('string'),
              'description' => 'Short description for the release'
            ],
            'changelog' => [
              'type' => Types::listOf(Types::get('ProjectReleaseChange')),
              'description' => 'All changes for this release'
            ]
          ];
        },
        'resolveField' => function($release, $args, $context, ResolveInfo $info) {
          $method = 'resolve' . ucfirst($info->fieldName);
          if (method_exists($this, $method)) {
            return $this->{$method}($release, $args, $context, $info);
          } else {
            return $release->{$info->fieldName};
          }
        }
      ];
    }

    public function resolveChangelog($release, $args) {
      // Find the associated changelogs
      return ProjectReleaseDataSource::findChangelogs($release->id);
    }
  }