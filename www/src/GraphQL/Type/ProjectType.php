<?php
  namespace App\GraphQL\Type;

  use Admiral\GraphQL\Types;
  use App\GraphQL\Data\Source\ProjectReleaseDataSource;

  use GraphQL\Type\Definition\ResolveInfo;
  use GraphQL\Type\Definition\ObjectType;

  use Admiral\Admiral\Permission;

  class ProjectType {
    public function config() {
      return [
        'name' => 'Project',
        'description' => 'My Projects',
        'fields' => function() {
          return [
            'id' => [
              'type' => Types::get('int'),
              'description' => 'Project id',
            ],
            'name' => [
              'type' => Types::get('string'),
              'description' => 'Project name'
            ],
            'description' => [
              'type' => Types::get('string'),
              'description' => 'Project description'
            ],
            'thumbnail' => [
              'type' => Types::get('string'),
              'description' => 'Project Thumbnail'
            ],
            'source' => [
              'type' => Types::get('string'),
              'description' => 'Link to the repo'
            ],
            'license' => [
              'type' => Types::get('string'),
              'description' => 'License this project is under'
            ],
            'releases' => [
              'type' => Types::listOf(Types::get('ProjectRelease')),
              'description' => 'List of releases for this project',
            ],
            'release' => [
              'type' => Types::get('ProjectRelease'),
              'description' => 'A specific release for this project',
              'args' => [
                'id' => Types::get('int'),
              ],
            ],
            'latest' => [
              'type' => new ObjectType([
                'name' => 'latestReleasesObject',
                'fields' => function() {
                  return [
                    'public' => [
                      'type' => Types::get('ProjectRelease'),
                      'description' => 'Latest public release available'
                    ],
                    'testing' =>  [
                      'type' => Types::get('ProjectRelease'),
                      'description' => 'Latest pre-release available'
                    ],
                  ];
                }
              ]),
              'description' => 'Latest releases available',
            ]
          ];
        },
        'resolveField' => function($project, $args, $context, ResolveInfo $info) {
          $method = 'resolve' . ucfirst($info->fieldName);
          if (method_exists($this, $method)) {
            return $this->{$method}($project, $args, $context, $info);
          } else {
            //dd($project);
            return $project->{$info->fieldName};
          }
        }
      ];
    }

    public function resolveLatest($project) {
      $projectReleaseDataSource = new ProjectReleaseDataSource();
      $release = $projectReleaseDataSource::findLatest($project->id);

      $releases = [
        'public' => $projectReleaseDataSource::findLatest($project->id, false)
      ];

      if(Permission::check('view_prereleases', 1)) {
        $releases['testing'] = $projectReleaseDataSource::findLatest($project->id, true);
      }

      return $releases;
    }

    public function resolveDescription($project) {
      return $project->projects_description->description;
    }

    public function resolveReleases($project, $args) {
      $projectReleaseDataSource = new ProjectReleaseDataSource();
      if(!empty($args['id'])) {
        $release = $projectReleaseDataSource::findRelease($args['id']);
        return $release;
      }
      
      $releases = $projectReleaseDataSource::findReleases($project->id);
      
      return $releases;
    }
  }