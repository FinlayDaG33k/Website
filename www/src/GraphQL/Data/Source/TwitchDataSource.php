<?php
  namespace App\GraphQL\Data\Source;

  use Cake\Core\Configure;
  use Cake\Http\Client;
  use Cake\Cache\Cache;

  class TwitchDataSource {
    private $clientId;
    private $clientSecret;
    private $http;
    private $channelInfo;
    private $OAuth;

    public function __construct() {
      $this->clientId = Configure::read('Twitch.client_credentials.client_id');
      $this->clientSecret = Configure::read('Twitch.client_credentials.client_secret');
      $this->http = new Client();
      $this->OAuth = $this->getOAuthToken();
      $this->channelInfo = $this->getChannelInfo();
    }
    
    /**
     * Returns whether the stream is live or not
     * 
     * @return bool Whether the stream is live
     */
    public function isLive(): bool {
      return empty($this->channelInfo) === false;
    }

    /**
     * Return the channelInfo
     * 
     * @return array The channel info
     */
    public function channelInfo() {
      return $this->channelInfo;
    }

    public function startTime() {
      if(empty($this->channelInfo)) return null;

      return $this->channelInfo['started_at'];
    }

    public function viewers() {
      if(empty($this->channelInfo)) return null;

      return $this->channelInfo['viewer_count'];
    }

    /**
     * Returns the name of the currently played game
     * 
     * @return string|null The name of the game
     */
    public function game(): ?string {
      // Check if we have a cached version available
      // If it's available, return that instead
      $cache = Cache::read('twitch_game_name', 'external_apis');
      if($cache !== false) return $cache;

      // Make sure a game id is set
      if(empty($this->channelInfo['game_id'])) return null;

      // Send our request
      try {
        $res = $this->http->get("https://api.twitch.tv/helix/games",['id' => $this->channelInfo['game_id']], [
          'headers' => [
            'Client-ID' => $this->clientId,
            'Authorization' => 'Bearer ' . $this->OAuth['access_token'],
          ]
        ]);
      } catch(\Exception $e) {
        throw new \Exception('Could not obtain game information for "'.$this->channelInfo['game_id'].'"!');
      }

      // Make sure our response was alright
      if(!$res->isOk()) throw new \Exception('Could not obtain game info. got code "' . $res->code . '"');

      // Parse our data
      $data = json_decode($res->getBody(), true);

      // Get our game info
      if(empty($data['data']) || empty($data['data'][0]) || empty($data['data'][0]['name'])) {
        $data = null;
      } else {
        $data = $data['data'][0]['name'];
      }
      
      // Write our cache
      Cache::write('twitch_game_name', $data, 'external_apis');

      // Return the name of the game
      return $data;
    }

    private function getOAuthToken() {
      // Check if we have a cached version available
      // If it's available, return that instead
      $cache = Cache::read('twitch_oauth_token', 'external_apis');
      if($cache !== false) return $cache;
      
      // Build our POST params
      $params = [
        'client_id' => $this->clientId,
        'client_secret' => $this->clientSecret,
        'grant_type' => 'client_credentials',
      ];

      // Send our request
      try {
        // Get our response
        $res = $this->http->post("https://id.twitch.tv/oauth2/token", $params);

        // Make sure our response was alright
        if(!$res->isOk()) throw new \Exception('Could not obtain OAuth token. got code "' . $res->code . '"');

        // Parse our response
        $token = \json_decode($res->getBody(), true);

        // Write the token to the cache
        Cache::write('twitch_oauth_token', $token, 'external_apis');

        // Set our token for immediate use
        return $token;
      } catch(\Exception $e) {
        throw new \Exception('Could not obtain OAuth token!');
      }
    }

    private function getChannelInfo() {
      // Check if we have a cached version available
      // If it's available, return that instead
      $cache = Cache::read('twitch_channel_info', 'external_apis');
      if($cache !== false) return $cache;

      // Send our request
      try {
        $res = $this->http->get( "https://api.twitch.tv/helix/streams/", ['user_login' => 'finlaydag33k'], [
          'headers' => [
            'Client-ID' => $this->clientId,
            'Authorization' => 'Bearer ' . $this->OAuth['access_token'],
          ],
          'timeout' => '1'
        ]);
      } catch (\Exception $e) {
        throw new \Exception('Could not obtain channel info');
      }

      // Make sure our response was alright
      if(!$res->isOk()) throw new \Exception('Could not obtain channel info. got code "' . $res->code . '"');

      // Parse our response data
      $data = json_decode($res->getBody(), true);

      // Get our stream info
      if(empty($data['data']) || empty($data['data'][0])) {
        $data = [];
      } else {
        $data = $data['data'][0];
      }

      // Write our cache
      Cache::write('twitch_channel_info', $data,'external_apis');

      // Return our data
      return $data;
    }
  }