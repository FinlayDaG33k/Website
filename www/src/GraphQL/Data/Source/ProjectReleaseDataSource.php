<?php
  namespace App\GraphQL\Data\Source;

  use Cake\ORM\TableRegistry;
  use Admiral\Admiral\Permission;

  class ProjectReleaseDataSource {
    private static $_projectsReleasesTable;
    private static $_projectsChangelogsTable;

    public function __construct() {
      self::$_projectsReleasesTable = TableRegistry::get('ProjectsReleases');
      self::$_projectsChangelogsTable = TableRegistry::get('ProjectsChangelogs');
    }

    public static function findReleases(int $project = null) {
      $releases = self::$_projectsReleasesTable;

      $conditions = function() {
        $conditions = [['published' => 1]];

        if(Permission::check('app.projects.releases.pre-releases.view', 1)) {
          $conditions[] = ['published' => -1];
        }

        if(Permission::check('app.projects.releases.private-releases.view', 1)) {
          $conditions[] = ['published' => 0];
        }

        return $conditions;
      };

      if($project === null) {
        return $releases
          ->find()
          ->where(['OR' => $conditions()]);
      }

      $releases = $releases
        ->findByProjectId($project)
        ->where(['OR' => $conditions()]);
        
      return $releases;
    }

    public static function findRelease(int $id = null) {
      if($id === null) return;

      $release = self::$_projectsReleasesTable
        ->findById($id);
        
      return $release->first();
    }

    public static function findLatest(int $project, bool $testing = false) {
      $release = self::$_projectsReleasesTable->findByProjectId($project);

      if($testing) {
        $release = $release->where(['published' => -1]);
      } else {
        $release = $release->where(['published' => 1]);
      }
       
      return $release
        ->order([
          'date' => 'DESC'
        ])
        ->first();
    }

    public static function findChangelogs(int $release) {
      $changelogs = self::$_projectsChangelogsTable
        ->findByReleaseId($release)
        ->select(['id' , 'change'])
        ->toArray();

      return $changelogs;
    }
  }