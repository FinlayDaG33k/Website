<?php
  namespace App\GraphQL\Data\Source;

  use FinlayDaG33k\HIBP\HIBP;

  class HibpDataSource {
    public static function checkPassword(string $hash = null) {
      return (new HIBP())->checkPassword($hash);
    }
  }