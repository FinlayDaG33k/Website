<?php
  namespace App\GraphQL\Data\Source;

  use Cake\ORM\TableRegistry;
  use Cake\Controller\Component\AuthComponent;
  use Admiral\Admiral\Permission;

  class ProjectDataSource {
    private static $_projectsTable;
    private static $_projectsReleasesTable;
    private static $_projectsDescriptionsTable;

    public function __construct() {
      self::$_projectsTable = TableRegistry::get('Projects');
      self::$_projectsReleasesTable = TableRegistry::get('ProjectsReleases'); 
      self::$_projectsDescriptionsTable = TableRegistry::get('ProjectsDescriptions');
    }

    public static function findProjects(bool $withRelease = true, int $limit = null, array $options = []) {
      $projects = self::$_projectsTable
        ->find();

      if($withRelease) {
        $projects = $projects
          ->select(['Projects.id'])
          ->select(self::$_projectsTable)
          ->matching('ProjectsReleases', function ($q) {
            $conditions = [['published' => 1]];
            if(Permission::check('app.projects.releases.private-releases.view', 1)) {
              $conditions[] = ['published' => 0];
            }

            if(Permission::check('app.projects.releases.pre-releases.view', 1)) {
              $conditions[] = ['published' => -1];
            }

            return $q
              ->where(['OR' => $conditions]);
          })
          ->distinct(['Projects.id']);
      }

      if($limit) {
        $projects = $projects->limit($limit);
      }

      if(!empty($options['cache'])) {
        $projects->cache($options['cache']);
      }

      if($projects->count() > 0) {
        $projects = self::$_projectsTable->loadInto($projects, ['ProjectsDescriptions']);
      }

      return $projects;
    }

    public static function findProject(int $id, bool $withRelease = true) {
      $project = self::$_projectsTable
        ->findById($id);

      if($withRelease) {
        $projects = $project->select(['Projects.id'])
          ->select(self::$_projectsTable)
          ->matching('ProjectsReleases', function ($q) {
            $conditions = [['published' => 1]];
            if(Permission::check('app.releases.private-releases.view', 1)) {
              $conditions[] = ['published' => 0];
            }

            if(Permission::check('app.releases.pre-releases.view', 1)) {
              $conditions[] = ['published' => -1];
            }

            return $q->where(['OR' => $conditions]);
          })
          ->distinct(['Projects.id']);
      }

      $project = $project->contain([
        'ProjectsDescriptions'
      ]);

      return $project->first();
    }

    public static function updateProject(int $id, array $data) {
      // Get the project by the requested it
      $project = self::$_projectsTable
        ->findById($id)
        ->contain([
          'ProjectsDescriptions'
        ])
        ->first();

      // Check if a project was found
      if(!$project) {
        return false;
      }

      // Prepare our data and patch our entity
      $data = [
        'name' => $data['name'],
        'projects_description' => [
          'description' => $data['description']
        ]
      ];
      $project = self::$_projectsTable->patchEntity($project, $data);

      // save the entity
      if(self::$_projectsTable->save($project)) {
        return true;
      }
      return false;
    }
  }