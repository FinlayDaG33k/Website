<?php
  namespace App\Controller;

  use Admiral\Admiral\Controller\AppController;
  use Admiral\Admiral\Permission;
  use App\GraphQL\Data\Source\ProjectDataSource;
  use App\GraphQL\Data\Source\ProjectReleaseDataSource;

  class ProjectsController extends AppController {
    private $_projectDataSource;
    private $_projectReleaseDataSource;

    public function initialize(): void {
      parent::initialize();

      $this->_projectDataSource = new ProjectDataSource();
      $this->_projectReleaseDataSource = new ProjectReleaseDataSource();

      $this->loadModel('Projects');

      if(in_array($this->request->action, ['index', 'add', 'edit'])) {
        $this->viewBuilder()->setLayout('Admiral/Admiral.admin');
      }
    }

    public function index() {
      $this->set('title', 'Projects');
      $this->set('projects', $this->_projectDataSource::findProjects(false));
    }

    public function display() {
      $this->set('projects', $this->_projectDataSource::findProjects(true));
    }

    public function add() {
      if(Permission::check('app.projects.add', -1)) {
        dd('User not logged in');
      }

      if(Permission::check('app.projects.add', 0)) {
        dd('User lacks right');
      }

      if($this->request->is('post')) {
        if(!$this->Projects->exists(['name' => $this->request->getData('name')])) {
          $entity = $this->Projects->newEntity([
            'name' => $this->request->getData('name'),
            'source' => '#',
            'thumbnail' => '#',
            'license' => '',
            'projects_description' => [
              'description' => $this->request->getData('description')
            ]
          ]);

          if($this->Projects->save($entity)) {
            $this->Flash->success('Project has been added');
            return $this->redirect(['plugin' => null, 'controller' => 'Projects', 'action' => 'edit', 'id' => $entity->id]);
          }

          $this->Flash->error('Something went wrong while trying to add the project');
        }
      }

      $this->set('title', 'Add Project');
    }

    public function edit($id) {
      if(Permission::check('app.projects.edit', -1)) {
        dd('User not logged in');
      }

      if(Permission::check('app.projects.edit', 0)) {
        dd('User lacks right');
      }

      if($this->request->is('post')) {
        if($this->_projectDataSource::updateProject($id, $this->request->getData())) {
          $this->Flash->success('Project has been updated');
        } else {
          $this->Flash->error('Project could not be updated');
        }
      }

      $project = $this->_projectDataSource::findProject($id, false);
      $project->releases = $this->_projectReleaseDataSource::findReleases($id, false);

      $this->set(compact('project'));
    }

    public function view($id) {
      $project = $this->_projectDataSource::findProject($id, false);

      $project->latest = [
        'public' => $this->_projectReleaseDataSource::findLatest($id, false),
        'testing' => $this->_projectReleaseDataSource::findLatest($id, true)
      ];

      // Check if the latest public version is newer than the latest testing
      // If so, don't bother with the testing version
      if($project->latest['public']->date ?? 0 >= $project->latest['testing']->date ?? 0) {
        $project->latest['testing'] = null;
      }

      $project->releases = $this->_projectReleaseDataSource::findReleases($id, true)
        ->order(['date' => 'DESC']);

      $this->set(compact('project'));
    }
  }
