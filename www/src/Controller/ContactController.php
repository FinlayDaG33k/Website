<?php
  namespace App\Controller;

  use App\Form\ContactForm;
  use Admiral\Admiral\Email;
  use Cake\Core\Configure;
  use FinlayDaG33k\AntiSpam\Token;

  class ContactController extends AppController {
    public function initialize(){
      parent::initialize();

      $this->AntispamToken = new Token();
    }

    public function index() {
      $contactForm = new ContactForm();
      
      if ($this->request->is('post')) {
        if ($contactForm->validate($this->request->getData())) {
          if($this->AntispamToken->validate($this->request->getData('antispam-token'))){
            // Build our mail
            $email = $this->_buildEmail();
    
            // Try to send the mail
            if(!$email->send()) {
              $this->Flash->error('There was an issue sending your mail. Please try again later!');
            } else {
              $this->Flash->success('I will get back to you asap!');
            }
          } else {
            $this->Flash->error('Oh oh, the antispam token you send wasn\'t valid!');
          }
        } else {
          $this->Flash->error('Please fill out the contact form properly');
        }
      }

      $this->set('contactForm', $contactForm);
    }

    private function _buildEmail(): Email {
      $email = new Email();
      $email->set('to', Configure::read('Contact.default.to'));
      $email->set('subject', '[FinlayDaG33k] '. $this->request->getData('subject'));
      
      // Add the sender information
      $email->set('from', [
        'mail' => $this->request->getData('email'),
        'name' => $this->request->getData('name'),
      ]);

      // Set some view vars
      $email->set('viewVars', [
        'content' => $this->request->getData('message'),
      ]);

      return $email;
    }
  }
