<?php
  namespace App\Controller;

  use App\Controller\AppController;
  use App\GraphQL\Data\Source\ProjectDataSource;
  use App\GraphQL\Data\Source\ProjectReleaseDataSource;
  use Cake\Event\Event;
  use Admiral\Admiral\Permission;
  use Cake\Filesystem\Folder;
  use Cake\Filesystem\File;
  use FinlayDaG33k\FileUpload\{
    FileUpload,
    Network\SimpleRequest
  };
  use Cake\I18n\Time;
  use Josegonzalez\CakeQueuesadilla\Queue\Queue;
  use Cake\Routing\Router;

  class ProjectsRestController extends AppController {
    private $_projectDataSource;
    private $_projectReleaseDataSource;

    public function initialize() {
      parent::initialize();
      $this->loadComponent('RequestHandler');
      $this->loadComponent('Json');

      $this->_projectDataSource = new ProjectDataSource();
      $this->_projectReleaseDataSource = new ProjectReleaseDataSource();

      $this->loadModel('Projects');
      $this->loadModel('ProjectsReleases');
      $this->loadModel('ProjectsChangelogs');
    }

    public function beforeFilter(Event $event) {
      // Allow all methods here
      $this->Auth->allow();
    }

    public function addRelease($project = null) {
      if(!$project) {
        $this->Json
          ->setData([
            'status' => 'failure',
            'message' => 'Project cannot be empty!',
            'result' => []
          ])
          ->setStatus(400)
          ->sendResponse();
      }

      if(!Permission::check('app.projects.releases.add', 1)) {
        $this->Json
          ->setData([
            'status' => 'failure',
            'message' => 'You are not allowed to add a release!',
            'result' => [],
          ])
          ->setStatus(403)
          ->sendResponse();
      }

      // Get the project name
      $projectName = $this->_projectDataSource::findProject($project,false)[0]->name;

      if(!$projectName) {
        $this->Json
          ->setData([
            'status' => 'failure',
            'message' => 'Project could not be found!',
            'result' => []
          ])
          ->setStatus(404)
          ->sendResponse();
      }

      $distdir = WWW_ROOT . "uploads/distfiles/";
      $distfile = new File($distdir . $this->request->getData('filename'));
      $ext = $distfile->ext();

      if(!$distfile->exists()) {
        $this->Json
          ->setData([
            'status' => 'failure',
            'message' => 'Distfile not found!',
            'result' => []
          ])
          ->setStatus(500)
          ->sendResponse();

        return;
      }

      try {
        rename($distfile->path, $distdir . ($projectName . "-" . $this->request->getData('version') . "." . $ext));
      } catch (\Exception $ex) {
        $this->Json
          ->setData([
            'status' => 'failure',
            'message' => 'Something went wrong while trying to rename the distfile',
            'result' => []
          ])
          ->setStatus(500)
          ->sendResponse();
      }
      

      $changes = [];
      foreach(json_decode($this->request->getData('changes')) as $change) {
        $changes[] = ['change' => $change];
      }

      $entity = $this->ProjectsReleases->newEntity([
        'project_id' => $project,
        'version' => $this->request->getData('version'),
        'description' => $this->request->getData('description'),
        'date' => Time::now(),
        'published' => $this->request->getData('published'),
        'projects_changelogs' => $changes
      ]);

      // Prevent an issues with the `change` keyword being reserved
      $driver = $this->ProjectsReleases->connection()->getDriver();
      $autoQuouting = $driver->isAutoQuotingEnabled();
      $driver->enableAutoQuoting(true);

      // Save the entity
      $status = $this->ProjectsReleases->save($entity);

      // Set the autoQuoting back to the original value
      $driver->enableAutoQuoting($autoQuouting);

      if(!$status) {
        $this->Json
          ->setData([
            'status' => 'failure',
            'message' => 'Could not add release!',
            'result' => []
          ])
          ->setStatus(500)
          ->sendResponse();
      }

      Queue::push('\App\Job\ProcessDistfileJob::run', [
        'app' => [
          'name' => $projectName,
          'id' => $project,
          'version_id' => $entity->id
        ],
        'distfile' => $projectName . '-' . $this->request->getData('version') . '.zip'
      ]);

      $this->Json
        ->setData([
          'status' => 'success',
          'message' => 'Release has been added!',
          'result' => []
        ])
        ->setStatus(500)
        ->sendResponse();
    }

    public function editRelease($release = null) {
      if($release == null) {
        $this->Json
          ->setData([
            'status' => 'failure',
            'message' => 'Release cannot be empty!',
            'result' => []
          ])
          ->setStatus(400)
          ->sendResponse();
      }

      if(!Permission::check('edit_releases', 1)) {
        $this->Json
          ->setData([
            'status' => 'failure',
            'message' => 'You are not allowed to edit a release!',
            'result' => []
          ])
          ->setStatus(403)
          ->sendResponse();
      }

      $entity = $this->ProjectsReleases->findById($release)->first();

      if(!$entity) {
        $this->Json
          ->setData([
            'status' => 'failure',
            'message' => 'Release not found!',
            'result' => []
          ])
          ->setStatus(404)
          ->sendResponse();
      }

      if($entity->description != $this->request->getData('description')) {
        $entity->description = $this->request->getData('description');
      }

      if($entity->published != $this->request->getData('published')) {
        $entity->published = $this->request->getData('published');
      }

      $changes = [];

      foreach(json_decode($this->request->getData('changes')) as $change) {
        if($change->id == null) {
          $changeEntity = $this->ProjectsChangelogs->newEntity();
        } else {
          $changeEntity = $this->ProjectsChangelogs->get($change->id);
        }

        if($changeEntity->change != $change->change) {
          // Delete the entity if it's change is null
          if($change->change == null) {
            $this->ProjectsChangelogs->delete($changeEntity);
            continue;
          }

          $changeEntity->change = $change->change;
        }

        $changes[] = $changeEntity;
      }

      $entity->projects_changelogs = $changes;

      $driver = $this->ProjectsReleases->connection()->getDriver();
      $autoQuouting = $driver->isAutoQuotingEnabled();
      $driver->enableAutoQuoting(true);

      // Save the entity
      $status = $this->ProjectsReleases->save($entity);

      // Set the autoQuoting back to the original value
      $driver->enableAutoQuoting($autoQuouting);

      if(!$status) {
        $this->Json
          ->setData([
            'status' => 'failure',
            'message' => 'Could not add release!',
            'result' => []
          ])
          ->setStatus(500)
          ->sendResponse();
      }

      $this->Json
        ->setData([
          'status' => 'success',
          'message' => 'Release has been updated!',
          'result' => []
        ])
        ->sendResponse();
    }

    public function addDist() {
      if(!Permission::check('edit_releases', 1)) {
        $this->Json
          ->setData([
            'status' => 'failure',
            'message' => 'You are not allowed to edit a release!',
            'result' => []
          ])
          ->setStatus(403)
          ->sendResponse();
      }

      // Get path to the folders and create any parents as needed
      $uploads = new Folder(WWW_ROOT . "uploads/distfiles/" , true);
      $temp = new Folder(TMP . DS . 'uploads', true);

      $request = new SimpleRequest();

      $fu = new FileUpload($request);
      $fu->tempFolder = $temp->path;
      $fu->uploadFolder = $uploads->path;
      $res = $fu->process();

      $this->response->statusCode($res);
    }
  }