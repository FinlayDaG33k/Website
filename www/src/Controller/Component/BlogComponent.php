<?php
  namespace App\Controller\Component;

  use Cake\Controller\Component;
  use Cake\Http\Cookie\Cookie;
  use Cake\Database\Expression\QueryExpression;
  use Cake\ORM\Query;
  use Admiral\Admiral\Permission;

  class BlogComponent extends Component {
    private $_articles;

    public function get() {
      return $this->_articles;
    }

    public function loadArticles() {
      $this->_articles = $this->getController()->Articles
        ->find()
        ->where(function (QueryExpression $exp, Query $q) {
          $published = $exp->or_(['published' => 1]);

          if($this->getController()->Auth->user()) {
            if(Permission::check('app.blog.posts.private.view', 1)) {
              $published->add(['published' => 0]);
            }

            if(Permission::check('app.blog.posts.unlisted.view', 1)) {
              $published->add(['published' => 2]);
            }
          }

          return $exp->or_([$published]);
        });
      return $this;
    }

    public function loadComments() {
      $this->_articles = $this->_articles->contain('Comments', function(Query $q) {
        // Check which comments may be seen by user
        $conditions = [['status' => 1]];
        if($this->getController()->Auth->user()) {
          $conditions[]['user_id'] = $this->getController()->Auth->user()->id;

          if(Permission::check('app.blog.comments.pending.view', 1)) {
            $conditions[]['status'] = 0;
          }

          if(Permission::check('app.blog.comments.spam.view', 1)) {
            $conditions[]['status'] = 2;
          }
        }

        // Contain the comments
        return $q
          ->where([
            'OR' => $conditions
          ]);
      });

      return $this;
    }

    public function loadTags() {
      $this->_articles = $this->_articles->contain(['Tags']);
      return $this;
    }

    public function order($conditions) {
      $this->_articles = $this->_articles->order($conditions);
      return $this;
    }
  }