<?php
  namespace App\Controller\Component;

  use Cake\Controller\Component;
  use Cake\Http\Cookie\Cookie;

  class PrivacyComponent extends Component {
    private $defaults = [
      "firstVisit" => true,
      "analytics" => true,
    ];
    private $settings;

    public function getSettings() {
      if($this->request->getCookie('userSettings')) {
        $this->settings = json_decode($this->request->getCookie('userSettings'),1);
      } else {
        $this->settings = $this->defaults;
      }

      $this->fixMissing();
      return $this->settings;
    }

    private function fixMissing() {
      foreach($this->defaults as $setting => $value) {
        if(!array_key_exists($setting, $this->settings)) {
          $this->settings[$setting] = $value;
        }
      }
    }

    public function createCookie() {
      $cookie = (new Cookie('userSettings'))
        ->withValue(json_encode($this->settings));
      return $this->response->withCookie($cookie);
    }
  }