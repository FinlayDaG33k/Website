<?php
  namespace App\Controller;

  class ResumeController extends AppController {
    private $_slogans = [
      "For people that are interested in hiring me!",
      "Which definitely does not have the same styling as the homepage!",
      "Where this subheader also changes on every visit!",
      "Which also has a redundant subheader for the sake of design!"
    ];

    private $_data = [
      'degrees' => [],
      'employers' => [
        [
          'name' => 'DevineHQ',
          'location' => null,
          'role' => 'Website Developer + System Administrator',
          'type' => 0, // Voluntary service
          'dates' => [
            'start' => 'Februari 2016',
            'end' => null // Null for present
          ],
          'description' => <<<EOT
            A company from a good friend of mine, who rocked the same boat as I did, I decided to lend him my aid to make his dream come true.
            I keep the servers up and running while also advising the CEO (my friend) on how to continue growing, while bringing awesome content to the world!
          EOT,
          'tasks' => [
            'Development of website',
            'Keeping the server running smoothly',
            'Technical advisory'
          ],
          'reference' => 'R. Voesenek',
        ],
        [
          'name' => 'WerkWent',
          'location' => 'Nijmegen',
          'role' => 'Medior Web Developer',
          'type' => 2, // Waged service
          'dates' => [
            'start' => 'October 2014',
            'end' => 'November 2019'
          ],
          'description' => <<<EOT
            I ended up here for day spending due to issues on school, but the CEO quickly recognized me as a valuable asset to their staff.
            Working on a variety of different projects (one of which is powering this site!), teaching new people how to start programming and helping them to get ready for the real work, as well as bringing new and exciting ideas to the table.
          EOT,
          'tasks' => [
            'Development of in-house CMS',
            'Substantive training of clients',
            'Technical advisory',
            'Lead developer for internal-projects'
          ],
          'reference' => null,
        ],
        [
          'name' => 'THESPIKE.GG',
          'location' => null,
          'role' => 'Medior Web Developer',
          'type' => 2, // Waged service
          'dates' => [
            'start' => 'April 2020',
            'end' => 'June 2020'
          ],
          'description' => <<<EOT
            News outlet for the videogame "Valorant".
            I worked primarily on their back-end with the occasional front-end related tasks.
            Once their site goes out of beta, I'll start shifting my focus towards streamlining the development process.
          EOT,
          'tasks' => [
            'Full-stack Development'
          ],
          'reference' => null,
        ],
        [
          'name' => 'Dorans E-Sports Championship 2022',
          'location' => 'Nijmegen',
          'role' => 'Tournament Organization',
          'type' => 0, // Voluntary service
          'dates' => [
            'start' => 'April 2022',
            'end' => 'April 2022'
          ],
          'description' => <<<EOT
            Gaming tournament hosted by the Radboud University where I volunteered to help organize the brackets for the game "Smash Ultimate".
          EOT,
          'tasks' => [
            'Technical setup of game stations',
            'Technical setup of Stream feed',
            'Game result tracking',
            'Game station assignment'
          ],
          'reference' => null,
        ],
        [
          'name' => 'LLS De Schildpad',
          'location' => 'Nijmegen',
          'role' => 'Navigator',
          'type' => 0, // Voluntary service
          'dates' => [
            'start' => 'November 2017',
            'end' => null // Null for present
          ],
          'description' => <<<EOT
            Cargoship a friend of mine owns where I help out in my spare time.
            Most often found sailing between Nijmegen and Rotterdam with the occasional trip to London.
          EOT,
          'tasks' => [
            'Navigation of the vessel',
            'Communication between vessels and mainland'
          ],
          'reference' => null,
        ],
        [
          'name' => 'Stichting Leergeld',
          'location' => 'Arnhem',
          'role' => 'Refurbisher',
          'type' => 0, // Voluntary service
          'dates' => [
            'start' => 'June 2010',
            'end' => 'August 2010'
          ],
          'description' => <<<EOT
            Charity that help out parents which are on a tight budget by allowing their children to go to school and practice sports like other children do.
            Here, I helped out refurbishing computers which where then lend to those children to do their homework on.
          EOT,
          'tasks' => [
            'Hardware Technician',
            'Software Engineer'
          ],
          'reference' => null,
        ],
        [
          'name' => 'Paradigit',
          'location' => 'Arnhem',
          'role' => 'Technical Service and Customer Support',
          'type' => 1, // Internship
          'dates' => [
            'start' => 'March 2012',
            'end' => 'March 2012'
          ],
          'description' => <<<EOT
            A (now gone) computer store in Arnhem, where I had a small internship.
          EOT,
          'tasks' => [
            'Troubleshooting customer\'s broken machines',
            'Technical Advisory'
          ],
          'reference' => null,
        ]
      ],
      'certifications' => [
        ['N/A', 'PHP/MySQL', 'EEGA Plus + NHA', 'October 14, 2022 - December 12, 2022', 'Completed'],
        ['N/A', 'Artificial Intelligence', 'EEGA Plus + ICDL Europe', 'November 23, 2022', 'Completed'],
        ['N/A', 'JSE-40-01', 'EEGA Plus + JS Institute', 'August 2022 - September 07, 2022', 'Completed'],
        ['N/A', 'IT Security', 'EEGA Plus + ICDL Europe', 'March 30, 2022 - April 20, 2022', 'Completed'],
        ['N/A', 'Data Protection', 'EEGA Plus + ICDL Europe', 'April 06, 2022 - April 20, 2022', 'Completed'],
        ['N/A', 'Using Databases', 'EEGA Plus + ICDL Europe', 'March 02, 2022 - March 16, 2022', 'Completed'],
        ['N/A', 'Driving License B', 'Dick Joosten, De Linge + CBR', 'September 2018 - October 08, 2019', 'Completed'],
        ['VMBO-T', 'Dutch language', 'S.G.M. De Brouwerij', 'September 2012 - Juli 2013', 'Completed'],
        ['VMBO-T', 'English language', 'S.G.M. De Brouwerij', 'September 2012 - Juli 2013', 'Completed'],
        ['VMBO-T', 'Basics of Biology', 'S.G.M. De Brouwerij', 'September 2012 - Juli 2013', 'Completed'],
        ['MBO-2', 'IT servicedesk', 'S.G.M. De Brouwerij', 'September 2013 - June 2014', 'Dropped'],
        ['N/A', 'Linux professional', 'LOI', 'March 2018 - March 2019', 'Dropped'],
        ['N/A', 'CompTIA A+', 'Werkwent Nijmegen', 'September 2014 - Oktober 2014', 'Dropped'],
      ],
      'skills' => [
        'programming' => [
          ['PHP', 'Advanced'],
          ['HTML', 'Advanced'],
          ['JavaScript', 'Advanced'],
          ['TypeScript', 'Advanced'],
          ['Python', 'Intermediate'],
          ['CSS', 'Intermediate'],
          ['Java', 'Novice'],
          ['R', 'Novice'],
          ['C/C++', 'Fundamental Awareness - Novice'],
        ],
        'operating systems' => [
          ['Ubuntu Linux', 'Advanced'],
          ['SolusOS', 'Advanced'],
          ['Proxmox VE', 'Intermediate'],
          ['Windows', 'Intermediate'],
        ],
        'misc' => [
          ['Physical Security', 'Advanced'],
          ['CakePHP', 'Intermediate'],
          ['Docker', 'Intermediate'],
          ['Naval Code', 'Intermediate'],
          ['Psychology', 'Intermediate'],
          ['Natural Sciences', 'Novice - Intermediate'],
          ['Chemistry', 'Novice - Intermediate'],
          ['Electronic Engineering', 'Novice - Intermediate'],
          ['Git', 'Novice - Intermediate'],
          ['First-Aid', 'Novice'],
        ],
        'languages' => [
          'english' => 80,
          'dutch' => 60,
          'german' => 25
        ]
      ],
      'software' => [
        'Deno',
        'IntelliJ IDEA Ultimate',
        'EmberJS',
        'Docker',
        'Redis',
        'CakePHP',
        'Manjaro Linux',
        'Ubuntu',
        'Bash',
        'MariaDB',
        'Bootstrap 4',
        'Proxmox VE',
        'OBS Studio',
        'GitAhead',
        'Electron',
        'HitFilm Express',
        'Insomnia',
      ],
      'projects' => [
        [
          'name' => 'Admiral',
          'description' => <<<EOL
            Admiral is a CMS built for CakePHP built with the idea to be extremely modular while not bothering end-users with things they should not need to be dealing with.
            Behind the scenes, it is what is powering my website and it's fully open-source under the GNU-GPLv2 license.
          EOL,
          'repo' => 'https://www.gitlab.com/admiralcms/admiral'
        ],
        [
          'name' => 'Yukikaze',
          'description' => <<<EOL
            Yukikaze is a Discord bot written for the Das Propaganda Maschine Discord server.
            She was built due to the need of one single bot, that could fulfill all the requirements we put on it.
          EOL,
          'repo' => 'https://gitlab.com/FinlayDaG33k/yukikaze'
        ],
        [
          'name' => 'Ninym',
          'description' => <<<EOL
            Ninym is a data-aggregation back-end to help combine data from a variety of sources (primarily iCAL, crypto tickers, OpenWeatherMap and the UPS powering my server) into one easily accessible stream using a custom-made dashboard.
            She is designed with modularity in mind, allowing me to add and remove modules as needed.
          EOL,
          'repo' => 'https://gitlab.com/FinlayDaG33k/ninym/'
        ],
        [
          'name' => 'deno-lib',
          'description' => <<<EOL
            Deno-lib is the result of the need to re-use code between different projects.
            To a certain degree, it can be regarded as a micro-framework for use in Deno projects.
          EOL,
          'repo' => 'https://github.com/FinlayDaG33k/deno-lib'
        ],
        [
          'name' => 'OpenSauce',
          'description' => <<<EOL
            Not exactly a code-based project but rather a repository to share recipes of food and drinks for people to make.
          EOL,
          'repo' => 'https://gitlab.com/FinlayDaG33k/opensauce'
        ],
        [
          'name' => 'GogoDl',
          'description' => <<<EOL
            GogoDl is a free open-source tool made for friends to download anime of the site "Gogoanime" for free.
            It is a port of "9AnimeDl" that uses Gogoanime instead of 9Anime.
          EOL,
          'repo' => 'https://www.gitlab.com/finlaydag33k/gogodl'
        ],
        [
          'name' => '9AnimeDl',
          'description' => <<<EOL
            9AnimeDl is a free open-source tool made for friends to download anime of the site "9Anime" for free.
            The legacy version (version 1) was written in C# with WPF but since the release of version 2 in December of 2019, it is written with Angular 8 and Electron.
          EOL,
          'repo' => 'https://www.gitlab.com/finlaydag33k/9animedl'
        ]

      ],
    ];

    public function initialize() {
      parent::initialize();

      $this->loadComponent('RequestHandler');
    }

    public function index() {
      $this->set('slogans', $this->_slogans);
      $this->set('data', $this->_data);
    }

    public function view() {
      $this->_data['software'] = array_splice($this->_data['software'], 0, 5);
      sort($this->_data['software']);
      $this->set('data', $this->_data);
      $this->viewBuilder()->layout('pdf');
    }
  }
