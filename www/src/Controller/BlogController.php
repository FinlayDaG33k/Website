<?php
  namespace App\Controller;

  use Admiral\Admiral\Permission;
  use Cake\Http\Exception\NotFoundException;
  use Cake\I18n\Time;
  use Cake\Filesystem\File;
  use Cake\Core\Configure;

  class BlogController extends AppController {
    public function initialize() {
      parent::initialize();

      // Load the required models
      $this->loadModel('Admiral/Blog.Articles');
      $this->loadModel('Admiral/Admiral.Users');
      $this->loadModel('Admiral/Blog.ArticlesTags');
      $this->loadModel('Admiral/Blog.ArticlesRevisions');
      
      // Load the required components
      $this->loadComponent('Paginator');
      $this->loadComponent('Blog');
      $this->loadComponent('Privacy');
    }

    public function display($query = null) {
      $articles = $this->Blog
        ->loadArticles()
        ->order(['created' => 'DESC'])
        ->loadTags()
        ->loadComments()
        ->get();
      
      if($query != null) {
        $articles->where([
          'OR' => [
            'title LIKE' => "%{$query}%",
            'slug LIKE' => "%{$query}%"
          ]
        ]);
      }

      $articles = $this->paginate($articles,['limit' => 5]);
      $this->set(compact('articles'));
    }

    public function view($slug = null, $hash = null) {
      // Find the first article matching our slug
      // Also load in our tags and comment chain
      $article = $this->Articles
        ->findBySlug($slug)
        ->contain([
          'Tags',
          'Comments'=> [
            'Reply' => ['Users'],
            'Users'
          ]
        ])
        ->first();

      // Throw a NotFoundException if no article was found
      if(!$article) throw new NotFoundException;

      if($article->published != 1) {
        // Throw a NotFoundException if the user is not logged in
        if(!$this->Auth->user()) throw new NotFoundException;

        // Check the state of the published article
        // If private (0) throw a NotFoundException
        // If unlisted (2) throw a NotFoundException
        // If invalid throw a NotFoundException
        switch($article->published) {
          case 0:
            if(!Permission::check('app.blog.posts.private.view', 1)) throw new NotFoundException;
            break;
          case 2:
            if(!Permission::check('app.blog.posts.private.view', 1)) throw new NotFoundException;
            break;
          default:
            throw new NotFoundException;
            break;
        }
      }

      // Open the file handle
      if($hash) {
        $file = new File(ROOT . DS . 'blog-posts' . DS . $hash . '.txt');
      } else {
        $file = new File(ROOT . DS . 'blog-posts' . DS . $article->hash . '.txt');
      }

      // Load our comments
      $children = $article->comments;

      // Check if the post hasn't been modified in a year
      // If so, display a warning
      $now = new Time();
      if($article->modified->addYears(1) <= $now) $this->Flash->warning(
        'This post was last modified over a year ago and as such, information in it may be outdated (or the post may be outright broken).'
      );

      // Pass our view vars
      $this->set(compact('article'));
      $this->set('body', $file->read());
      if(!Configure::read('debug')) {
        $this->set('shortcodeCache', 'blog-rendered-' . (!empty($hash) ? $hash : $article->hash));
      } else {
        $this->set('shortcodeCache', null);
      }
    }
  }