<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Event\Event;
use Cake\Utility\Inflector;

// Webhooks
use App\Webhook\GetGitlabBinary;

class WebhookController extends AppController {
  public function initialize() {
    parent::initialize();
    $this->loadComponent('Csrf');
    $this->loadComponent('Json');
  }

  public function beforeFilter(Event $event) {
    $this->Auth->allow();
    $this->getEventManager()->off($this->Csrf);
  }

  public function index() {
    // Only allow POST requests
    if(!$this->request->is('post')) {
      $this->Json->setStatus(403)->setData([
        'errors' => [
          'Only POST requests are allowed for webhooks'
        ]
      ])
      ->sendResponse();
    }

    // Get the Hook class
    $hook = $this->request->getQuery('hook');
    $r = new \ReflectionClass('App\\Webhook\\' . Inflector::classify($hook));

    // Make sure the class is not abstract
    if($r->isAbstract()) {
      $this->Json->setStatus(500)->setData([
        'errors' => [
          'Class "App\\Webhook\\' . Inflector::classify($hook) . '" cannot be abstract'
        ]
      ])
      ->sendResponse();
    }
    
    // Instanciate the class and run it's run() method
    $res = $r->newInstance()->run($this);
    
    // Return the json response
    $this->Json
      ->setStatus($res['status'])
      ->setData($res['info'])
      ->sendResponse();
  }
}