<?php
namespace App\Controller;

use App\Util\H2Push;
use App\Util\Preload;

use Cake\Controller\Controller;
use Cake\Event\Event;
use Cake\Utility\Inflector;

class AppController extends Controller {
  protected $userSettings;

  public function initialize() {
    parent::initialize();

    // Trust the proxy with getting the IPs
    $this->request->trustProxy = true;

    $this->loadModel('Admiral/Admiral.Options');

    // Load components
    $this->loadComponent('RequestHandler', [
      'enableBeforeRedirect' => false,
    ]);
    $this->loadComponent('Flash');
    $this->loadComponent('Auth', [
      'loginAction' => [
        'plugin'=>null,
        'controller' => 'Users',
        'action' => 'login',
        'login'
      ],
      'logoutRedirect' => [
        'plugin'=>null,
        'controller' => 'pages',
        'action' => 'display',
        'home'
      ]
    ]);
    $this->loadComponent('Privacy');
    $this->loadComponent('Admiral/Admiral.User');
    $this->loadComponent('Admiral/Admiral.SecurityHeader');

    // Check if the user is logged in
    // If not, try to auto-login the user
    if(!$this->Auth->user()){
      // Try to autologin the user
      $this->loadComponent('Admiral/Admiral.AutoLogin');
      $this->Auth->setUser(
        $this->AutoLogin->login($this->request->getCookie('remember_me'))
      );
    }

    // If the user is logged in, her his/her details
    if($this->Auth->user()) $this->User->loadDetails();

    // Set the user
    $this->set('user', $this->Auth->User());

    // Get the userSettings
    $this->userSettings = $this->Privacy->getSettings();
    $this->set('user_settings',$this->userSettings);

    $this->response = $this->Privacy->createCookie();
  }

  public function beforeFilter(Event $event) {
    // Allow the following actions to be called without authentication
    $this->Auth->allow(['index','view','display']);
  }

  public function shutdownProcess() {
    // Get our HTTP2/PUSH header
    //$header = H2Push::get();

    // Get our HTTP Preload header
    $header = Preload::get();

    // Check if our header contains a string
    // If so, add our Link header
    if(!empty($header)) $this->response = $this->response->withAddedHeader('Link', $header);

    // Set our permission headers
    $this->SecurityHeader
      ->setPermission('interest-cohort')
      ->getPermissions();

    // Enable HSTS
    $this->SecurityHeader->setHsts(true, false, true, 63072000);

    // Set our Referrer policy
    $this->SecurityHeader->setReferrerPolicy('no-referrer-when-downgrade');

    // Disallow iFrames
    $this->SecurityHeader->setXFrame('DENY');

    // Do not sniff MIME-types
    $this->SecurityHeader->sniffMime(true);

    // Add Content Security Policies
    $this->SecurityHeader
      ->setCsp('default-src', 'self')
      ->setCsp('img-src', 'self')
      ->setCsp('script-src', 'self')
      ->setCsp('style-src', 'self')
      ->setCsp('img-src', 'https://ipfs.finlaydag33k.nl')
      ->setCsp('script-src', 'unsafe-eval')
      ->setCsp('style-src', 'unsafe-inline')
      ->getContentPolicies();
  }
}
