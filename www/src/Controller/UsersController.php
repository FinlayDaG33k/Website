<?php
  namespace App\Controller;

  use Admiral\Admiral\Controller\Base\UsersBaseController;
  use App\Form\RegisterForm;
  use App\Form\LoginForm;
  use App\Form\UserDetailsForm;
  use Cake\Event\Event;
  use Cake\Auth\DefaultPasswordHasher;
  use Cake\Routing\Router;
  use Cake\I18n\Time;
  use App\Util\Formatters;
  use App\GraphQL\Data\Source\HibpDataSource;
  use Admiral\Admiral\Email;
  use Cake\Utility\Security;

  class UsersController extends UsersBaseController {
    public function beforeFilter(Event $event) {
      parent::beforeFilter($event);
    }

    public function initialize(): void {
      parent::initialize();
    }

    public function register(){
      if($this->Auth->user('id')) {
        $this->redirect(['action' => 'my_account', 'my-account']);
      }

      parent::register();

      // Set the page title
      $this->set('userSettings', $this->userSettings);
    }

    public function login(){
      if($this->Auth->user('id')) {
        $this->redirect(['action' => 'my_account', 'my-account']);
      }
      parent::login();
    }

    public function logout(){
      parent::logout();

      return $this->redirect($this->Auth->logout());
    }

    public function myAccount($id = null){
      // check if logged in, if not redirect to the login page
      if(!$this->Auth->user('id')) {
        $this->redirect(['controller' => 'Users', 'action' => 'login', 'login']);
      }

      parent::myAccount($this->Auth->user('id'));

      $this->set('userSettings', $this->userSettings);
    }

    public function resetPassword() {
      // Check if the user is already logged in
      if($this->Auth->user()) {
        // User is logged in
        // Redirect to the my_account page instead
        return $this->redirect(['controller' => 'Users', 'action' => 'my_account', 'my-account']);
      }

      $this->set('token',$this->request->getParam('pass')[0]);

      parent::resetPassword();
    }

    public function forgotPassword(){
      // Check if the user is already logged in
      if($this->Auth->user()) {
        // User is logged in
        // Redirect to the my_account page instead
        return $this->redirect(['controller' => 'Users', 'action' => 'my_account', 'my-account']);
      }

      parent::forgotPassword();

      $this->set('userSettings', $this->userSettings);
    }
  }
