<div class="row" id="intro">
  <div class="col-md-12">
    <div class="jumbotron" id="intro-jumbotron">
      <h1 class="display-3 text-danger">Well that didn't work out...</h1>
      <p class="lead text-danger">
        It seems like you've hit a dead end
      </p>
    </div>             
  </div>
</div>
<div class="container"> 
  <div class="row">
    <div class="col-12">
      <p>
        I couldn't find the page you tried to request.<br />
        Don't worry, I've already been informed about this and will look into it :)<br />
      </p>
    </div>
  </div>
</div>