<div class="nobr">
  <!-- Score -->
  <?php for($i=1; $i <= $rating; $i++): ?>
    <span class="fa fa-cog checked"></span>
  <?php endfor; ?>

  <!-- "padding" for cleaner looks -->
  <?php for($i=1; $i <= 5 - $rating; $i++): ?>
    <span class="fa fa-cog" style="opacity:0.5;"></span>
  <?php endfor;?>
</div>