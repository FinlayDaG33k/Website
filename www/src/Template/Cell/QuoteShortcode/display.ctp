<blockquote class="quote-card">
  <p>{{ the_content|raw }}</p>
  <?php if(!empty($source)): ?>
    <cite><?= h($source); ?></cite>
  <?php endif; ?>
</blockquote>