<div class="row mb-2">
  <div class="col col-sm-12" style="max-width: <?= $dimensions['max-width']; ?>px;">
    <div class="card rounded-0">
      <a href="<?= $urls['full']; ?>" target="_blank">
        <img class="card-img-top rounded-0" data-srcset="<?= $urls['set']; ?>">
        <div class="img-loader text-center" style="height: <?= $dimensions['max-height']; ?>px;">
          <?= $this->element('sk-cube-spinner'); ?>
          <br />
          Hang on tight while we're loading this image!
        </div>
        <div class="img-error text-center" style="display: none;">
          Something went wrong while loading this image :&lt;
        </div>
      </a>
      <?php if(!empty($caption)): ?>
        <div class="card-body">
          <p class="card-text"><?= h($caption); ?></p>
        </div>
      <?php endif; ?>
      <?php if(!empty($source)): ?>
        <div class="card-footer">
          <p class="card-text">
            <a href="<?= $source; ?>" target="_blank">Image source</a>
          </p>
        </div>
      <?php endif; ?>
    </div>
  </div>
</div>