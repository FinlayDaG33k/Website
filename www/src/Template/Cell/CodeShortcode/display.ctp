<pre class="line-numbers language-<?= $lang; ?> code-toolbar">
  <code class="language-<?= $lang; ?>">
    {{ the_content|raw }}
  </code>
</pre>