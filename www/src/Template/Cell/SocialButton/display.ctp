<a href="<?= $link; ?>" target="_blank" class="btn btn-primary social-button rounded-0 mr-1 mb-1" rel="noopener">
  <svg class="social-icon">
    <use xlink:href="/img/brands/<?= $icon; ?>.svg#icon"></use>
  </svg>
</a>