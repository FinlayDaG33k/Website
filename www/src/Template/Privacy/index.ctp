<?php $this->pageTitle = 'Privacy Settings'; ?>

<div class="row no-banner">
  <div class="container">
    You're probably not used to this, but I actually value freedom of choice.<br /> 
    Therefore, I made a lot of stuff optional here.<br /> 
    On this page, you can choose whether you want certain stuff to be enabled or not.
  </div>
</div>
<div class="row">
  <div class="container">
    <?= $this->element('privacy'); ?>
  </div>
</div>
<div class="row">
  <div class="container">
    
  </div>
</div>