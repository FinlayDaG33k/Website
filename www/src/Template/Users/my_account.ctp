<?php $this->pageTitle = 'My Account'; ?>

<div class="container">
  <div class="row no-banner">
    <div class="col-md-6">
      <h4>Manage FIDO2 Tokens</h4>
      <!-- New token registration -->
      <form data-action="register-token">
        <div class="input-group mb-3">
        <input type="text" class="form-control rounded-0" placeholder="Enter a token name" name="token-name"> 
          <div class="input-group-append">
            <button class="btn btn-primary rounded-0" type="submit"><i class="fas fa-plus"></i></button>
          </div>
        </div>
      </form>

      <!-- Registered Tokens -->
      <table class="table">
        <thead>
          <th>Token Name</th>
          <th>Actions</th>
        </thead>
        <tbody>
          <?php foreach($authenticationKeys as $name => $data): ?>
            <tr>
              <td data-toggle="tooltip" data-placement="bottom" title="<?= $this->WebauthnKey->fingerprint($data->getPublicKeyCredentialId()); ?>">
                <?php if(!empty($name)): ?>
                  <?= h($name); ?>
                <?php else: ?>
                  <?= h($this->WebauthnKey->fingerprint($data->getPublicKeyCredentialId())); ?>
                <?php endif; ?>
              </td>
              <td>
                <button class="btn btn-danger btn-xs" data-title="Delete">
                  <span class="far fa-trash-alt"></span>
                </button>
              </td>
            </tr>
          <?php endforeach; ?>
        </tbody>
      </table>
    </div>
    <div class="col-md-6">
      <h4>Your Details</h4>
      <?= $this->Flash->render('user_details'); ?>
                
      <?= $this->Form->create($userDetailsForm,["url"=>"/my-account","class"=>"userdetails-form","id"=>"userdetails-form"]); ?>
        <div class="form-group">
          <?= $this->Form->control("firstname",["placeholder"=>"First Name","label"=>false,"class"=>"form-control rounded-0","value" => $userData['users_detail']['firstname']]); ?>
        </div>
        <div class="form-group">
        <?= $this->Form->control("lastname",["placeholder"=>"Last Name","label"=>false,"class"=>"form-control rounded-0","value" => $userData['users_detail']['lastname']]); ?>
        </div>
        <div class="form-group">
          <?= $this->Form->hidden('action',["value" => "user_details"]); ?>
          <?= $this->Form->button('Submit',["class"=>"btn btn-primary bg-primary-normal rounded-0"]); ?>
        </div>
      <?= $this->Form->end(); ?>
    </div>
  </div>
</div>