<?php $this->pageTitle = 'Login'; ?>

<div class="container">
  <div class="row no-banner">
    <div class="col-md-6">
      <h4>Login</h4>
      <?= $this->Flash->render(); ?>
                
      <form data-action="user-login">
        <div class="form-group">
          <?= $this->Form->control("username",["placeholder"=>"Username","label"=>false,"class"=>"form-control rounded-0"]); ?>
        </div>
        <div class="form-group">
          <?= $this->Form->checkbox('remember_me',['id'=>'remember_me']); ?>
          <label for="remember_me">Remember Me</label>
        </div>
        <div class="form-group">
          <button type="submit" class="btn btn-success btn rounded-0" data-method="token"><?= __d('Admiral/Admiral','Security Token'); ?></button>
          <button type="submit" class="btn btn-primary rounded-0" data-method="email"><?= __d('Admiral/Admiral','Email'); ?></button>
        </div>
        <div class="form-group">
          Don't have an account? <?= $this->Html->link('Create one', ['controller' => 'Users', 'action' => 'register', 'register'], ['class'=> 'text-primary']); ?>
        </div>
      </form>

      <form data-action="email-login" class="d-none">
        <div class="form-group">
          <?= $this->Form->control("code",["placeholder"=>'Enter your login code',"required"=>true,"label"=>false,"class"=>"form-control form-control-lg rounded-0"]); ?>
          <div class="invalid-feedback">Enter your login code</div>
        </div>
        <div class="form-group">
          <button type="submit" class="btn btn-primary btn-lg rounded-0">Login</button>
          <button type="reset" class="btn btn-danger btn-lg rounded-0">Cancel</button>
        </div>
      </form>
    </div>
    <div class="col-md-6">
      <h4>Security note</h4>
      Before you login into your site, make sure the url bar has a green lock.<br />
      If that's the case you should be fine to login, although nowadays the green lock is no guarrantee you are safe.<br />
      <br />
      My website doesn't use passwords, instead, you'll get to choose between either of two methods to authenticate.<br />
      <ul class="p-0 ml-3 mb-0">
        <li>Security Token using FIDO2 (eg. YubiKey or NitroKey FIDO2)</li>
        <li>One-Time Password send to your registered email</li>
      </ul>
      <br />
      Checking the "Remember me" box will automatically logged in if your session expires.<br />
      Please do not check this box if you are using a shared computer.
    </div>
  </div>
</div>