<?php $this->pageTitle = 'Register'; ?>

<div class="container">
  <div class="row no-banner">
    <div class="col-md-6">
      <h4>Register</h4>
      <?= $this->Flash->render(); ?>
                
      <?= $this->Form->create($register,["url"=>"/register","class"=>"register-form","id"=>"register-form"]); ?>
      <div class="form-group">
          <?= $this->Form->control("username",["placeholder"=>"Enter your awesome username","label"=>false,"class"=>"form-control rounded-0"]); ?>
        </div>
        <div class="form-group">
          <?= $this->Form->control("email",[
            "placeholder"=>"Enter your email address",
            "label"=>false,
            "class"=>"form-control rounded-0",
            "title"=>"I'll promise to never share your email with anyone else.",
            "data-toggle"=>"tooltip",
            "data-placement"=>"bottom"
          ]); ?>
        </div>
        <?= $this->Form->button('Submit',["class"=>"btn btn-primary rounded-0"]); ?>
        <?= $this->Form->button('Reset',["type"=>"reset","class"=>"btn btn-danger rounded-0"]); ?>
        <div class="form-group">
          Already have an account? <?= $this->Html->link('Login instead', ['controller' => 'Users', 'action' => 'login', 'login'], ['class'=> 'text-primary']); ?>
        </div>
      <?= $this->Form->end(); ?>
    </div>
    <div class="col-md-6">
      <h4>Why register?</h4>
      Registration on my website is completely optional, however, it adds some additional benefits over non-registered users.<br />
      For example, you will be informed about new blogposts whenever I post one (can be disabled) and you'll be able to partake in giveaways and discussions on the site!<br />
      Oh yea, and you'll get free access to testing releases for my project!<br />
      <br />
      I promise you to not purposefully bother you or sell the data you enter but do note that your username will be public when you go around and join the discussions!<br />
    </div>
  </div>
</div>