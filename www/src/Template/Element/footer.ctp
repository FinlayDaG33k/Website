<div class="container">
  <div class="row extra">
    <div class="col-md-3">
      <h3>Advertisement</h3> 
      <p> <!-- Place Non-Tracking Ad here--> </p> 
    </div>
    <div class="col-md-3" id="legal-stuff">
      <h3>Nerdy stuff</h3>
      <p>
        <ul>
          <li>
            <a href="https://status.finlaydag33k.nl/" target="_blank" rel="noopener">Service Incidents</a>
          </li>
          <li>
            <?= $this->Html->link('GraphQL API',[
              '_name' => 'graphql:documentation'
            ]); ?>
          </li>
        </ul>
      </p>
    </div>
    <div class="col-md-3" id="legal-stuff">
      <h3>Legal</h3> 
      <p>
        <ul>
          <li><?= $this->Html->link('Privacy Settings',['plugin'=>null,"controller"=>"Privacy","action"=>"index",'privacy']); ?></li>
          <li><?= $this->Html->link('Pentesting',['plugin'=>null,"controller"=>"Pages","action"=>"display",'pentesting']); ?></li>
          <li><?= $this->Html->link('Review Policy',['plugin'=>null,"controller"=>"Pages","action"=>"display",'reviews']); ?></li>
        </ul>
      </p> 
    </div>
    <div class="col-md-3" id="legal-stuff">
      <h3>Around the Web</h3>
      <p>
        <?= $this->cache(function() {
          echo $this->cell('SocialButton',[
            'link' => 'https://www.reddit.com/r/finlaydag33k',
            'icon' => 'reddit'
          ]);
          echo $this->cell('SocialButton',[
            'link' => 'https://social.linux.pizza/@finlaydag33k',
            'icon' => 'mastodon'
          ]);

          echo $this->cell('SocialButton',[
            'link' => 'https://www.youtube.com/finlaydag33k',
            'icon' => 'youtube'
          ]);

        }, ['key' => 'SocialButtons']); ?>
      </p>
    </div>
  </div>
  <div class="row copyright">
    <div class="col-md-12">
      <span>
        &copy; 2018-<?= $this->Time->format(Cake\I18n\Time::now(),'YYYY'); ?> 
        <?= $this->Html->link('Aroop "FinlayDaG33k" Roelofs', ['plugin'=>null,'controller' => 'Pages', 'action' => 'display', 'home']) ?>
        - I <span class="copyright-icon oven"></span> with <a href="https://cakephp.org" rel="noopener"><span class="copyright-icon cake"></span></a> - 
        Powered by <?= $this->Html->link('Admiral', 'https://www.gitlab.com/admiralcms/admiral', ['target' => '_blank', 'rel' => 'noopener']); ?>
      </span> 
    </div>
  </div>
</div>
<a href="#" class="scroll-up"><i class="fa fa-chevron-up"></i></a>