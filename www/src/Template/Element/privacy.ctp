<div id="accordion">
  <div class="card rounded-0 mb-2">
    <div class="card-header" id="headingOne" data-toggle="collapse" data-target="#collapseOne">
      Functional Cookies
      <label class="switch float-right">
        <input type="checkbox" class="primary" data-action="privacy-toggle" disabled checked>
        <span class="slider"></span>
      </label>
    </div>
    <div id="collapseOne" class="collapse" aria-labelledby="headingOne" data-parent="#accordion">
      <div class="card-body">
        I put some basic required cookies in your browser to have this site function properly.<br />
        These cookies include:
        <ul>
          <li>Session cookies</li>
          <li>Settings cookies</li>
        </ul>
        These cookies can't and shouldn't be disabled.
      </div>
    </div>
  </div>
  <div class="card rounded-0 mb-2">
    <div class="card-header" id="headingFour" data-toggle="collapse" data-target="#collapseFour">
      Analytics
      <label class="switch float-right">
        <input type="checkbox" class="primary" data-action="privacy-toggle" data-toggle="toggle" data-setting="analytics" <?php if($user_settings['analytics']){ ?>checked<?php } ?>>
        <span class="slider"></span>
      </label>
    </div>
    <div id="collapseFour" class="collapse" aria-labelledby="headingFour" data-parent="#accordion">
      <div class="card-body">
        Analytics help me to see how my site is doing, how far my reach is, how often people come back etc. etc.<br />
        For analytics, I use a custom, fully <?= $this->Html->link('open-source solution', 'https://gitlab.com/FinlayDaG33k/admiral-analytics', ['target' => '_blank', 'rel' => 'noopener']); ?> that hooks directly into my CMS, Admiral.<br />
        By doing this, I can keep your data on my own servers instead of having it hang around at third-party providers (whom will use it to track you and sell you stuff) while also being able to offer full transparency about what I do with the data I gather..<br />
        Once you leave my site, my website won't track you.<br />
        All data obtained is data that are being sent by the client during a request anyways.<br />
        Ofcourse, I do understand that you might not want to be tracked at all, therefore, I allow you to completely disable my analytics as long as you want!<br />

        <h1>How is your data collected and processed?</h1>
        <p>
          It collects your data by loading a small script that sends a tiny request to my server for the data collection.<br />
          Data collected by this includes (and is currently limited to):
          <ul>
            <li>A unique identifier</li>
            <li>Your IP address</li>
            <li>Your Country (not yet implemented)</li>
            <li>Your browser's User Agent</li>
            <li>The page you are on</li>
            <li>The page where you originate from</li>
            <li>The user's settings (the thingy above)</li>
            <li>The timestamp of collection</li>
          </ul>
          This data is not personally identifiable as-is and only becomes so when you explicitly tell me.
        </p>
        <p>
          This data shall be used for the following purposes:
          <ul>
            <li>See which pages on my sites are the most popular</li>
            <li>See where from the globe my visitors originate from</li>
            <li>See what platforms people use to visit my website</li>
            <li>See what settings people use</li>
            <li>See how much visitors I have within a given timespan</li>
          </ul>

          But shall not be used for these purposes:
          <ul>
            <li>Selling data to 3rd party advertisers</li>
            <li>Track you outside my website</li>
          </ul>
        </p>
      </div>
    </div>
  </div>
</div>
