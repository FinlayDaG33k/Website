<div class="modal" tabindex="-1" role="dialog" id="firstTimeVisitModal">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content rounded-0">
      <div class="modal-header">
        <h5 class="modal-title">Setup preferences</h5>
      </div>
      <div class="modal-body">
        <p>
          It seems like this is the first time you visit this site (or you wiped your cookies)!<br />
          Because of that, you are now seeing this little pop-up!<br />
          This site is one of the few sites that gives you the ability to customize a things.<br />
          Just enable or disable things below as you wish and hit "I'm done".<br />
        </p>
        <p>
          <b>Note:</b> In order to ensure proper function of the site, I ask you to <u>disable</u> any adblocker you may have.<br />
          Since you can disable stuff like analytics in this screen, there is no real reason to have it running (outside of causing potential issues).
        </p>
        <p>
          <?= $this->element('privacy'); ?>
        </p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary rounded-0">I'm done</button>
      </div>
    </div>
  </div>
</div>
