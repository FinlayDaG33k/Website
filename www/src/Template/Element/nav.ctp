<section id="navbar">
  <section id="navbar-greyline"></section>
  <div class="container">
    <nav class="navbar navbar-expand-lg"> 
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarToggler42" aria-controls="navbarTogglerDemo01" aria-expanded="false" aria-label="Toggle navigation"> 
        <svg xmlns="http://www.w3.org/2000/svg" width="32" height="32" style="--primary-normal">
          <path fill="none" style="stroke:var(--primary-normal)" stroke-width="2" d="M4 8h24M4 16h24M4 24h24"/>
        </svg>
      </button>               
      <div class="collapse navbar-collapse" id="navbarToggler42"> 
        <ul class="navbar-nav mr-auto mt-2 mt-lg-0"> 
          <li class="nav-item <?php echo ($this->request->_matchedRoute == '/') ? 'active': ''; ?>"> 
            <?= $this->Html->link('Home', ['plugin'=>null,'controller' => 'Pages', 'action' => 'display', 'home'], ['class' => 'nav-link']) ?>
          </li>    
          <li class="nav-item <?php echo (fnmatch('/blog*',$this->request->_matchedRoute)) ? 'active': ''; ?>"> 
            <?= $this->Html->link('Blog', ['plugin'=>null,'controller' => 'Blog', 'action' => 'display'], ['class' => 'nav-link']) ?>
          </li>
          <li class="nav-item dropdown <?php echo (fnmatch('/about*',$this->request->_matchedRoute)) ? 'active': ''; ?>"> 
            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">About</a> 
            <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink"> 
              <?= $this->Html->link('My Story', ['plugin'=>null,'controller' => 'Pages', 'action' => 'display', 'about'], ['class' => 'dropdown-item']) ?>
              <div class="dropdown-divider"></div>
              <?= $this->Html->link('My Resume', ['plugin'=>null,'controller' => 'Resume', 'action' => 'index', 'resume'], ['class' => 'dropdown-item']) ?>
              <div class="dropdown-divider"></div>
              <?= $this->Html->link('Website Sourcecode', 'https://www.gitlab.com/finlaydag33k/website', ['class' => 'dropdown-item']) ?>
            </div>                     
          </li>
          <li class="nav-item dropdown"> 
            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i id="livestream-liveicon" class="fas fa-video breathing-live d-none"></i> Livestream</a> 
            <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink" id="twitch-dropdown"> 
                <a class="dropdown-item" href="https://twitch.tv/finlaydag33k" target="_blank" rel="noopener">
                  <i class="fab fa-twitch"></i> Visit on Twitch
                </a>
            </div>                     
          </li> 
          <li class="nav-item <?php echo ($this->request->_matchedRoute == '/contact') ? 'active': ''; ?>"> 
            <?= $this->Html->link('Contact', ['plugin'=>null,'controller' => 'Contact', 'action' => 'index', 'contact'], ['class' => 'nav-link']) ?>
          </li>  
          <!--
          <li class="nav-item <?php echo ($this->request->_matchedRoute == '/turtle-stream') ? 'active': ''; ?>"> 
            <?= $this->Html->link($this->Html->tag('span','',['class' => 'align-middle glyphicons glyphicons-turtle']).' Live Turtles!',['plugin'=>null,'controller' => 'Pages', 'action' => 'display', 'turtle-stream'],['class' => 'nav-link','escape'=>false]);?>
          </li>
          -->
          <li class="nav-item dropdown"> 
            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Account</a> 
            <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink"> 
              <?php if($user): ?>
                <a class="dropdown-item" href="#">Welcome, <?= h($user->user_details->firstname); ?></a>
                <?php if(\Admiral\Admiral\Permission::check('admiral.admiral.cms.access', 1)): ?>
                  <div class="dropdown-divider"></div>
                  <?= $this->Html->link('Admin Dashboard', ['plugin'=>'Admiral/Admiral','controller' => 'Admin', 'action' => 'index'], ['class' => 'dropdown-item']) ?>
                <?php endif; ?>
                <?= $this->Html->link('My Account', ['plugin'=>null,'controller' => 'Users', 'action' => 'my_account', 'my-account'], ['class' => 'dropdown-item']) ?>
                <div class="dropdown-divider"></div>
                <?= $this->Html->link('Log out', ['plugin'=>null,'controller' => 'Users', 'action' => 'logout', 'logout'], ['class' => 'dropdown-item']) ?>
              <?php else: ?>
                <?= $this->Html->link('Login', ['plugin'=>null,'controller' => 'Users', 'action' => 'login'], ['class' => 'dropdown-item']) ?>
                <?= $this->Html->link('Register', ['plugin'=>null,'controller' => 'Users', 'action' => 'register', 'register'], ['class' => 'dropdown-item']) ?>
              <?php endif; ?>
            </div>                     
          </li>
          <li class="nav-item <?php echo ($this->request->_matchedRoute == '/donate') ? 'active': ''; ?>">
            <?= $this->Html->link('Donate', ['plugin'=>null,'controller' => 'Pages', 'action' => 'display', 'donate'], ['class' => 'nav-link']); ?>
          </li>
        </ul>                 
        <form id="search-form" class="form-inline my-2 my-lg-0"> 
          <div class="input-group">
            <input class="form-control rounded-0" type="search" placeholder="Search">
            <div class="input-group-append">
              <button class="btn btn-outline-secondary" type="submit">
                <i class="fa fa-search"></i>
              </button>
            </div>
          </div>
        </form>                 
      </div>               
    </nav>
  </div>
</section>
