<a href="<?= h($this->Url->build(['plugin' => null, 'controller' => 'Projects', 'action' => 'view', 'id' => $project->id])); ?>">
  <div class="project-card">
    <div class="project-thumbnail">
      <?php if(!empty($project->thumbnail) && $project->thumbnail != '#'): ?>
        <img data-src="<?= $project->thumbnail; ?>" class="img-responsive fadein">
      <?php else: ?>
        <img data-src="/img/placeholders/1920x1080.webp" class="img-responsive fadein">
      <?php endif; ?>
    </div>
    <div class="project-info">
      <header class="project-header">
        <h3 class="project-title">
          <?= h($project->name); ?>
        </h3>
      </header>
      <div class="project-content">
        <p>
          <?= h($project->projects_description->description); ?>
        </p>
        <p>
          <?= $this->Html->link(
            'Visit Project Page',
            ['plugin' => null, 'controller' => 'Projects', 'action' => 'view', 'id' => $project->id],
            ['class' => 'more-link']
          ); ?>
        </p>
      </div>
    </div>
  </div>
</a>