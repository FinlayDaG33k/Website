<?php
if (!isset($params['escape']) || $params['escape'] !== false) {
  $message = h($message);
}
?>

<div class="alert alert-warning text-dark rounded-0" role="alert">
  <?= $message ?>
</div>