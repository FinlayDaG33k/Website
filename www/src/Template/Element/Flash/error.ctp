<?php
if (!isset($params['escape']) || $params['escape'] !== false) {
  $message = h($message);
}
?>

<div class="alert alert-danger text-white rounded-0" role="alert">
  <?= $message ?>
</div>