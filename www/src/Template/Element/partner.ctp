<div class="col-md-3 d-flex align-items-center">
  <a href="<?= htmlentities($url); ?>" target="_blank" rel="noopener">
    <img data-src="/img/partners/<?= $name; ?>.webp" alt="<?= $name; ?>" class="img-responsive partner-logo">
  </a>     
</div>
