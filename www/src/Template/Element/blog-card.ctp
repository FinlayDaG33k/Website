<div class="blog-card">
  <div class="blog-info">
    <!-- Title -->
    <header class="blog-header">
      <h3 class="blog-title">
        <?= $this->Html->link($this->Article->getTitle($article), ['action' => 'view', $article->slug]) ?>
      </h3>
    </header>

    <!-- Card content -->
    <div class="blog-content">
      <div class="blog-meta">
        <!-- Post date and comment counter -->
        <span>
          <i class="fa fa-calendar" aria-hidden="true"></i> on <time datetime="<?= $article->created->format('Y-m-d') ?>"><?= $article->created->format('M d, Y') ?></time>
          <i class="fas fa-comments" aria-hidden="true"></i> <?= count($article->comments); ?> Comments
        </span>
        <div>
          <!-- Tags -->
          <ul class="post-categories">
            <?php foreach($article->tags as $tag): ?>
              <li>
                <a href="#" rel="category tag" target="_self" class="waves-effect waves-light bg-primary-light text-white"><?= $tag->title; ?></a>
              </li>
            <?php endforeach; ?>
          </ul>
        </div>
      </div>
      <!-- Excerpt -->
      <p class="post-content"><?= $this->Article->getExcerpt($this->Article->getBody($article)); ?></p>

      <!-- Read more button -->
      <p>
        <?= $this->Html->link("Read More", ['controller'=> 'blog', 'action' => 'view', $article->slug],['class' => 'more-link']) ?>
      </p>
    </div>
  </div>
</div>