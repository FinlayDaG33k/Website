<?php $this->pageTitle = 'About Me'; ?>

<div class="row mt-3">
  <div class="container">
    <h3>Introduction</h3>
      Hii Guys,

      I'm Aroop 'Finlay' Roelofs a.k.a FinlayDaG33k.
      I'm age <?= $this->Time->getDifference('08-11-1998')->y; ?> and born on the 8th of November 1998.<br />
      I am a Hobbyist-programmer, Gamer, Blogger and just a big ass nerd.<br />
      I basically live in the attic in our home, and I sometimes appear like a ninja to look if food magically appeared in the fridge.<br />
      I am a huge music fan and probably can't live a day without it.<br />
      I tend to talk a lot in the "I" form (You probably already noticed it), but that's no big deal... I guess...<br />
      <br />
      When programming, I often make basic stuff and sometimes more advanced stuff.<br />
      My most commonly used languages are:<br />
      <ul>
        <li>PHP</li>
        <li>Javascript</li>
        <li>HTML</li>
        <li>Python</li>
        <li>VB.NET</li>
        <li>Bash</li>
      </ul>

      On the age of 12, I got diagnosed with Autism (PDD-NOS to be exact) this has made my life a bit more difficult.<br />
      However, unlike popular believe, it's not an illness or disease.<br />
      I can function with it quite normally (I can get groceries, I can cook, I can travel by public transport etc. etc.),<br />
      It just makes communication a bit more difficult for me.<br />
      <br />
      At the age of 13 I started to realize I was colorblind.<br />
      I can see about 25% color saturation, so I am able to see which color is what, just have some difficulties seing differences between tints<br />
      It never really bothered me until people started to ask which tint of color I liked best.<br />
  </div>
</div>

<div class="row mt-3">
  <div class="container">
    <h3>The story behind my online name</h3>
      I became <code>FinlayDaG33k</code> when I was around 14 years old.<br />
      Back then, I had some friends who I gamed with a lot.<br />
      We used to go under many names.<br />
      <br />
      On FlyFF we used the name <code>Piece The Veil</code>.<br />
      I was known under the name: <code>PTV Rez</code> (Pierce The Veil Resurrect).<br />
      This was because I always played as an Assist. later on, I, of course, chose the path of the Ringmaster.<br />
      We used this name from early 2004 to late 2005.<br />
      We had quite a high rank back then.<br />
      Due to all sorts of reason, this team disbanded.<br />
      Frankly enough, some shitty wrist violin trash "band" stole the name and used it for their own in 2006...<br />
      Nobody remembers this team or my name anymore.<br />
      <br />
      On DoTA we used the name <code>Random Dutch Team</code>.<br />
      We used this when we discovered the game in mid-2006.<br />
      We managed to get a decent rank in the national ladders.<br />
      I was known under the name <code>[RDT] Estah</code>.<br />
      I don't know how I came up with the name, but ah well :)<br />
      This team also disbanded due to inactivity in early-2007.<br />
      Again, to never be remembered by anyone.<br />
      <br />
      After a long period of radio silence around attempts on becoming an actual pro gaming team, we decided to make our last attempt.<br />
      Our last attempt was made in early-mid 2014.<br />
      This time in the game <code>Team Fortress 2</code>.<br />
      This time, we came up with a name that suited our current situations.<br />
      I became a programmer, another team member became a chemist, and the other 3 became engineers.<br />
      So we choose for the name <code>Team G33k</code><br />

      Back then, I always talked very happily (much of which you can see in the videos I release nowadays).<br />
      With this, A great mistake came in:<br />
      I wanted to say the word <code>finally</code> but instead, my friends heard is as <code>finlay</code>.<br />
      So because of this, they decided to call me this from now on.<br />
      <br />
      Because we had a bit of an issue with names this time, we decided to go with our nicknames and slapped <code>DaG33k</code> behind it.<br />
      So this where our names pretty much (guess which one is me):
      <ul>
          <li>BapaoDaG33k</li>
          <li>1nC0ntr07DaG33k (InControlDaG33k)</li>
          <li>YetiDaG33k</li>
          <li>MikadoDaG33k</li>
          <li>FinlayDaG33k</li>
      </ul>

      After a long year of playing <code>Team Fortress 2</code>, our team disbanded in a horrible way in mid-2015.<br />
      <code>Bapao</code>,<code>1nC0ntr07</code>,<code>Yeti</code> and <code>Midako</code> where driving to a festival (I didn't join because I was too young, and didn't really like it anyways), when they got in a car accident.<br />
      Killing all of them (2 died nearly instantly according to the report, the others on their way to the hospital or in the hospital).<br />
      <br />
      Because I wanted to honour them, instead of changing my name again, I stuck with <code>FinlayDaG33k</code>.<br />
      Trying to always keep the people around me happy, helping them were needed, and just showing people the joy of life is what I want.<br />
      Because some day, it will end for me as well.<br />
      <br />
      And that is how I became <code>FinlayDaG33k</code>.<br />
      I know it's a heavy story, but this is how I got my online name and stuck to it.
  </div>
</div>