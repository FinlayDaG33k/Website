<?php $this->pageTitle = 'GraphQL'; ?>

<div class="row" id="intro">
  <div class="col-md-12">
    <div class="jumbotron">
      <h1 class="display-3">GraphQL</h1>
      <p class="lead">In case you for some reason want to integrate my stuff</p>
    </div>             
  </div>
</div>  

<div class="container mb-2">
  <div class="row">
    <div class="col-12">
      As of the 8th of August 2019, this website supports a GraphQL based API as to replace the old REST-style API.<br />
      I personally felt like it'd be a better choice to do so as it can be maintained easier and allows for more customized resource loading.<br />
      The API can be found at <?= $this->Html->link('/graphql'); ?>.<br />
      If you're using a GraphQL editor like <?= $this->Html->link('Insomnia', 'https://insomnia.rest/'); ?>, the schema will automatically be fetched for you.<br />
      <br />
      GraphQL is self-documenting so please view the documentation in your GraphQL editor to find out more.
    </div>
  </div>
</div>