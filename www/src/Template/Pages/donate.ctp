<?php $this->pageTitle = 'Donate'; ?>

<div class="container">
  <div class="row no-banner">
      <div class="col-md-6">
        <h4>Why donate?</h4>
        <div style="padding-bottom:25px;">
          Donating is the best way to support me financially.<br />
          Currently, this site and all my other projects are fully paid out of my own pocket.<br />
          Development, the server upkeep (I own the server physically), time invested in the projects, all coming from my own.<br />
          This means that in the (knowing me, pretty likely) event something goes wrong, any expenses to get it sorted are all mine.<br />
          So if you'd like to support me in my work, then please leave a donation, anything is appreciated.<br />
          <br />
          By donating, you support the individuality of my projects and website.
        </div>
      </div>
      <div class="col-md-6">
        <h4>Donation options</h4>
        <div style="padding-bottom:25px;">
          I support a variety of ways with which you can donate if you please.<br />
          Your preferred way missing? <?= $this->Html->link('Let me know', ['plugin'=>null, 'controller' => 'Contact', 'action' => 'index', 'contact']); ?> and we can probably get it sorted :)
        </div>
        <?= $this->cache(function() {
          echo $this->cell('SocialButton',[
            'link' => 'https://www.paypal.me/finlaydag33k',
            'icon' => 'paypal'
          ]);
          echo $this->cell('SocialButton',[
            'link' => 'https://ko-fi.com/finlaydag33k',
            'icon' => 'kofi'
          ]);

          echo $this->cell('SocialButton',[
            'link' => 'bitcoin:1HyR7vmQce4oxVUDvGNCw43vkaLZwAzwyz?message=Donation%20through%20website',
            'icon' => 'bitcoin'
          ]);
          echo $this->cell('SocialButton',[
            'link' => 'ethereum:0x010261f2b80FBBabec3eA2022e1f15e6b9F972B2',
            'icon' => 'ethereum'
          ]);
          echo $this->cell('SocialButton',[
            'link' => 'doge:DEhUsPmybfo5ZVNrUNtuHAdPYLhEZ6Lpi8',
            'icon' => 'dogecoin'
          ]);
        }, ['key' => 'DonationButtons']); ?>
        <?= $this->Form->errors; ?>
      </div>
  </div>
</div>