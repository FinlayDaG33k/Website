<?php $this->pageTitle = 'Review Policy'; ?>

<div class="row" id="intro">
  <div class="col-md-12">
    <div class="jumbotron">
      <h1 class="display-3">Review Policy</h1>
      <p class="lead">For an honest opinion or your product!</p>
    </div>             
  </div>
</div> 

<div class="container mt-3">
  <div class="row">
    <p>
      Want to send me your product for me to review?<br />
      That's awesome!<br />
      Unfortunately, I'm not some kind of high profile Youtuber or blogger.<br />
      However, I will always try to do my best to review products to the best of my extend!.<br />
      Please do note that these rules also apply to non-physical products and/or services as well as those I bought myself (just to keep things fair) :)
    </p>
    <p>
      But, there are some things I want to mention before you start sending me stuff.<br />
      I do take nearly all products that I can safely show off for review.<br />
      So don't send me your drugs or stuff that need to be handled very carefully (eg. by a specialist, knifes are fine).<br />
      Stuff that is only dangerous if broken or damaged (like stuff made from glass, batteries etc.) is allowed but please don't send me stuff like firearms (airsoft guns are fine though).<br />
      Ofcourse, before I give you the shipping address, we should have already settled on this. ;)
    <p>
      Items that are for general consumers/prosumers are preferred!<br />
      Of course, more specialized/niche items are welcome as well but do note that I might not have the adience around that you might want to target.
    </p>
    <p>
      I want to make one point extremely clear, however:<br />
      It will be posted on MY social media and/or on MY website.<br />
      This means that I dictate the rules and if you don't agree with them then unfortunately I have to decline your offer.<br />
      Let's take a look at these rules shall we?
    </p>
  </div>
  <div class="row">
    <div class="accordion w-100" id="accordion">
      <div class="card rounded-0 mb-2">
        <div class="card-header" id="headingOne" data-toggle="collapse" data-target="#collapseOne" >
          No Cheating!
        </div>
        <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
          <div class="card-body">
            I will make my reviews 100% honest. This means I will point out as many negatives and possibles as I can find and inform the 
            If you don't like this, go find someone who is willing to bend over for you or your company instead.
            If you can't handle criticism about your product, then don't even try to sell them.
            Either I will point them out (and if it's a prototype you can still fix it before the final release hits the market) or the community will do so. (which may potentially cost you your reputation)
            If your product fails (or simply doesn't reach any expectation/promises at all) it will make it into the review.
            I do not simply tell you in secret that the product fails so that you can fix it and act like nothing happened.
            My thoughts aren't for sale.
            If your product sucks, it sucks and I will publish this in the review. No money can change it (in fact, attempts to bribe me will get published as well).
            I also will not take down my review because you don't like it.
            The only reason for me to take it down is because of some massive issue on my side (like the video freezing).
          </div>
        </div>
      </div>

      <div class="card rounded-0 mb-2">
        <div class="card-header" id="headingTwo" data-toggle="collapse" data-target="#collapseTwo" >
          In for a Gamble?
        </div>
        <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
          <div class="card-body">
            If you send your product, you take a gamble.<br />
            I don't care if it's "not how it was supposed to be" or "a bad unit".<br />
            All units should be checked for quality and A faulty unit should be not have passed through QC.<br />
            The only "excuse" I can accept is the device breaking in transit (which I will judge based on the state of the packaging and possible fragile components), or it being a prototype (in which case I should know this beforehand).
          </div>
        </div>
      </div>

      <div class="card rounded-0 mb-2">
        <div class="card-header" id="headingThree" data-toggle="collapse" data-target="#collapseThree" >
          No peeking allowed!
        </div>
        <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
          <div class="card-body">
            I will not allow anyone but myself (and maybe my girlfriend) to see a review before it's being posted online.<br />
            No exceptions.<br />
          </div>
        </div>
      </div>

      <div class="card rounded-0 mb-2">
        <div class="card-header" id="headingFour" data-toggle="collapse" data-target="#collapseFour" >
          No Going Home!
        </div>
        <div id="collapseFour" class="collapse" aria-labelledby="headingFour" data-parent="#accordion">
          <div class="card-body">
            Any review samples will be kept or put into a giveaway.<br />
            So if it is your only prototype, then don't send it or you won't have a prototype left.
          </div>
        </div>
      </div>

      <div class="card rounded-0 mb-2">
        <div class="card-header" id="headingFive" data-toggle="collapse" data-target="#collapseFive" >
          Always stay Competitive!
        </div>
        <div id="collapseFive" class="collapse" aria-labelledby="headingFive" data-parent="#accordion">
          <div class="card-body">
          There is a big chance I will compare your gear to competitor's gear (or even your own depending on the scenario).<br />
              If you don't like that, then don't send it (it's very common for reviews to do so anyways).
          </div>
        </div>
      </div>

      <div class="card rounded-0 mb-2">
        <div class="card-header" id="headingSix" data-toggle="collapse" data-target="#collapseSix" >
          Change is good!
        </div>
        <div id="collapseSix" class="collapse" aria-labelledby="headingSix" data-parent="#accordion">
          <div class="card-body">
            I may always update the terms of reviews.<br />
            However, old reviews won't be affected by it. (see <code>No Cheating</code> section)
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="row">
    <p>
      Now that we got that out of the way, if you still want me to review your product, then head over to the contact form and tell me about you, your company and your product.<br />
      If it sounds interesting enough to me, then I will give you my address, to which you can send my review sample.<br />
      Thanks!
    </p>
  </div>
</div>

<div class="row mt-3">
  <div class="container">
    <h3>Update log</h3>
    <ul>
      <li>09/08/2019
        <ul>
          <li>Updated layout as well as some phrasing.</li>
        </ul>
      </li>
      <li>14/02/2019
        <ul>
          <li>Updated some phrasing (terms have stayed mostly the same)</li>
        </ul>
      </li>
    </ul>
  </div>
</div>