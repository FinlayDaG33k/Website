<?php
$slogans = [
  "Where everything goes wrong!",
  "Which apparently is still running!?",
  "HOLY BALLS IT WORKS!",
  "Redundant subheader for the sake of design!",
  "If you can read this, nothing went wrong...",
  "Wait... this isn't an error?",
  "So viele alte Kameraden!",
  "Where ads don't track you!",
  "Where popular opinion means nothing!",
  "Where this subheader changes on every visit!"
];
$this->pageTitle = 'Home';
?>

<div class="row" id="intro">
  <div class="col-md-12">
    <div class="jumbotron">
      <h1 class="display-3">Welcome to my website!</h1>
      <p class="lead">
        <?php if($this->Festivity->getFestivity() === 'Saint Patricks Day'): ?>
          Where we are all a little bit Irish today!
        <?php else: ?>
          <?= $slogans[mt_rand(0, count($slogans) - 1)]; ?>
        <?php endif; ?>
      </p>
    </div>
  </div>
</div>
<div class="row services-section" id="services">
  <div class="container">
    <div class="row section-header">
      <h3 class="section-title">My Shizzle</h3>
      <p class="section-subtitle">What do I do?</p>
    </div>
    <div class="row section-body">
      <div class="col-md-6">
        <div class="media service-area">
          <div class="service-box">
            <i class="fas fa-cogs"></i>
          </div>
          <div class="media-body">
            <h4 class="entry-title">Development</h4>
            <p>
              Development is my primary occupation.<br />
              To me, one day of not touching anything with development, is one day I have not been alive.<br />
              Though some speculate I might have been sick that day.<br />
            </p>
          </div>
        </div>
      </div>
      <div class="col-md-6">
        <div class="media service-area">
          <div class="service-box">
            <i class="fas fa-gamepad"></i>
          </div>
          <div class="media-body">
            <h4 class="entry-title">Gaming</h4>
            <p>
              Gaming is my secondary way of going around the clock.<br />
              Games like <code>League of Legends</code> and <code>World of Warships</code> make my heart beat!<br />
              I'm not some highly-skilled player, but I'm highly-competitive none-the-less!
            </p>
          </div>
        </div>
      </div>
    </div>
    <div class="row section-body">
      <div class="col-md-6">
        <div class="media service-area">
          <div class="service-box">
            <i class="fas fa-microchip"></i>
          </div>
          <div class="media-body">
            <h4 class="entry-title">Tech</h4>
            <p>
              As somebody who loves tech, you'll find some tech-related stuff here every now and then.<br />
              However, since I'm not the richest, it won't be that much about the latest and greatest.
            </p>
          </div>
        </div>
      </div>
      <div class="col-md-6">
        <div class="media service-area">
          <div class="service-box">
            <i class="fas fa-meh"></i>
          </div>
          <div class="media-body">
            <h4 class="entry-title">Ranting</h4>
            <p>
              I'll complain about everything I think can be done better.<br />
              You'll find a lot of rants on my blog.<br />
              Consider them, think about them, discuss them!
            </p>
          </div>
        </div>
      </div>
    </div>
    <div class="row section-body">
      <div class="col-md-6">
        <div class="media service-area">
          <div class="service-box">
            <i class="fas fa-globe-americas"></i>
          </div>
          <div class="media-body">
            <h4 class="entry-title">Making this world a better place</h4>
            <p>
              I always come up with creative solutions to real problems.<br />
              My goal is to make this world a better place not only for those here now, but also those who come after us.
            </p>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="row" id="projects">
  <div class="container">
    <div class="row section-header">
      <h3 class="section-title">My Projects</h3>
      <p class="section-subtitle">A small preview of what I've been working on</p>
    </div>
    <div class="row section-body" id="projects-cards"></div>
  </div>
</div>

<div class="row" id="partners">
  <div class="container">
    <div class="row section-header">
      <h3 class="section-title">My Partners</h3>
      <p class="section-subtitle">Coming from all across the globe, I partnered with these lads to give you awesome content!</p>
    </div>
    <div class="row section-body">
      <?= $this->cache(function() { ?>
        <?= $this->element('partner',["name" => "devinehq","url"=>"https://www.devinehq.eu/"]); ?>
        <?= $this->element('partner',["name" => "operationtulip","url"=>"https://www.operationtulip.com/"]); ?>
      <?php }, ['key' => 'Partner Logos']); ?>
    </div>
  </div>
</div>
