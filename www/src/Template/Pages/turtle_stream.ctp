<?php $this->pageTitle = 'Turtle Stream'; ?>

<div class="row">
  <div class="col-md-12">
    <div class="jumbotron pt-6 bg-primary-normal">
      <h1 class="display-3 text-white"><span class="align-middle glyphicons glyphicons-turtle"></span> Live Turtles</h1>
      <p class="lead text-white">Watch some turtles happily swimming around!</p>
    </div>             
  </div>
</div>
<div class="row">
  <div class="container">
    <div class="row">
      <div class="col-md-8">
        <img class="turtle-stream" src="http://warmolt7.schildpaddenopvang.nl:8888/videostream.cgi?user=user123&amp;pwd=user123">
      </div>
      <div class="col-md-4">
        <p>
          This stream is being provided by <a href="https://www.schildpaddenopvang.nl" target="_blank">Stichting SchildpaddenOpvang</a> in Harkema (Fryslân).<br />
          They are a charity that takes care of turtles in The Netherlands that are found in the wild.<br />
          In The Netherlands, a turtle that is being put in the wild will die a slow death.<br />
        </p>
        <p>
          One can feed the turtles by giving a <a href="https://www.geef.nl/en/donate?charity=6151&ref=&referenceCode=&amount=5.00&fixedAmount=N" target="_blank">donation</a> to Schildpaddenopvang.<br />
          When a donation has been made, the feeder machine will start to spin after a moment!<br />
          Don't worry, you can't overfeed the turtles!<br />
          None of these proceeds go to me.<br />
        </p>
      </div>
    </div>
  </div>
</div>