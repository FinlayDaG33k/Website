<?php $this->pageTitle = 'Resume'; ?>

<div class="row" id="intro">
  <div class="col-md-12">
    <div class="jumbotron" id="intro-jumbotron">
      <h1 class="display-3">My Resume</h1>
      <p class="lead"><?= $slogans[mt_rand(0, count($slogans) - 1)]; ?></p>
    </div>
  </div>
</div>
<div class="row services-section" id="services">
  <div class="container">
    <div class="row section-header">
      <h3 class="section-title">My Experience</h3>
      <p class="section-subtitle">Employers, internships and voluntary work</p>
    </div>
    <?php $index = 0; ?>
    <?php foreach($data['employers'] as $key => $employer): ?>
      <?php if($index == 0): ?><div class="row section-body"><?php endif; ?>
        <div class="col-md-6">
          <div class="media service-area">
            <div class="service-box">
              <?php switch($employer['type']):
                case 0: ?>
                  <i class="fas fa-hands-helping"></i>
                <?php break; ?>
                <?php case 1: ?>
                  <i class="fas fa- fa-graduation-cap"></i>
                <?php break; ?>
                <?php case 2: ?>
                  <i class="fas fa-briefcase"></i>
                <?php break; ?>
              <?php endswitch; ?>
            </div>
            <div class="media-body">
              <h4 class="entry-title"><?= h($employer['name']); ?></h4>
              <p class="section-subtitle">
                <?= h($employer['dates']['start']); ?>
                <?php if($employer['dates']['start'] !== $employer['dates']['end']): ?>
                  -
                  <?php if($employer['dates']['end'] !== null): ?>
                    <?= h($employer['dates']['end']); ?>
                  <?php else: ?>
                    Present
                  <?php endif; ?>
                <?php endif; ?>
              </p>
              <p>
                <?= nl2br(h($employer['description'])); ?>
              </p>
            </div>
          </div>
        </div>
      <?php if($index == 1): ?></div><?php endif; ?>
      <?php
        if($index == 1) $index = 0;
        else $index++;
      ?>
      <?php if($employer === array_key_last($data['employers'])): ?></div><?php endif; ?>
    <?php endforeach; ?>
    </div>
  </div>
</div>

<div class="row" id="projects">
  <div class="container">
    <div class="row section-header">
      <h3 class="section-title">My Education</h3>
      <p class="section-subtitle">Degrees, Diplomas, certifications and Licenses</p>
    </div>
    <div class="row section-body">
      <div class="col-md-12">
        <div class="media service-area">
          <div class="media-body">
            <h4 class="entry-title">Degrees and Diplomas</h4>
            Absolutely none.
          </div>
        </div>
      </div>
    </div>
    <div class="row section-body">
      <div class="col-md-12">
        <div class="media service-area">
          <div class="media-body">
            <h4 class="entry-title">Certifications and Licenses</h4>
            <table class="table">
              <thead>
                <?= $this->Html->tableHeaders(
                  ['Level', 'Trade', 'Educator', 'Time', 'Status'],
                  ['class' => 'col']
                ); ?>
              </thead>
              <tbody>
                <?= $this->Html->tableCells($data['certifications']); ?>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="row" id="projects">
  <div class="container">
    <div class="row section-header">
      <h3 class="section-title">My Projects</h3>
      <p class="section-subtitle">Projects I want to highlight</p>
    </div>
    <div class="row section-body">
      <div class="col-md-12">
        <div class="media service-area">
          <div class="media-body">
            <table class="table">
              <thead>
                <?= $this->Html->tableHeaders(
                  ['Name', 'Description', 'Repository'],
                  ['class' => 'col']
                ); ?>
              </thead>
              <tbody>
                <?= $this->Html->tableCells($data['projects']); ?>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="row" id="partners">
  <div class="container">
    <div class="row section-header">
      <h3 class="section-title">My Skills</h3>
      <p class="section-subtitle">The skills I possess and how much I personally think I have mastered them</p>
    </div>
    <div class="row section-body">
      <div class="col-md-12">
        <div class="media service-area">
          <div class="media-body">
          <h4 class="entry-title">Programming Languages</h4>
            <table class="table">
              <thead>
                <?= $this->Html->tableHeaders(
                  ['Skill', 'Rating'],
                  ['class' => 'col']
                ); ?>
              </thead>
              <tbody>
                <?= $this->Html->tableCells($data['skills']['programming']); ?>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
    <div class="row section-body">
      <div class="col-md-12">
        <div class="media service-area">
          <div class="media-body">
          <h4 class="entry-title">Operating Systems</h4>
            <table class="table">
              <thead>
                <?= $this->Html->tableHeaders(
                  ['Skill', 'Rating'],
                  ['class' => 'col']
                ); ?>
              </thead>
              <tbody>
                <?= $this->Html->tableCells($data['skills']['operating systems']); ?>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
    <div class="row section-body">
      <div class="col-md-12">
        <div class="media service-area">
          <div class="media-body">
          <h4 class="entry-title">Miscellaneous</h4>
            <table class="table">
              <thead>
                <?= $this->Html->tableHeaders(
                  ['Skill', 'Rating'],
                  ['class' => 'col']
                ); ?>
              </thead>
              <tbody>
                <?= $this->Html->tableCells($data['skills']['misc']); ?>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
