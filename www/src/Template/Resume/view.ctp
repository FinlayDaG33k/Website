<?php $this->pageTitle = 'Resume'; ?>

<div class="container">
  <div class="row">
    <!-- Main content -->
    <div class="col-8 mt-4">
      <!-- Introduction -->
      <div class="row">
        <div class="col-12">
          <h4>Introduction</h4>
          Hii there, I am Aroop Roelofs. People know me as an analytical person, someone that thinks ahead and likes to investigate new things.<br />
          Over the past <?= $this->Time->getDifference('01-01-2011')->y; ?> years, I have obtained valuable technical knowledge and insights in a wide variety of fields.<br />
          I also have a good eye for detail, which allows me to invent creative solutions for a lot of technical problems.<br />
          On the workfloor, I would love to bring this all to bear as to, together with my colleagues, deliver a high quality of products.<br />
          In this, I think it is important to play into the qualities and capacities of each, in order to reach our common goals.<br/ >
          While I would like to fill this resume with all my prior experiences, I'd like to keep it as compact as I can, so feel free to have a look at <?= $this->Html->link('my website', ['controller' => 'Resume', 'action' => 'index', 'resume']); ?>.
        </div>
      </div>

      <!-- Experience -->
      <div class="row mt-2">
        <div class="col-12">
          <h4>Experience</h4>
          <?php foreach(array_slice($data['employers'], 0, 3) as $employer): ?>
            <div class="row mt-2">
              <div class="col-2">
                <p class="font-weight-bold">
                  <?= h($employer['dates']['start']); ?>
                  <br />
                  <?php if($employer['dates']['start'] !== $employer['dates']['end']): ?>
                    <?php if($employer['dates']['end'] !== null): ?>
                      <?= h($employer['dates']['end']); ?>
                    <?php else: ?>
                      Present
                    <?php endif; ?>
                  <?php endif; ?>
                </p>
              </div>
              <div class="col-10">
                <p class="mb-0">
                  <span class="font-weight-bold"><?= h($employer['role']); ?></span>
                  <br />
                  <span class="text-muted">
                    <?= h($employer['name']); ?>
                    <?php if(!empty($employer['location'])): ?>
                    , <?= h($employer['location']); ?>
                    <?php endif; ?>
                </span>
                </p>
                <ul class="p-0">
                  <?php foreach(array_slice($employer['tasks'], 0, 5) as $task): ?>
                    <li><?= h($task); ?></li>
                  <?php endforeach; ?>
                </ul>
              </div>
            </div>
          <?php endforeach; ?>
        </div>
      </div>

      <!-- Certifications -->
      <div class="row mt-2">
        <div class="col-12">
          <h4>Certifications</h4>
          <?php foreach(array_splice($data['certifications'], 0, 5) as $certification): ?>
            <?php if($certification[4] !== 'Completed') continue; ?>
            <div class="row mt-2">
              <div class="col-2">
                <p class="font-weight-bold"><?= h($certification[3]); ?></p>
              </div>
              <div class="col-10">
                <p>
                  <span class="font-weight-bold">
                    <?php if($certification[0] !== 'N/A'): ?>
                      <?= h($certification[0]); ?>
                    <?php endif; ?>
                    <?= h($certification[1]); ?>
                  </span>
                  <br />
                  <span class="text-muted"><?= h($certification[2]); ?></span>
                </p>
              </div>
            </div>
          <?php endforeach; ?>
        </div>
      </div>

      <!-- Prefered software -->
      <div class="row mt-2">
        <div class="col-12">
          <h4>Preferred Software</h4>
          <?php foreach($data['software'] as $software): ?>
            <button class="btn btn-xs btn-primary rounded-0"><?= h($software); ?></button>
          <?php endforeach; ?>
        </div>
      </div>

      <!-- Projects -->
      <div class="row mt-2">
        <div class="col-12">
          <h4>Projects</h4>
          <?php foreach(array_splice($data['projects'], 0, 5) as $project): ?>
          <div class="row mb-2">
            <div class="col-2">
              <?php if($project['repo']): ?><a href="<?= h($project['repo']); ?>" target="_new"><?php endif; ?>
              <span class="font-weight-bold"><?= h($project['name']); ?></span>
              <?php if($project['repo']): ?></a><?php endif; ?>
            </div>
            <div class="col-10">
              <?= nl2br(h($project['description'])); ?>
            </div>
          </div>
          <?php endforeach; ?>
        </div>
      </div>
    </div>

    <!-- Sidebar -->
    <div class="col-4 px-4" style="background-color: #EEEEEE">
      <div class="row">
        <div class="col-12">
          <?= $this->Html->image('resume_picture.jpg', [
            'alt' => 'My picture',
            'class' => 'img-fluid my-4'
          ]); ?>
        </div>
      </div>
      <div class="row">
        <div class="col-12">
          <h4>Personal Information</h4>
          <table class="table">
            <tbody>
              <tr>
                <th>Name</th>
                <td>Aroop Roelofs</td>
              </tr>
              <tr>
                <th>Date of birth</th>
                <td>November 08, 1998</td>
              </tr>
              <tr>
                <th>Place of birth</th>
                <td>New Delhi, India</td>
              </tr>
              <tr id="address">
                <th>Country of residence</th>
                <td>The Netherlands</th>
              </tr>
            </tbody>
          </table>
        </div>
      </div>
      <div class="row">
        <div class="col-12">
          <h4>Contact Information</h4>
          <table class="table">
            <tbody>
              <tr>
                <th>Website</th>
                <td><?= $this->Html->link('https://www.finlaydag33k.nl', 'https://www.finlaydag33k.nl'); ?></td>
              </tr>
              <tr>
                <th>Email</th>
                <td>me@finlaydag33k.nl</td>
              </tr>
              <tr>
                <th>PGP Key</th>
                <td>0x738EE8DBC43F9861</td>
              </tr>
              <tr>
                <th>Mobile Phone</th>
                <td id="phone-number">Requestable through email</td>
              </tr>
              <tr>
                <th>Matrix</th>
                <td>@finlaydag33k:finlaydag33k.nl</th>
              </tr>
            </tbody>
          </table>
        </div>
      </div>
      <div class="row">
        <div class="col-12">
          <h4>Programming Skills</h4>
          <table class="table">
            <tbody>
              <?php foreach($data['skills']['programming'] as $skill): ?>
                <?php if(!in_array($skill[1], ['Advanced', 'Intermediate'])) continue; ?>
                <tr>
                  <td><?= h($skill[0]); ?></td>
                  <td>
                    <?php if($skill[1] == 'Advanced'): ?>
                      <i class="fas fa-circle" style="color: #286B99;"></i>
                      <i class="fas fa-circle" style="color: #286B99;"></i>
                      <i class="fas fa-circle" style="color: #286B99;"></i>
                      <i class="fas fa-circle" style="color: #286B99;"></i>
                      <i class="far fa-circle" style="color: #286B99;"></i>
                      <br />
                      Advanced
                    <?php elseif($skill[1] == 'Intermediate'): ?>
                      <i class="fas fa-circle" style="color: #286B99;"></i>
                      <i class="fas fa-circle" style="color: #286B99;"></i>
                      <i class="fas fa-circle" style="color: #286B99;"></i>
                      <i class="far fa-circle" style="color: #286B99;"></i>
                      <i class="far fa-circle" style="color: #286B99;"></i>
                      <br />
                      Intermediate
                    <?php endif; ?>
                  </td>
                </tr>
              <?php endforeach; ?>
            </tbody>
          </table>
        </div>
      </div>
      <new-page />
      <div class="row">
        <div class="col-12">
          <h4>Languages</h4>
          <table class="table">
            <tbody>
              <?php foreach($data['skills']['languages'] as $language => $score): ?>
                <tr>
                  <td scope="row"><?= h(ucfirst($language)); ?></td>
                  <td>
                    <?php $dots = floor($score / 20); ?>
                    <?php for($i = 0; $i < $dots; $i++): ?>
                      <i class="fas fa-circle" style="color: #286B99;"></i>
                    <?php endfor; ?>

                    <?php for($i = 0; $i < 5 - $dots; $i++): ?>
                      <i class="far fa-circle" style="color: #286B99;"></i>
                    <?php endfor; ?>
                    <br />

                    <?php if($dots < 2): ?>
                      Basic
                    <?php elseif($dots == 3): ?>
                      Good
                    <?php elseif($dots == 4): ?>
                      Very Good
                    <?php elseif($dots == 5): ?>
                      Excellent
                    <?php endif; ?>
                  </td>
                </tr>
              <?php endforeach; ?>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>
