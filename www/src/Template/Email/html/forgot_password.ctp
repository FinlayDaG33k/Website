<p>
  Good<?= h($daysection); ?> <?= h($username); ?>,
</p>
<p>
  If I'm not mistaken, you have requested to reset your password.<br />
  Because of that, I have send this mail in order to help you out!<br />
  If you did not request this, then please do ignore this email.<br />
</p>
<p>
  If you did request it, then please click the link below to continue.<br />
  To make sure you are safe, I want to inCake\\Mailer\\Emailform you that whomever has this link, will gain access to your account.<br />
  As such, please make sure you keep this link and your password (both old and new) to yourself.<br />
  <a href="<?= h($url); ?>/<?= h($token); ?>">Reset my password!</a>
  I hope I was of any help.<br />
</p>
<p>
  Yours Sincerely,<br />
  FinlayDaG33k's WebMaid
</p>