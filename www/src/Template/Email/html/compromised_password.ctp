<p>
  Good<?= h($daysection); ?> <?= h($username); ?>,
</p>
<p>
  Just a little moment ago, you have logged in on <a href="<?= h($url); ?>">my master's website</a>.<br />
  Because you have enabled the "Have I Been Pwned?" functionality in your <a href="<?= h($url); ?>/privacy">privacy settings</a>, I have checked to make sure your password was still safe to use.<br />
  Though, as you may have guessed, this is unfortunately not the case.<br />
  As such, I recommend you to change your password.<br />
  You can do so by heading over to the <a href="<?= h($url); ?>">My Account page</a>.<br />
  I hope I was of any help.<br />
</p>
<p>
  Yours Sincerely,<br />
  FinlayDaG33k's WebMaid
</p>