<?php $this->pageTitle = 'Contact'; ?>

<div class="container">
  <div class="row no-banner">
      <div class="col-md-6">
        <h4>Contact Form</h4>
        <div style="padding-bottom:25px;">
          Got a question? I'd love to hear it from you!<br />
          Send me a message and i'll respond as soon as possible!
        </div>
        <?= $this->Flash->render(); ?>
        
        <?= $this->Form->create($contactForm,["url"=>"/contact","class"=>"contact-form","id"=>"contact-form"]); ?>
          <?= $this->Form->control("name",["placeholder"=>"Your Name","label"=>false,"class"=>"form-control"]); ?>
          <?= $this->Form->control("email",["placeholder"=>"Your Email","label"=>false,"class"=>"form-control"]); ?>
          <?= $this->Form->control("subject",["placeholder"=>"The Subject","label"=>false,"class"=>"form-control"]); ?>
          <?= $this->Form->textarea("message",["placeholder"=>"Your Message","label"=>false,"class"=>"form-control"]); ?>
          <?= $this->Form->button('Submit',[
            "class"=>"btn btn-primary bg-primary-normal rounded-0",
            "antispam" => true,
          ]); ?>
        <?= $this->Form->end(); ?>
      </div>
      <div class="col-md-6">
        <h4>Social Media</h4>
        <div style="padding-bottom:25px;">
          I'm active on a variety of social media, feel free to like and follow me!
        </div>
        <?= $this->cache(function() {
          echo $this->cell('SocialButton',[
            'link' => 'https://www.reddit.com/r/finlaydag33k',
            'icon' => 'reddit'
          ]);
          echo $this->cell('SocialButton',[
            'link' => 'https://social.linux.pizza/@finlaydag33k',
            'icon' => 'mastodon'
          ]);

          echo $this->cell('SocialButton',[
            'link' => 'https://www.youtube.com/finlaydag33k',
            'icon' => 'youtube'
          ]);
        }, ['key' => 'SocialButtons']); ?>
        <?= $this->Form->errors; ?>
      </div>
  </div>
</div>