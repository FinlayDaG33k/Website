<div class="row">
  <div class="col-lg-12">
    <?= $this->Flash->render(); ?>
  </div>
</div>

<div class="row mb-2">
  <div class="col-12">
    <?= $this->Form->create(null, [
      "url" => $this->Url->build([
        'plugin'=>null,
        'controller' => 'Projects',
        'action'=>'add'
      ]),
    ]); ?>
      <div class="row">
        <div class="col-9">
          <?= $this->Form->control('name', [
            'type' => 'text',
            'label' => false,
            'placeholder' => 'Project Name',
            'class' => 'form-control'
          ]); ?>
          <?= $this->Form->control('description', [
            'type' => 'textarea',
            'label' => false,
            'placeholder' => 'Project Description',
            'class' => 'form-control'
          ]); ?>
        </div>
        <div class="col-3">
          <div class="row">
            <div class="col-12">
              <div class="card">
                <div class="card-header">
                  Save
                </div>
                <div class="card-body">
                  <?= $this->Form->button('Save',[
                    "type"=>"submit",
                    "class"=>"btn btn-primary"
                  ]); ?>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    <?= $this->Form->end(); ?>
  </div>
</div>