<div class="row">
  <div class="col-lg-12">
    <?= $this->Flash->render(); ?>
  </div>
</div>

<div class="row mb-2">
  <div class="col-12">
    <?= $this->Form->create(null, [
      "url" => $this->Url->build([
        'plugin'=>null,
        'controller' => 'Projects',
        'action'=>'edit',
        'id' => $this->request->getParam('id')
      ])
    ]); ?>
      <div class="row">
        <div class="col-9">
          <?= $this->Form->control('name', [
            'type' => 'text',
            'label' => false,
            'placeholder' => 'Project Name',
            'class' => 'form-control',
            'value' => h($project->name)
          ]); ?>
          <?= $this->Form->control('description', [
            'type' => 'textarea',
            'label' => false,
            'placeholder' => 'Project Description',
            'class' => 'form-control',
            'value' => $project->projects_description->description
          ]); ?>
        </div>
        <div class="col-3">
          <div class="row">
            <div class="col-12">
              <div class="card">
                <div class="card-header">
                  Save
                </div>
                <div class="card-body">
                  <?= $this->Form->button('Save',[
                    "type"=>"submit",
                    "class"=>"btn btn-primary"
                  ]); ?>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    <?= $this->Form->end(); ?>
  </div>
</div>

<div class="row mt-4">
  <div class="col-9">
    <div class="row mb-2">
      <div class="col-3">
        <h4 class="d-inline">Version History</h4>
        <button class="btn btn-primary rounded-0 btn-sm" data-action="add-release">
          <i class="fas fa-plus"></i>
        </button>
      </div>
    </div>
    <table class="table table-striped">
      <thead>
        <th>Version</th>
        <th>Date</th>
        <th>Status</th>
        <th>Description</th>
        <th>Changelog</th>
        <th>Edit</th>
      </thead>
      <tbody>
        <?php foreach($project->releases as $release): ?>
          <tr data-release-id="<?= $release->id; ?>">
            <td><?= h($release->version); ?></td>
            <td><?= h($release->date->i18nFormat('MMM dd, yyyy kk:mm:ss')); ?></td>
            <td><?= $this->Project->releaseStatus($release->published); ?></td>
            <td data-field="release-description"><?= h($release->description); ?></td>
            <td>
              <a class="btn btn-primary rounded-0" href="#" data-action="view-changelog" data-version="<?= h($release->version); ?>" data-release="<?= h($release->id); ?>">
                <i class="fas fa-eye"></i>
              </a>
            </td>
            <td>
              <a class="btn btn-primary rounded-0 text-white" data-action="edit-release" data-version="<?= h($release->version); ?>" data-release="<?= h($release->id); ?>">
                <i class="fas fa-pen"></i>
              </a>
            </td>
          </tr>
        <?php endforeach; ?>
      </tbody>
    </table>
  </div>
</div>

<div class="modal" tabindex="-1" role="dialog" id="changelogModal">
  <div class="modal-dialog modal-lg modal-dialog-centered">
    <div class="modal-content rounded-0">

      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title">
          Changelog for version <span></span> of <?= h($project->name); ?>
        </h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>

      <!-- Modal body -->
      <div class="modal-body">
        <ul></ul>
      </div>
    </div>
  </div>
</div>

<div class="modal" tabindex="-1" role="dialog" id="addReleaseModal">
  <div class="modal-dialog modal-lg modal-dialog-centered">
    <div class="modal-content rounded-0">

      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title">
          Add Release to <?= h($project->name); ?>
        </h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>

      <!-- Modal body -->
      <div class="modal-body">
      <?= $this->Form->create(null, [
        "url" => $this->Url->build([
          '_name' => 'api:v1:projects:add-release',
          $project->id
        ]),
      ]); ?>
        <div class="row">
          <div class="col-12">
            <?= $this->Form->control('version', [
              'type' => 'text',
              'label' => false,
              'placeholder' => 'Release version',
              'class' => 'form-control rounded-0'
            ]); ?>
            <?= $this->Form->control('message', [
              'type' => 'text',
              'label' => false,
              'placeholder' => 'Release description',
              'class' => 'form-control rounded-0'
            ]); ?>
          </div>
        </div>
        <div class="row">
          <div class="col-12">
            <button type="button" class="btn btn-primary" data-action="add-dist-file">Add Dist file</button>
            <span id="fileName">Please select a file</span>
          </div>
        </div>
        <div class="row">
          <div class="col-12" id="changes">
          </div>
        </div>
        <div class="row" data-id="publish-statuses">
          <div class="col-12">
            <div class="form-check">
              <input class="form-check-input" type="radio" name="publishStatus" id="exampleRadios1" value="0">
              <label class="form-check-label" for="exampleRadios1">
                Private
              </label>
            </div>
            <div class="form-check">
              <input class="form-check-input" type="radio" name="publishStatus" id="exampleRadios1" value="-1">
              <label class="form-check-label" for="exampleRadios1">
                Pre-release
              </label>
            </div>
            <div class="form-check">
              <input class="form-check-input" type="radio" name="publishStatus" id="exampleRadios1" value="1">
              <label class="form-check-label" for="exampleRadios1">
                Public
              </label>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-12">
            <?= $this->Form->button('Add',[
              "type"=>"submit",
              "class"=>"btn btn-primary",
              "data-action" => 'add-release'
            ]); ?>

            <div class="progress">
              <div id="upload-progress" class="progress-bar" style="width:0%"></div>
            </div> 
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="modal" tabindex="-1" role="dialog" id="editReleaseModal">
  <div class="modal-dialog modal-lg modal-dialog-centered">
    <div class="modal-content rounded-0">

      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title">
          Edit version <span></span> for <?= h($project->name); ?>
        </h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>

      <!-- Modal body -->
      <div class="modal-body">
        <?= $this->Form->create(null, [
          "url" => $this->Url->build([
            '_name' => 'api:v1:projects:edit-release',
            $release->id
          ]),
        ]); ?>
          <div class="row">
            <div class="col-12">
              <?= $this->Form->control('message', [
                'type' => 'text',
                'label' => false,
                'placeholder' => 'Release description',
                'class' => 'form-control rounded-0'
              ]); ?>
            </div>
          </div>
          <div class="row">
            <div class="col-12" data-id="changes">
            </div>
          </div>
          <div class="row" data-id="publish-statuses">
            <div class="col-12">
              <div class="form-check">
                <input class="form-check-input" type="radio" name="publishStatus" id="exampleRadios1" value="0">
                <label class="form-check-label" for="exampleRadios1">
                  Private
                </label>
              </div>
              <div class="form-check">
                <input class="form-check-input" type="radio" name="publishStatus" id="exampleRadios1" value="-1">
                <label class="form-check-label" for="exampleRadios1">
                  Pre-release
                </label>
              </div>
              <div class="form-check">
                <input class="form-check-input" type="radio" name="publishStatus" id="exampleRadios1" value="1">
                <label class="form-check-label" for="exampleRadios1">
                  Public
                </label>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-12">
              <?= $this->Form->button('Save',[
                "type"=>"submit",
                "class"=>"btn btn-primary",
                "data-action" => 'save-release',
                "data-release-id" => ""
              ]); ?>
            </div>
          </div>
        <?= $this->Form->end(); ?>
      </div>
    </div>
  </div>
</div>

<?php \Admiral\Admiral\Asset::scriptStart(); ?>
<script>
var changeTemplate;
var changesObj = {};
var changeIndex = 0;
var fileObj;

$(function() {
  changeTemplate = Handlebars.compile($("#change-template").html());

  $("#changes").append(changeTemplate({
    index: changeIndex++,
    isNew: true
  }));
});

$(document).on('click','[data-release][data-action=view-changelog]', function() {
  var version = $(this).data("version");
  $.ajax({
    url: `<?= h($this->Url->build('/graphql', true)); ?>`,
      method: 'POST',
      contentType: 'application/json',
      headers: {
        'X-CSRF-Token': <?= json_encode($this->request->getParam('_csrfToken')); ?>
      },
      data: JSON.stringify({
        query: `query{project_release(id:${$(this).data("release")}){changelog{change}}}`
      }),
      dataType: 'json'
  }).done(function(res) {
    $("#changelogModal .modal-header .modal-title span").text(version);

    $("#changelogModal .modal-body ul").html("");

    $.each(res.data.project_release.changelog, function (index, change) {
      $("#changelogModal .modal-body ul").append(`<li>${change.change}</li>`);
    });

    $('#changelogModal').modal({
      backdrop: 'static',
      keyboard: false
    });
  });
});

$(document).on('click','[data-action=add-release]', function() {  
  $('#addReleaseModal').modal({
    backdrop: 'static',
    keyboard: false
  });
});

$(document).on('hide.bs.modal', '#addReleaseModal', function() {
  changesObj = {};
  changeIndex = 0;
  fileObj = null;
});

$(document).on('hide.bs.modal', '#editReleaseModal', function() {
  changesObj = {};
  changeIndex = 0;
});

$(document).on('click', '#addReleaseModal [data-action=add-change]', function(e) {
  e.preventDefault();

  // Make sure a value had been entered
  var value = $(`#addReleaseModal input[data-change-index=${$(this).data("change-index")}]`)[0].value;
  if(value === "") {
    toastr.error('Change can not be empty!', 'Ehh...');
    return;
  }

  // Push the value to the object
  changesObj[$(this).data("change-index")] = value;

  // Change the button to a removal button
  var button = $(`button[data-action=add-change][data-change-index=${$(this).data("change-index")}]`)
    .toggleClass('btn-success btn-danger');
  button.find('i').toggleClass('fa-plus fa-minus');
  button.attr("data-action", 'remove-change');

  // Add a new change input
  $("#changes").append(changeTemplate({
    index: changeIndex++,
    isNew: true
  }));
});

$(document).on('click', '#editReleaseModal [data-action=add-change]', function(e) {
  e.preventDefault();

  // Make sure a value had been entered
  var value = $(`#editReleaseModal input[data-change-index=${$(this).data("change-index")}]`)[0].value;
  if(value === "") {
    toastr.error('Change can not be empty!', 'Ehh...');
    return;
  }

  // Push the value to the object
  changesObj[$(this).data("change-index")] = {
    id: null,
    change: value
  };

  // Change the button to a removal button
  var button = $(`button[data-action=add-change][data-change-index=${$(this).data("change-index")}]`)
    .toggleClass('btn-success btn-danger');
  button.find('i').toggleClass('fa-plus fa-minus');
  button.attr("data-action", 'remove-change');

  // Add a new change input
  $("#editReleaseModal .modal-body div[data-id=changes]").append(changeTemplate({
    index: changeIndex++,
    isNew: true
  }));
});

$(document).on('click', '#addReleaseModal [data-action=remove-change]', function(e) {
  e.preventDefault();

  // Remove the change from the list
  delete changesObj[$(this).data("change-index")];
  $(`div[data-change-index=${$(this).data("change-index")}]`).remove();
});

$(document).on('click', '#editReleaseModal [data-action=remove-change]', function(e) {
  e.preventDefault();

  if(changesObj[$(this).data("change-index")].id) {
    changesObj[$(this).data("change-index")].change = null;
  } else {
    delete changesObj[$(this).data("change-index")];
  }

  // Remove the change from the list
  $(`div[data-change-index=${$(this).data("change-index")}]`).remove();
});

$(document).on('click', 'button[data-action=add-release][type=submit]', function(e) {
  e.preventDefault();
  r.upload();
});

$(document).find('input[type=file]').on('change',function(){
  var reader = new FileReader();
  reader.readAsDataURL($(this).prop('files')[0]);

  reader.onloadend = function() {
    fileObj = reader.result;
    $("button[data-action=add-release][type=submit]").removeClass("disabled");
  }
});

$(document).on('click', 'a[data-action=edit-release]', function() {
  $("#editReleaseModal .modal-body [data-id=changes]").html("");
  $("#editReleaseModal .modal-body button[type=submit][data-action=save-release]").attr("data-release-id", $(this).data("release"));

  var version = $(this).data("version");
  var release = $(this).data("release");
  var getChanges = $.ajax({
    url: `<?= h($this->Url->build('/graphql', true)); ?>`,
    method: 'POST',
    contentType: 'application/json',
    headers: {
      'X-CSRF-Token': <?= json_encode($this->request->getParam('_csrfToken')); ?>
    },
    data: JSON.stringify({
      query: `query{project_release(id:${release}){changelog{id, change}}}`
    }),
    dataType: 'json'
  }).done(function(res) {
    $("#editReleaseModal .modal-header .modal-title span").text(version);
    $("#editReleaseModal .modal-body[data-id=changes]").html("");
    $("#editReleaseModal .modal-body input[name=message]").val($(`tr[data-release-id=${release}] td[data-field=release-description]`).text());


    var form = $("#editReleaseModal .modal-body form");

    $.each(res.data.project_release.changelog, function (index, change) {
      context = {
        index: index,
        id: change.id,
        value: change,
        isNew: false
      };
      
      changesObj[index] = {
        id: change.id,
        change: change
      };
      changeIndex++;
      
      $("#editReleaseModal .modal-body [data-id=changes]").append(changeTemplate(context));
    });

    $("#editReleaseModal .modal-body [data-id=changes]").append(changeTemplate({
      index: changeIndex++,
      isNew: true
    }));
  });

  var getInfo = $.ajax({
    url: `<?= h($this->Url->build('/graphql', true)); ?>`,
    method: 'POST',
    contentType: 'application/json',
    data: JSON.stringify({
      query: `query{project_release(id:${release}){published,changelog{id,change}}}`
    }),
    dataType: 'json'
  }).done(function(res) {
    const published = res.data.project_release.published;
    // Reset the checkboxes
    $("#editReleaseModal [name=publishStatus]").prop('checked', false);
    $(`#editReleaseModal [name=publishStatus][value=${published}]`).prop('checked', true);
  });

  Promise.all([getChanges, getInfo]).then(function() {
    $('#editReleaseModal').modal({
      backdrop: 'static',
      keyboard: false
    });
  });
});

$(document).on('click', 'button[type=submit][data-action=save-release]', function(e) {
  e.preventDefault();

  // Update the current changes
  $.each($('[data-change-id]'), function(index, change) {
    changesObj[$(change).data("change-index")].change = $(change).val();
  });

  var fd = new FormData();
  fd.append('release', $(this).data("release-id"));
  fd.append('description', $("#editReleaseModal .modal-body input[name=message]").val());
  fd.append('changes', JSON.stringify(changesObj));
  fd.append('published', $("#editReleaseModal input[name=publishStatus]:checked").val());

  $.ajax({
    url: `/api/v1/projects/edit-release/${$(this).data("release-id")}`,
    method: 'POST',
    headers: {
      'X-CSRF-Token': <?= json_encode($this->request->getParam('_csrfToken')); ?>
    },
    data: fd,
    processData: false,
    contentType: false
  }).done(function(res) {
    switch(res.status) {
      case "success":
        toastr.success(res.message);
        $("#editReleaseModal").modal('hide');
        break;
      case "failure":
        toastr.error(res.message);
        break;
    }
  });
});
</script>

<script id="change-template" type="text/x-handlebars-template">
  <div class="row mt-1" data-change-index="{{ index }}">
    <div class="col-11">
      {{#if isNew}}
        <input type="text" placeholder="Change" class="form-control d-inline rounded-0" data-change-index="{{ index }}">
      {{else}}
        <input type="text" placeholder="Change" class="form-control d-inline rounded-0" data-change-index="{{ index }}" data-change-id="{{ id }}" value="{{ value.change }}">
      {{/if}}
    </div>
    <div class="col-1">
      {{#if isNew}}
        <button class="btn btn-success btn-sm rounded-0" data-action="add-change" data-change-index="{{ index }}">
          <i class="fas fa-plus"></i>
        </button>
      {{else}}
        <button class="btn btn-danger btn-sm rounded-0" data-action="remove-change" data-change-index="{{ index }}">
          <i class="fas fa-minus"></i>
        </button>
      {{/if}}
    </div>
  </div>
</script>

<?= $this->Html->script('FinlayDaG33k/FileUpload.resumable.js'); ?>
<?= $this->Html->script('FinlayDaG33k/FileUpload.resumable-uploader.js'); ?>

<script>
var r = new Resumable({
  target:'<?= $this->Url->build(['_name' => 'api:v1:projects:add-dist', $project->id]); ?>',
  chunkSize: 0.5*1024*1024,
  headers: {
    'X-CSRF-Token': <?= json_encode($this->request->getParam('_csrfToken')); ?>
  }
});

r.assignBrowse($("button[data-action=add-dist-file]"));

r.on('fileAdded', function(file){
  $("#fileName").text(file.fileName);
  $("#upload-progress").css("width", 0 + "%");
});

r.on('fileSuccess', function(file,message){
  $("#upload-progress").css("width", 100 + "%");

  var fd = new FormData();
  fd.append('version', $("#version").val());
  fd.append('description', $("#message").val());
  fd.append('changes', JSON.stringify(changesObj));
  fd.append('filename', file.fileName);
  fd.append('published', $("#addReleaseModal input[name=publishStatus]:checked").val())

  $.ajax({
    url: '<?= $this->Url->build(['_name' => 'api:v1:projects:add-release', $project->id]); ?>',
    method: 'POST',
    headers: {
      'X-CSRF-Token': <?= json_encode($this->request->getParam('_csrfToken')); ?>
    },
    data: fd,
    processData: false,
    contentType: false
  }).done(function(res) {
    console.log(res);
  });
});

r.on('fileError', function(file, message){
  console.log("oof");
});

r.on('progress', function(){
  $("#upload-progress").css("width", (r.progress() * 100) + "%");
});
</script>
<?php \Admiral\Admiral\Asset::scriptEnd(); ?>