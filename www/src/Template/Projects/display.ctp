<?php
  $slogans = [
    'Stuff that tends to break',
    'My arsenal of stuff',
    'Got redundant subheader for the sake of design?'
  ];
  $this->pageTitle = 'Projects';
?>

<div class="row" id="intro">
  <div class="col-md-12">
    <div class="jumbotron" id="intro-jumbotron">
      <h1 class="display-3">Projects</h1>
      <p class="lead"><?= $slogans[mt_rand(0, count($slogans) - 1)]; ?></p>
    </div>             
  </div>
</div>

<div class="container mb-2">
  <div class="row">
    <?php foreach($projects as $project): ?>
      <div class="col-md-4">
        <?= $this->element('project-card', ['project' => $project]); ?>
      </div>
    <?php endforeach; ?>
  </div>
</div>
