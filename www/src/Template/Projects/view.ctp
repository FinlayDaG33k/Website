<?php
  use Admiral\Admiral\Permission;
  $this->pageTitle = $project->name;
?>


<div class="container">
  <div class="row mt-4">
    <div class="col-6">
      <?php if(!empty($project->thumbnail) && $project->thumbnail != '#'): ?>
        <?= $this->Html->image($project->thumbnail,['class'=>'img-fluid fadein']); ?>
      <?php else: ?>
        <?= $this->Html->image("placeholders/1920x1080.png",['class'=>'img-fluid fadein']); ?>
      <?php endif; ?>
    </div>
    <div class="col-6">
      <div class="row">
        <div class="col-12">
          <h4><?= h($project->name); ?></h4>
          <hr>
          <p>
            <?= h($project->projects_description->description); ?>
          </p>
        </div>
      </div>
      <div class="row">
        <div class="col-8">
          <table class="table table-borderless">
            <tbody>
              <tr>
                <th class="p-0">Latest Versions</th>
                <td class="p-0">
                  <ul class="list-unstyled mb-0">
                    <?php if(!empty($project->latest['public'])): ?>
                      <li>
                        <?= h($project->latest['public']->version); ?>
                        (public)
                      </li>
                    <?php endif; ?>
                    <?php if(!empty($project->latest['testing'])): ?>
                      <li>
                        <?= h($project->latest['testing']->version); ?>
                        (testing)
                      </li>
                    <?php endif; ?>
                  </ul>
                </div>
                </td>
              </tr>
              <tr>
                <th class="p-0">License</th>
                <td class="p-0">
                  <?php if(empty($project->license)): ?>
                    Unknown
                  <?php else: ?>
                    <?= h($project->license); ?>
                  <?php endif; ?>
                </td>
              </tr>
              <tr>
                <th class="p-0">Source</th>
                <td class="p-0">
                  <?php if(!empty($project->source) && $project->source != '#'): ?>
                    <a href="<?= h($project->source); ?>" target="_blank">View Source</a>
                  <?php else: ?>
                    Unavailable
                  <?php endif;?>
                </td>
              </tr>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
  <div class="row mt-2">
    <div class="col-12">
      <h4>Version History</h4>
      <hr>
      <table class="table table-borderless">
        <thead>
          <th>Version</th>
          <th>Date</th>
          <th>Description</th>
          <th>Changelog</th>
          <th>Download</th>
        </thead>
        <tbody>
          <?php foreach($project->releases as $release): ?>
            <tr>
              <td><?= h($release->version); ?></td>
              <td><?= h($release->date->i18nFormat('MMM dd, yyyy kk:mm:ss')); ?></td>
              <td><?= h($release->description); ?></td>
              <td>
                <a class="btn btn-primary rounded-0 text-white" data-action="changelog" data-version="<?= h($release->version); ?>" data-release="<?= h($release->id); ?>">
                  <i class="fas fa-eye"></i>
                </a>
              </td>
              <td>
              <?php if(file_exists(WWW_ROOT . 'uploads' .DS . 'distfiles/' . $project->name . '-' . $release->version . '-win.zip')): ?>
                  <a class="btn btn-success rounded-0" href="/uploads/distfiles/<?= h($project->name); ?>-<?= h($release->version); ?>-win.zip" target="_blank" data-action="download" data-version="<?= h($release->version); ?>" data-release="<?= h($release->id); ?>">
                    <i class="fab fa-windows"></i>
                  </a>
                <?php endif; ?>
                <?php if(file_exists(WWW_ROOT . 'uploads' .DS . 'distfiles/' . $project->name . '-' . $release->version . '-linux.zip')): ?>
                  <a class="btn btn-success rounded-0" href="/uploads/distfiles/<?= h($project->name); ?>-<?= h($release->version); ?>-linux.zip" target="_blank" data-action="download" data-version="<?= h($release->version); ?>" data-release="<?= h($release->id); ?>">
                    <i class="fab fa-linux"></i>
                  </a>
                <?php endif; ?>
              </td>
            </tr>
          <?php endforeach; ?>
        </tbody>
      </table>
    </div>
  </div>
</div>

<div class="modal" tabindex="-1" role="dialog" id="changelogModal">
  <div class="modal-dialog modal-lg modal-dialog-centered">
    <div class="modal-content rounded-0">
      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title">
          Changelog for version <span></span> of <?= h($project->name); ?>
        </h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>

      <!-- Modal body -->
      <div class="modal-body">
        <ul></ul>
      </div>
    </div>
  </div>
</div>
