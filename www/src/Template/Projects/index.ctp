<table id="projectstable" class="table table-striped table-sm">
  <thead>
    <th>Project</th>
    <th>View</th>
    <th>Edit</th>
  </thead>
  <tbody>
    <?php foreach($projects as $project): ?>
      <tr>
        <td><?= h($project->name); ?></td>
        <td>
          <?= 
            $this->Html->link(
              $this->Html->tag(
                'button',
                $this->Html->tag('span',null,['class'=>'far fa-eye','escape'=>false]),
                [
                  'escape'=>false,
                  'class'=>'btn btn-primary btn-xs',
                  'data-placement'=>'top',
                  'data-toggle'=>"tooltip",
                  'title'=>"View " . h($project->name) 
                ]
              ),
              ['plugin'=>null,'controller'=>'Projects','action'=>'view',$project->id],
              ['escape'=>false, 'target'=>'_blank']
            ); 
          ?>
        </td>
        <td>
          <?= 
            $this->Html->link(
              $this->Html->tag(
                'button',
                $this->Html->tag('span',null,['class'=>'far fa-edit','escape'=>false]),
                [
                  'escape'=>false,
                  'class'=>'btn btn-primary btn-xs',
                  'data-placement'=>'top',
                  'data-toggle'=>"tooltip",
                  'title'=>"Edit " . h($project->name) 
                ]
              ),
              ['plugin'=>null,'controller'=>'Projects','action'=>'edit',$project->id],
              ['escape'=>false]
            ); 
          ?>
        </td>
      </tr>
    <?php endforeach; ?>
  </tbody>
  <!-- This is actually the footer -->
  <thead>
    <th>Project</th>
    <th>View</th>
    <th>Edit</th>
  </thead>
</table>

<script>
  $(function(){
    $("[data-toggle=tooltip]").tooltip();
  });
</script>
