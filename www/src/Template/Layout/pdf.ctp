<?= $this->Html->docType(); ?>
<html lang="en">
  <head>
    <?= $this->Html->charset(); ?>
    <!-- Theme -->
    <?= $this->Asset->css('/css/themes/light.css'); ?>

    <!-- Stylesheets -->
    <?= $this->Asset->css('/css/vendor/bootstrap/bootstrap.css'); ?>
    <?= $this->Asset->css('/css/main.css'); ?>
    <?= $this->Asset->css('/css/vendor/fontawesome/fontawesome.css'); ?>
    <?= $this->Asset->css('/css/vendor/glyphicons/glyphicons.css'); ?>
    <?= $this->Asset->css('/css/vendor/prism.css'); ?>
    <?= $this->Asset->css('/css/vendor/toastr.css'); ?>

    <style>
      new-page {
        page-break-before: always;
      }

      body {
        font-size: 12px;
      }
    </style>
  </head>
  <body>
    <header>
      <section id="top-cards">
        <div class="container">
          <div class="row">
            <div class="col-md-3">
              <a class="brand" href="/"><?= $this->Html->image('/img/logo-FDG-300-01-300x300.png'); ?></a>
            </div>
            <div class="col-md-4">
            </div>
          </div>
        </div>
      </section>
    </header>

    <main>
      <?= $this->fetch('content'); ?>
    </main>

    <?= $this->fetch('scripts'); ?>
  </body>
</html>
