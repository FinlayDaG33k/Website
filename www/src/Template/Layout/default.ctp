<?= $this->Html->docType(); ?>
<html lang="en">
<head>
  <title>
    <?php if(!empty($this->pageTitle)): ?>
      <?= h($this->pageTitle); ?> - FinlayDaG33k
    <?php else: ?>
      Some page - FinlayDaG33k
    <?php endif; ?>
  </title>

  <!-- Meta tags -->
  <?= $this->Html->charset(); ?>
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="Just a random nerd on the internet">
  <meta name="keywords" content="HTML,CSS,JavaScript,PHP,Rants">
  <meta name="author" content="Aroop Roelofs">
  <meta property="og:description" content="Just a random nerd on the internet">
  <meta property="og:site_name" content="FinlayDaG33k's Website">
  <meta property="og:type" content="website">
  <meta property="og:url" content="<?= $this->Url->build(null, true); ?> ">

  <?= $this->cache(function() use ($user_settings) { ?>
    <!-- Preloads -->
    <?= $this->Asset->bundle('vendor-css', ['hash' => true, 'preload' => true]); ?>
    <?= $this->Asset->css('/css/main.css', ['hash' => true, 'preload' => true]); ?>
    <?= $this->Asset->font('/fonts/fontawesome/fa-brands-400.woff2', ['hash' => false, 'preload' => true, 'crossorigin' => true]); ?>
    <?= $this->Asset->font('/fonts/fontawesome/fa-solid-900.woff2', ['hash' => false, 'preload' => true, 'crossorigin' => true]); ?>
    <?= $this->Asset->font('/fonts/fontawesome/fa-regular-400.woff2', ['hash' => false, 'preload' => true, 'crossorigin' => true]); ?>

    <!-- Favicons -->
    <link rel="apple-touch-icon" sizes="57x57" href="/img/favicon/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="/img/favicon/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="/img/favicon/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="/img/favicon/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="/img/favicon/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="/img/favicon/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="/img/favicon/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="/img/favicon/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="/img/favicon/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192"  href="/img/favicon/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/img/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="/img/favicon/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/img/favicon/favicon-16x16.png">
    <link rel="manifest" href="/img/favicon/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="/img/favicon/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">

    <!-- Stylesheets -->
    <?= $this->Asset->bundle('vendor-css', ['hash' => true]); ?>
    <?= $this->Asset->css('/css/main.css', ['hash' => true]); ?>

    <!-- JavaScript -->
    <?= $this->Asset->bundle('vendor-js', ['hash' => true], ['defer' => true]); ?>
    <?= $this->Asset->bundle('app-js', ['hash' => true], ['defer' => true]); ?>
  <?php }, ['key' => 'Assets Header' . (env('DEBUG', false) ? \Cake\Utility\Security::randomString((4)) : '')]); ?>

  <?php if($user_settings['analytics']): ?>
    <!-- Analytics -->
    <?= $this->Asset->js('/js/analytics.js', ['plugin' => 'FinlayDaG33k/Analytics', 'hash' => true], ['defer' => true]); ?>
    <?= $this->Asset->js('/js/analytics.js', ['hash' => true], ['defer' => true]); ?>
  <?php endif; ?>

  <!-- Theme -->
  <?php
    switch($this->Festivity->getFestivity()) {
      case 'Saint Patricks Day':
        echo $this->Asset->css('/css/themes/saint-paddys.css', ['hash' => true], ['defer' => true]);
        break;
      default:
        echo $this->Asset->css('/css/themes/default.css', ['hash' => true], ['defer' => true]);
        break;
    }
  ?>
</head>


<body>
    <header>
      <section id="top-cards">
        <div class="container">
          <div class="row">
            <div class="col-md-3">
              <a class="brand" href="/"><?= $this->Asset->img('/img/logo-FDG-54x54.webp', ['hash' => true, 'preload' => true]); ?></a>
            </div>
            <div class="col-md-4">
            </div>
          </div>
        </div>
      </section>
      <?= $this->element('nav') ?>
    </header>

    <main>
      <?= $this->fetch('content') ?>
      <?= $this->element('global-modals'); ?>
    </main>
    <footer class="page-footer">
      <?= $this->cache(function() {
        echo $this->element('footer');
      }, ['key' => 'Footer']); ?>
    </footer>

    <!-- Additional scripts -->
    <?= $this->fetch('scripts'); ?>
  </body>
</html>
