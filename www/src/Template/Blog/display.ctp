<?php $this->pageTitle = 'Blog Posts'; ?>

<div class="row no-banner mt-3">
  <div class="container">
    <?php foreach ($articles as $article): ?>
      <?= $this->element('blog-card', ['article' => $article]); ?>
    <?php endforeach; ?>
  </div>
</div>
<div class="row">
  <div class="container">
    <nav>
      <ul class="pagination">
        <?= $this->Paginator->first(); ?>
        <?= $this->Paginator->numbers([
          'modulus' => 4,
          'first' => 3,
          'last' => 4
        ]); ?>
        <?= $this->Paginator->last(); ?>
      </ul>
    </nav>
  </div>
</div>
