<?php $this->pageTitle = $article->title; ?>

<!-- Article header -->
<div class="row" id="intro">
  <div class="col-md-12">
    <div class="jumbotron" id="intro-jumbotron">
      <h1 class="display-3"><?= $article->title; ?></h1>
    </div>             
  </div>
</div>

<!-- Actual article -->
<div class="row">
  <div class="container">
    <?= $this->Flash->render(); ?>
    <?= $this->Shortcode->doShortcode($body, [
      'cache' => $shortcodeCache
    ]); ?>
  </div>
</div>

<!-- Tags -->
<div class="row pt-4">
  <div class="container">
    <div class="card rounded-0 mb-3">
      <div class="card-body">
        <p class="card-text">
          <button class="btn btn-info bg-primary-normal rounded-0"><i class="fas fa-tags"></i></button>
          <?php foreach($article->tags as $tag): ?>
            <button class="btn btn-primary bg-primary-light rounded-0"><?= h($tag->title); ?></button>
          <?php endforeach; ?>
        </p>
      </div>
    </div>
  </div>
</div>

<!-- Comments -->
<div class="row">
  <div class="container">
    <div class="card rounded-0 mb-3">
      <div class="card-body">
        <p class="card-text">
          <h4>Comments</h4>
          <hr>
          <div id="comments-section"></div>
          <center>
            <button class="btn btn-primary w-100 bg-primary-normal rounded-0" id="btn-load-comments">
              <div class="sk-cube-grid" style="display: none;">
                <div class="sk-cube sk-cube1"></div>
                <div class="sk-cube sk-cube2"></div>
                <div class="sk-cube sk-cube3"></div>
                <div class="sk-cube sk-cube4"></div>
                <div class="sk-cube sk-cube5"></div>
                <div class="sk-cube sk-cube6"></div>
                <div class="sk-cube sk-cube7"></div>
                <div class="sk-cube sk-cube8"></div>
                <div class="sk-cube sk-cube9"></div>
              </div>
              Load more comments
            </button>
          </center>
        </p>
      </div>
    </div>
  </div>
</div>

<!-- Leave a comment -->
<div class="row">
  <div class="container">
    <div class="card rounded-0 mb-3">
      <div class="card-body">
        <p class="card-text">
          <h4>Leave a comment</h4>
          <hr>
          <?php if(!empty($user)): ?>
            <form id="comment-form">
              <div class="form-group text-muted">
                <i class="fas fa-reply" aria-hidden="true"></i>
                <span id="reply-to-name"><?= h($article->title); ?></span>
              </div>
              <div class="form-group">
                <textarea name="body" class="form-control rounded-0" rows="5" style="resize: none;"></textarea>
              </div>
              <div class="form-group">
                <button type="submit" class="btn btn-primary bg-primary-normal rounded-0">
                  <i class="fas fa-reply" aria-hidden="true"></i> Reply
                </button>
                <button type="reset" class="btn btn-danger">
                  <i class="fas fa-trash-alt" aria-hidden="true"></i> Cancel
                </button>
              </div>
              <input type="hidden" id="form-replyto" name="replyto">
            </form>
          <?php else: ?>
            Please login to leave comment!
          <?php endif; ?>
        </p>
      </div>
    </div>
  </div>
</div>

<?php $this->append('scripts'); ?>
<script>
  var currCommentsPage = 1;
  var template;
  document.addEventListener('DOMContentLoaded', async () => {
    // Initialize a new Handlebars loader
    const hbl = new HandlebarsLoader('/js/handlebars-templates', '.handlebars');

    // Get our blog-comment handlebars template
    // Compile it as well
    const templateResp = await hbl.load('blog-comment');
    template = Handlebars.compile(templateResp);

    loadComments();

    $("#btn-load-comments").on('click', function() {
      var button = $(this);
      var icon = button.children()[0];
      icon.style = "display: inline-block";
      button.addClass('disabled');
      button.prop('disabled', true);

      loadComments().done(function(){
        icon.style = "display: none";
        button.removeClass('disabled');
        button.prop('disabled', false);
      })
      .fail(function(res) {
        console.log(res);
        switch(res.status) {
          case 404:
            toastr.error("Could not load more comments");
            break;
        }

        currCommentsPage--;

        icon.style = "display: none";
        button.removeClass('disabled');
        button.prop('disabled', false);
      });
    });

    $(document).on('click',"[id^=btn-reply]", function() {
      $("#form-replyto").val($(this).data("comment-id"));
      $("#reply-to-name").text($(this).data("user"));
    })

    $("button:reset").on('click', function() {
      console.log("yeet");
      $("#form-replyto").removeAttr('value');
      $("#reply-to-name").text("<?= h($article->title); ?>");
    });
  });

  function loadComments() {
    return $.ajax({
      url: '/api/comments/getByArticle/<?= h($article->id); ?>/' + currCommentsPage++,
      success: function(res) {
        $.each(res.result, function (key, value) {
          $("#comments-section").append(template(value.comment));
        });
      }
    });
  }
</script>

<?php if(!empty($user)): ?>
  <script>
    document.addEventListener('DOMContentLoaded', () => {
      $("#comment-form").on('submit', function(event) {
        event.preventDefault();

        var fd = new FormData();
        fd.append("body", $(this).find('textarea[name=body]').val());
        fd.append("replyto", $(this).find('input[name=replyto]').val());

        $.ajax({
          url: '/api/comments/<?= h($article->id); ?>',
          method: 'POST',
          headers: {
            'X-CSRF-Token': <?= json_encode($this->request->getParam('_csrfToken')); ?>
          },
          data: fd,
          contentType: false,
          processData: false,
        }).done(function(res) {
          // Reset the form fields
          $("#comment-form").find('textarea[name=body]').val("");
          $("#form-replyto").removeAttr('value');
          $("#reply-to-name").text("<?= h($article->title); ?>");

          // Show a toast
          toastr.success("Your comment has been posted!");
        }).fail(function(res) {
          toastr.error("Something went wrong while trying to post your comment");
        });
      });
    });
  </script>
<?php endif; ?>
<?php $this->end(); ?>