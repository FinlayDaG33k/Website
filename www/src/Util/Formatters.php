<?php
  namespace App\Util;

  use Cake\I18n\Time;

  class Formatters {
    public static function getDaysection() {
      $now = Time::now();
      $hour = $now->i18nFormat("HH");

      // Morning (0000 -> 1159)
      if ($hour < "12") {
        return "morning";
      }

      // Afternoon (1200 -> 1659)
      else if ($hour >= "12" && $hour < "17") {
        return "afternoon";
      }

      // Evening (1700 -> 1859)
      else if ($hour >= "17" && $hour < "19") {
        return "evening";
      }

      // Night (1900 -> 2359)
      else if ($hour >= "19") {
        return "night";
      }
    }
  }