<?php


  namespace App\Util;


  class Preload {
    private static $assets = [];
    private static $autoDetectTypes = [
      "/\.js(\?.*)?$/i" => 'script',
      "/\.css(\?.*)?$/i" => 'style',
      "/\.(jpe?g|png|gif|apng|tiff|bmp|webp|ico)(\?.*)?$/i" => 'image',
      "/\.(woff2)?$/i" => 'font/woff2'
    ];

    public static function add(string $input, $type = null, $crossorigin = false) {
      // Make sure we didn't already define the asset
      if(!empty(self::$assets[$input])) return;

      // Check whether a type is specified
      // If not, auto detect it
      if($type === null) {
        foreach(self::$autoDetectTypes as $regex => $pushType) {
          if(preg_match($regex, $input)) {
            $type = $pushType;
            break;
          }
        }
      }

      // Throw an exception if auto detection failed
      if ($type === null) throw new RuntimeException("Could not detect h2 push type for asset \"$input\", please specify in filter.");

      // Add our asset to our array
      self::$assets[$input] = [
        'type' => $type,
        'crossorigin' => $crossorigin,
      ];
    }

    public static function get(): ?string {
      // Array to hold our assets
      $assets = [];

      // Loop over assets to build link substrings
      // Check whether it needs to be loaded cross-origin or not
      // Then build the asset substring accordingly
      foreach(self::$assets as $asset => $config) {
        $type = $config['type'];
        if ($config['crossorigin']) {
          $assets[] = "<$asset>; rel=preload; as=$type; crossorigin";
        } else {
          $assets[] = "<$asset>; rel=preload; as=$type";
        }
      }

      // Check if we have atleast one asset
      // If not, just return null
      if(count($assets) < 1) return null;

      // Build our header
      $header = implode(',', $assets);

      // Return our header
      return $header;
    }
  }
