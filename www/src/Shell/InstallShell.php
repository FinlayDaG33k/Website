<?php
// src/Shell/InstallShell.php
namespace App\Shell;

use Cake\Console\Shell;
use Cake\Core\Plugin;
use Cake\ORM\TableRegistry;
use Cake\Utility\Inflector;

class InstallShell extends Shell {
  public function migrations() {
    // Load a list of the plugins
    $plugins = Plugin::loaded();

    // Define the order in which to migrate the plugins
    $order = [
      'Admiral/Admiral',
      'Admiral/Blog',
      'Admiral/Contact',
      'FinlayDaG33k/Analytics',
      'FinlayDaG33k/Comments'
    ];

    // exclude plugins
    $excludes = [
      'DebugKit',
      'Bake',
      'Migrations',
      'WyriHaximus/TwigView'
    ];

    foreach($excludes as $exclude) {
      // Remove the plugin from the list
      unset($plugins[array_search($exclude, $plugins)]);
    }

    // Loop over all the plugins in the order list
    foreach($order as $entry){
      // Check if the plugin is installed
      if(in_array($entry,$plugins)){
        // Plugin is installed
        // Run it's migrations
        $this->dispatchShell('migrations','migrate','-p',$entry);
      }

      // Remove the plugin from the list
      unset($plugins[array_search($entry, $plugins)]);
    }

    // Migrate the left-over plugins
    foreach($plugins as $plugin) {
      $this->dispatchShell('migrations','migrate','-p',$plugin);
    }

    // Migrate the main app
    $this->dispatchShell('migrations','migrate');
  }

  public function blog() {
    $path = '/var/www/imports';

    echo "Importing users...";
    $usersData = json_decode(file_get_contents($path . '/users.json'),true);
    $usersTable = TableRegistry::get('Admiral/Admiral.Users');
    $memberRole = $usersTable->Roles->findByName('Member')->first();
    foreach($usersData as $user) {
      $data = [
        'username' => $user['user_nicename'],
        'email' => $user['user_email'],
        'password' => $user['user_pass'],
        'created' => $user['user_registered'],
        'users_detail' => [
          'firstname' => $user['user_nicename'],
          'lastname' => ''
        ],
        'roles' => [
          ['id' => $memberRole->id]
        ]
      ];
      $usersEntity = $usersTable->newEntity($data);
      $usersEntity->id = $user['ID'];
      $usersTable->save($usersEntity);
    }

    echo "Importing tags...";
    $tagsData = json_decode(file_get_contents($path . '/categories.json'),true);
    $tagsTable = TableRegistry::get('Admiral/Blog.Tags');
    foreach($tagsData as $i => $tag) {
      foreach($tag as $id => $title) {
        $tag = $tagsTable->newEntity();
        $tag->id = $id;
        $tag->title = $title;

        $tagsTable->save($tag);
      }
    }

    echo "Importing Posts...";
    $articlesData = json_decode(file_get_contents($path . '/posts.json'),true);
    $articlesTable = TableRegistry::get('Admiral/Blog.Articles');
    foreach($articlesData as $article) {
      $articleEntity = $articlesTable->newEntity();
      $articleEntity->id = $article['id'];
      $articleEntity->user_id = 1;
      $articleEntity->title = $article['post_title'];
      $articleEntity->created = $article['post_date'];
      $articleEntity->modified = $article['post_modified'];

      // Clean up the post a bit
      $article['post_content'] = str_replace('&nbsp;', '', $article['post_content']);
      $article['post_content'] = preg_replace('/\r\n\r\n/', '', $article['post_content']);
      $article['post_content'] = str_replace('<!-- wp:paragraph -->', '', $article['post_content']);
      $article['post_content'] = str_replace('<!-- /wp:paragraph -->', '', $article['post_content']);
      $article['post_content'] = preg_replace('/<h2>(.+)<\/h2>\\r\\n<hr \/>/', '[h]$1[/h]', $article['post_content']);

      $articleEntity->body = $article['post_content'];

      switch($article['post_status']) {
        case "publish":
          $articleEntity->published = 1;
          break;
        case "private":
          $articleEntity->published = 0;
          break;
        case "draft":
          $articleEntity->published = 2;
          break;
      }
      
      // Check if the slug exists, and if not, add a number behind it
      $slug = Inflector::slug($article['post_title']);
      if($articlesTable->exists(['slug' => $slug])){
        $iteration = 1;
        while($articlesTable->exists(['slug' => $slug])){
          $slug= Inflector::slug($article['post_title'] . "-" . $iteration);
          $iteration++;
        }
      }

      $articleEntity->slug = $slug;

      $articlesTable->save($articleEntity);
    }

    echo "Importing post categories...";
    $articlesTagsData = json_decode(file_get_contents($path . '/posts_categories.json'),true);
    $articlesTagsTable = TableRegistry::get('Admiral/Blog.ArticlesTags');
    foreach($articlesTagsData as $articleTag) {
      $entity = $articlesTagsTable->newEntity([
        'article_id' => $articleTag['object_id'],
        'tag_id' => $articleTag['term_taxonomy_id']
      ]);
      $articlesTagsTable->save($entity);
    }

    
    echo "Importing comments...";
    $commentsData = json_decode(file_get_contents($path . '/comments.json'),true);
    $commentsTable = TableRegistry::get('FinlayDaG33k/Comments.Comments');
    $articlesCommentsTable = TableRegistry::get('FinlayDaG33k/Comments.ArticlesComments');
    foreach($commentsData as $comment) {
      $data = [
        'user_id' => $comment['user_id'],
        'body' => $comment['comment_content'],
        'posted' => $comment['comment_date'],
        'modified' => $comment['comment_date'],
        'reply' => null,
        'status' => 1,
        'articles' => [
          ['id' => $comment['comment_post_ID']]
        ]
      ];

      $commentEntity = $commentsTable->newEntity($data);

      $commentEntity->id = $comment['comment_ID'];

      if($comment['comment_parent'] != "0") {
        $commentEntity->reply = $comment['comment_parent'];
      }

      $commentsTable->save($commentEntity);
    }
  }
}