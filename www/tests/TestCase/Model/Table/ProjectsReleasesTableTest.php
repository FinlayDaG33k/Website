<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\ProjectsReleasesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\ProjectsReleasesTable Test Case
 */
class ProjectsReleasesTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\ProjectsReleasesTable
     */
    public $ProjectsReleases;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.ProjectsReleases',
        'app.Projects'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('ProjectsReleases') ? [] : ['className' => ProjectsReleasesTable::class];
        $this->ProjectsReleases = TableRegistry::getTableLocator()->get('ProjectsReleases', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->ProjectsReleases);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
