<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\UsersDetailsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\UsersDetailsTable Test Case
 */
class UsersDetailsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\UsersDetailsTable
     */
    public $UsersDetails;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.users_details'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('UsersDetails') ? [] : ['className' => UsersDetailsTable::class];
        $this->UsersDetails = TableRegistry::getTableLocator()->get('UsersDetails', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->UsersDetails);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
