<?php
  return [
    'number' => '<li class="page-item"><a class="page-link" href="{{url}}">{{text}}</a></li>',
    'current' => '<li class="page-item active"><a class="page-link" href="{{url}}">{{text}}</a></li>',
    'first' => '<li class="page-item"><a class="page-link" href="{{url}}">First</a></li>',
    'last' => '<li class="page-item"><a class="page-link" href="{{url}}">Last</a></li>',
    'ellipsis' => '<li class="page-item disabled"><a class="page-link" href="#">...</a></li>',
  ];