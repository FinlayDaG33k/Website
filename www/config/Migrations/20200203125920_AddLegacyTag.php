<?php
use Migrations\AbstractMigration;
use Cake\ORM\TableRegistry;

class AddLegacyTag extends AbstractMigration {
  public function change() {
    // Find the tag id
    $table = TableRegistry::get('Tags');
    $tag = $table->findByTitle('Legacy WordPress Imports')->first();
    
    if(!$tag) {
      // Tag is not found
      // Create the tag
      $tag = $table->newEntity([
        'title' => 'Legacy WordPress Imports',
        'created' => date("Y-m-d H:i:s"),
        'modified' => date("Y-m-d H:i:s"),
      ]);

      $table->save($tag);
    }
    
    // Fetch all the article IDs
    $rows = $this->fetchAll("SELECT `id` FROM `articles` WHERE id < 3876;");

    // Load the table
    $table = TableRegistry::get('ArticlesTags');

    // Insert the tag for all articles
    echo "Adding tag to " . count($rows) . " article(s), this may take a while...". PHP_EOL;
    foreach($rows as $row) {
      $entry = [
        'article_id' => $row['id'],
        'tag_id' => $tag->id,
      ];

      $entity = $table->newEntity($entry);
      $table->save($entity);
    }
  }
}
