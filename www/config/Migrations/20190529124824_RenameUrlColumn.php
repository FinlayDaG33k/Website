<?php
  use Migrations\AbstractMigration;

  class RenameUrlColumn extends AbstractMigration {
    public function change() {
      $this->table('projects')
        ->renameColumn('url', 'source')
        ->save();
    }
  }