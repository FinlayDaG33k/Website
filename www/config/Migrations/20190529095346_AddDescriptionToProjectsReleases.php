<?php
  use Migrations\AbstractMigration;

  class AddDescriptionToProjectsReleases extends AbstractMigration {
    public function change() {
      $this->table('projects_releases')
        ->addColumn('description','text',['null'=>false])
        ->save();
    }
  }