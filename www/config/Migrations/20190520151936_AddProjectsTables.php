<?php
  use Migrations\AbstractMigration;

  class AddProjectsTables extends AbstractMigration {
    public function change() {
      $this->table('projects')
        ->addColumn('name','text',['default' => null,'null' => true])
        ->addColumn('url','text',['default'=>null,'null'=>false])
        ->addColumn('thumbnail','text',['default'=>null])
        ->save();
      
      $this->table('projects_descriptions')
        ->addColumn('project_id','integer',['default'=>null,'null'=>false])
        ->addColumn('description','integer',['default'=>null,'null'=>false])
        ->addForeignKey('project_id','projects','id', ['delete'=> 'CASCADE', 'update'=> 'NO_ACTION'])
        ->save();
      
      $this->table('projects_releases')
        ->addColumn('project_id','integer',['default'=>null,'null'=>false])
        ->addColumn('version','text',['default'=>null,'null'=>false])
        ->addColumn('date','datetime',['default'=>null,'null'=>false])
        ->addColumn('published','integer',['default'=>0,'null'=>false])
        ->addForeignKey('project_id','projects','id', ['delete'=> 'CASCADE', 'update'=> 'NO_ACTION'])
        ->save();
        
      $this->table('projects_changelogs')
        ->addColumn('release_id','integer',['default'=>null,'null'=>false])
        ->addColumn('change','text',['default'=>null,'null'=>false])
        ->addForeignKey('release_id','projects_releases','id', ['delete'=> 'CASCADE', 'update'=> 'NO_ACTION'])
        ->save();
    }
  }