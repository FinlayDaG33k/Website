<?php
  use Migrations\AbstractMigration;
  use Cake\ORM\TableRegistry;

  class AddProjectsPermissions extends AbstractMigration {
    public function change() {
      // Load the required tables
      $permissionsTable = TableRegistry::get('Permissions');
      $rolesTable = TableRegistry::get('Roles');
      $linkTable = TableRegistry::get('RolesPermissions');

      // Create the permission nodes and add them to the table
      $entities = $permissionsTable->newEntities([
        ['name' => 'view_projects'],
        ['name' => 'add_projects'],
        ['name' => 'edit_projects'],
        ['name' => 'delete_projects'],
        ['name' => 'view_releases'],
        ['name' => 'add_releases'],
        ['name' => 'edit_releases'],
        ['name' => 'delete_releases'],
        ['name' => 'view_prereleases'],
        ['name' => 'view_privatereleases']
      ]);
      
      foreach($entities as $entity) {
        $permissionsTable->save($entity);
      }

      // Turn our entities into a collection
      $entities = collection($entities);

      // Define which roles should get which permission
      $roles = [
        'Administrator' => [
          'view_projects' => true,
          'add_projects' => true,
          'edit_projects' => true,
          'delete_projects' => true,
          'view_releases' => true,
          'add_releases' => true,
          'edit_releases' => true,
          'delete_releases' => true,
          'view_prereleases' => true,
          'view_privatereleases' => true
        ],
        'Member' => [
          'view_prereleases' => true
        ]
      ];

      // Loop over the role-permission list
      foreach($roles as $role => $permissions) {
        $role_id = ($rolesTable->findByName($role)->first())->id;

        // Loop over the permissions for the role
        foreach($permissions as $permission => $value) {
          // Check if the role should have the permission
          if(!$value) {
            // Role should not get the permission, skip it
            continue;
          }

          $permission_id = ($entities->firstMatch(['name' => $permission]))->id;
          
          $link = $linkTable->newEntity(['role_id' => $role_id, 'permission_id' => $permission_id]);
          $linkTable->save($link);
        }
      }
    }
  }