<?php
  use Migrations\AbstractMigration;

  class UpdateProjectsDescriptionsTables extends AbstractMigration {
    public function change() {
      $this->table('projects_descriptions')
        ->changeColumn('description','text',['null'=>false])
        ->save();
    }
  }