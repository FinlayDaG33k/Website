<?php
  use Migrations\AbstractMigration;

  class AddLicenseColumn extends AbstractMigration {
    public function change() {
      $this->table('projects')
        ->addColumn('license','text',['null'=>false])
        ->save();
    }
  }