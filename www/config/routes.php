<?php
/**
 * Routes configuration
 *
 * In this file, you set up routes to your controllers and their actions.
 * Routes are very important mechanism that allows you to freely connect
 * different URLs to chosen controllers and their actions (functions).
 *
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link          https://cakephp.org CakePHP(tm) Project
 * @license       https://opensource.org/licenses/mit-license.php MIT License
 */

use Cake\Core\Plugin;
use Cake\Routing\RouteBuilder;
use Cake\Routing\Router;
use Cake\Routing\Route\DashedRoute;

/**
 * The default class to use for all routes
 *
 * The following route classes are supplied with CakePHP and are appropriate
 * to set as the default:
 *
 * - Route
 * - InflectedRoute
 * - DashedRoute
 *
 * If no call is made to `Router::defaultRouteClass()`, the class used is
 * `Route` (`Cake\Routing\Route\Route`)
 *
 * Note that `Route` does not do any inflections on URLs which will result in
 * inconsistently cased URLs when used with `:plugin`, `:controller` and
 * `:action` markers.
 *
 * Cache: Routes are cached to improve performance, check the RoutingMiddleware
 * constructor in your `src/Application.php` file to change this behavior.
 *
 */
Router::defaultRouteClass(DashedRoute::class);

Router::scope('/', function (RouteBuilder $routes) {
    $routes->connect('/',['controller' => 'Pages', 'action' => 'display', 'home']);

    $routes->scope('/about', function(RouteBuilder $routes) {
      $routes->connect('/',['controller' => 'Pages', 'action' => 'display', 'about']);
      $routes->connect('/resume',['controller' => 'Resume', 'action' => 'index', 'resume']);
      $routes->connect('/resume/pdf',['controller' => 'Resume', 'action' => 'view', 'resume']);

      $routes->fallbacks(DashedRoute::class);
    });

    //$routes->connect('/turtle-stream',['controller' => 'Pages', 'action' => 'display', 'turtle-stream']);
    $routes->connect('/blog',['plugin'=>null,'controller' => 'Blog', 'action' => 'display']);
    $routes->connect('/blog/page/:page',['plugin'=>null,'controller' => 'Blog', 'action' => 'display'])
      ->setPass(['page']);
    $routes->connect('/blog/article/:slug',['plugin'=>null,'controller' => 'Blog', 'action' => 'view'])
      ->setPass(['slug']);
    $routes->connect('/blog/article/:slug/:hash',['plugin'=>null,'controller' => 'Blog', 'action' => 'view'])
      ->setPass(['slug', 'hash']);
    $routes->connect('/blog/search',['plugin'=>null,'controller' => 'Blog', 'action' => 'display']);
    $routes->connect('/blog/search/:query',['plugin'=>null,'controller' => 'Blog', 'action' => 'display'])
      ->setPass(['query']);
    $routes->connect('/contact',['plugin'=>null,'controller' => 'Contact', 'action' => 'index', 'contact']);
    $routes->connect('/login',['controller' => 'Users', 'action' => 'login']);
    $routes->connect('/register',['controller' => 'Users', 'action' => 'register', 'register']);
    $routes->connect('/my-account',['controller' => 'Users', 'action' => 'my_account', 'my-account']);
    $routes->connect('/logout',['controller' => 'Users', 'action' => 'logout', 'logout']);
    $routes->connect('/privacy',['controller' => 'Privacy', 'action' => 'index','privacy']);
    $routes->connect('/donate',['controller' => 'Pages', 'action' => 'display', 'donate']);

    $routes->connect('/policies/reviews',['controller' => 'Pages', 'action' => 'display', 'reviews' ]);
    $routes->connect('/policies/pentesting',['controller' => 'Pages', 'action' => 'display', 'pentesting']);

    $routes->connect('/webhook', ['controller' => 'Webhook', 'action' => 'index'], ['_name' => 'webhook']);

    $routes->get('/projects', ['controller' => 'Projects', 'action' => 'display']);
    $routes->get('/projects/:id', ['controller' => 'Projects', 'action' => 'view'])
      ->setPass(['id']);

    // Band-aid patch for people that are coming here through legacy blog posts
    $routes->connect('/:year/:month/:slug', ['plugin' => null, 'controller' => 'Blog', 'action' => 'view'])
      ->setPass(['slug'])
      ->setPatterns([
        'year' => '[0-9]+',
        'month' => '[0-9]+',
      ]);

    // Redirect to Social platforms
    $routes->redirect('discord', 'https://discord.gg/fBg7KEC', ['status' => 308]);
    $routes->redirect('mastodon', 'https://social.linux.pizza/@finlaydag33k', ['status' => 308]);
    $routes->redirect('reddit', 'https://www.reddit.com/r/finlaydag33k', ['status' => 308]);
    $routes->redirect('youtube', 'https://www.youtube.com/finlaydag33k', ['status' => 308]);
    $routes->redirect('gitlab', 'https://www.gitlab.com/finlaydag33k', ['status' => 308]);
    $routes->redirect('github', 'https://www.github.com/finlaydag33k', ['status' => 308]);

    $routes->fallbacks(DashedRoute::class);
});

/**
 * Routes for the CMS Dashboard
 */
Router::scope('/admin', function ($routes) {
  $routes->scope('/projects', function($routes) {
    $routes->connect('/', 'Projects::index');
    $routes->connect('/add', ['plugin' => null, 'controller' => 'Projects', 'action' => 'add']);
    $routes->connect('/edit/:id', ['plugin' => null, 'controller' => 'Projects', 'action' => 'edit'])
      ->setPass(['id']);
  });

  $routes->fallbacks(DashedRoute::class);
});

/**
 * Routes for the API
 */
Router::scope('/api',['_namePrefix' => 'api:'], function($routes) {
  // V1 API
  $routes->scope('/v1', ['_namePrefix' => 'v1:'], function($routes) {
    // Projects
    $routes->scope('/projects', ['_namePrefix' => 'projects:'], function($routes) {
      $routes->post('/add-release/*', 'ProjectsRest::addRelease', 'add-release');
      $routes->post('/edit-release/:id', 'ProjectsRest::editRelease', 'edit-release')
        ->setPass(['id']);
      $routes->connect('/add-dist/*', 'ProjectsRest::addDist', ['_name' => 'add-dist']);
    });
  });

  $routes->fallbacks(DashedRoute::class);
});

/**
 * Routes for the GraphQL
 */
Router::scope('/graphql',['_namePrefix' => 'graphql:'], function($routes) {
  $routes->connect('/documentation', ['controller' => 'Pages', 'action' => 'display', 'graphql'], ['_name' => 'documentation']);

  $routes->fallbacks(DashedRoute::class);
});
