class Twitch
  prop isLive
  prop game
  prop start
  prop viewers

  def load
    const gql = new GraphQL('/graphql')
    const query = "query{twitch {isLive,game,start,viewers}}"
    const resp = await gql.query(query, {}) 

  def render 
    <self>
      <a.dropdown-item href="#">
        <i.fas.fas-gamepad>
          "Playing {game}"
      <a.dropdown-item href="#">
        <i.fas.fa-clock>
          "Live for ~???"
      <a.dropdown-item href="#">
        <i.fas.fa-user-astronaut>
          "{viewers} viewers"
      <div.dropdown-divider>
      <a.dropdown-item href="https://twitch.tv/finlaydag33k" target="_blank">
        <i.fab.fa-twitch> "Watch on Twitch"