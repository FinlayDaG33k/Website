// Obtain our Twitch data
document.addEventListener('DOMContentLoaded', async () => {
  // Initialize a new GraphQL class
  // Then add our query
  // Finally, execute it
  const resp = await new GraphQL('/graphql')
    .setQuery(`query {
        twitch {
          isLive
          game
          start
          viewers
        }
      }
    `)
    .execute();

  // Check if we have data
  if(!resp.data) return;

  // Check if we're live, if not, do not continue
  if(!resp.data.twitch.isLive) return;
  
  // Show the live icon
  document.getElementById('livestream-liveicon').classList.remove('d-none');

  // Initialize a new Handlebars loader
  const hbl = new HandlebarsLoader('/js/handlebars-templates', '.handlebars');

  // Get our Twitch-dropdown handlebars template
  // Compile it as well
  const templateResp = await hbl.load('twitch-dropdown');
  const template = Handlebars.compile(templateResp);

  // Update the dropdown
  document.getElementById('twitch-dropdown').innerHTML = template(resp.data.twitch);
});