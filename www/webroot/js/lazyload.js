// Initialize the lazyloader
var lazyLoadInstance = new LazyLoad({elements_selector: "img[data-src], img[data-srcset]"});

// Update our lazyloader after DOM is reay
document.addEventListener('DOMContentLoaded', async () => {
  lazyLoadInstance.update();
});