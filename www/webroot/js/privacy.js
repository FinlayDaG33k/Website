document.addEventListener('DOMContentLoaded', async () => {
  // Check if this is the first visit of the user
  // Show the privacy if so
  if(JSON.parse(Cookies.get("userSettings")).firstVisit) {
    new BSN.Modal(`#firstTimeVisitModal`, {backdrop: 'static', keyboard: false}).show();
  }

  // Handle updating of a privacy setting
  // Get a list of all checkboxes
  // Then add eventlisteners to all
  const checkboxes = document.querySelectorAll(`input[type='checkbox'][data-action='privacy-toggle']`);
  Array.from(checkboxes).forEach((checkbox) => {
    checkbox.addEventListener('change', (e) => {
      e.stopPropagation();

      // Load the settings
      var user_settings = JSON.parse(Cookies.get("userSettings"));

      // Change the data
      user_settings[checkbox.dataset.setting] = checkbox.checked;

      // Save the cookie
      Cookies.set("userSettings", JSON.stringify(user_settings));
    });
  });

  // Handle clicking of the "I'm done" button in the privacy modal
  document.querySelector(`#firstTimeVisitModal .modal-footer>.btn`)?.addEventListener('click', () => {
    var cookie_data = JSON.parse(Cookies.get("userSettings"));
    cookie_data.firstVisit = false;
    Cookies.set("userSettings", cookie_data);

    new BSN.Modal(`#firstTimeVisitModal`).hide();
  });
});
