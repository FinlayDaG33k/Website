document.addEventListener('DOMContentLoaded', async () => {
  // Handle submit form submissions
  const form = document.getElementById('search-form');
  form.addEventListener('submit', (e) => {
    e.preventDefault();
    const query = e.currentTarget[0].value;
    window.location.href = `/blog/search/${query}`;
  });
});