function scorePassword(pass) {
  var score = 0;
  if (!pass)
    return score;

  // award every unique letter until 5 repetitions
  var letters = new Object();
  for (var i=0; i<pass.length; i++) {
    letters[pass[i]] = (letters[pass[i]] || 0) + 1;
    score += 5.0 / letters[pass[i]];
  }

  // bonus points for mixing it up
  var variations = {
    digits: /\d/.test(pass),
    lower: /[a-z]/.test(pass),
    upper: /[A-Z]/.test(pass),
    nonWords: /\W/.test(pass)
  }

  variationCount = 0;
  for (var check in variations) {
    variationCount += (variations[check] == true) ? 1 : 0;
  }
  score += (variationCount - 1) * 10;

  return parseInt(score);
}

function passwordScoreMessage(score) {
  if (score < 10)
    return "Please enter a password";
  if (score >= 10)
    return "Are you even trying?";
  if (score >= 25)
    return "Could be better";
  if (score >= 50)
    return "It's getting there";
  if (score >= 60)
    return "That should do";
  if (score >= 100)
    return "You're going for this now ain't ya?";
  return "";
}