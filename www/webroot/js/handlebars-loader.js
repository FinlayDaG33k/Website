class HandlebarsLoader { 
  path;
  ext;
  handlebars = Handlebars;

  constructor(path = '/js/handlebars-templates', ext = '.handlebars') {
    this.path = path;
    this.ext = ext;
  }

  load(name) {
    return fetch(`${this.path}/${name}${this.ext}`, {
      method: 'GET',
      headers: {
        'Content-Type': 'text/plain',
        'Accept': 'text/plain'
      },
    })
    .then(resp => {
      if(!resp.ok) throw new Error();
      return resp.text();
    });
  }
}