// Listen for token registration submission
document.addEventListener('DOMContentLoaded', async () => {
  document.querySelector(`form[data-action="register-token"]`)?.addEventListener('submit', async (e) => {
    // Prevent page refresh
    e.preventDefault();

    // Get our form
    let form = document.querySelector(`form[data-action="register-token"]`);

    // Get the token name
    let tokenName = form.elements['token-name'].value;
    
    // Initialize our registration request
    const tokenRegistration = new TokenRegistration();

    // Register our token
    const result = await tokenRegistration.register('/graphql', tokenName);
    
    // Display our result in a toast
    switch(result.data.registerToken.success) {
      case true:
        notyf.success(result.data.registerToken.message);
        break;
      case false:
        notyf.error(result.data.registerToken.message);
        break;
    }
  });
});