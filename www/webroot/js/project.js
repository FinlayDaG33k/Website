document.addEventListener('DOMContentLoaded', () => {
  // Get all changelog buttons on the page
  const buttons = document.querySelectorAll(`[data-action="changelog"]`);

  // Add a function to disable all changelog buttons
  const disableButtons = () => {
    Array.from(buttons).forEach((button) => {
      button.classList.add('disabled');
    });
  }

  // Add a function to enable all changelog buttons
  const enableButtons = () => {
    Array.from(buttons).forEach((button) => {
      button.classList.remove('disabled');
    });
  }

  // Loop over each button to add an event handler
  Array.from(buttons).forEach((button) => {
    button.addEventListener('click', async (e) => {
      e.preventDefault();

      // Disable all changelog buttons
      disableButtons();

      // Initialize a new GraphQL query
      const gql = new GraphQL('/graphql');

      // Get our release and version
      const release = button.getAttribute('data-release');
      const version = button.getAttribute('data-version');

      // Add our query
      // TODO: Use actual GraphQL variables for this
      gql.setQuery(`query{project_release(id:${release}){changelog{change}}}`);

      // Execute our query
      const res = await gql.execute();

      // Show a toastr on error
      if(!res.data.project_release.changelog) return toastr.error('Could not load changelog', 'Whoops...');

      // Set some modal values
      document.querySelector('#changelogModal .modal-header .modal-title span').innerText = version;
      document.querySelector('#changelogModal .modal-body ul').innerHTML = "";
      let html = "";
      for await(let change of res.data.project_release.changelog) {
        html += `<li>${change.change}</li>`;
      }
      document.querySelector('#changelogModal .modal-body ul').innerHTML = html;

      // Display our modal
      new BSN.Modal(`#changelogModal`, {backdrop: 'static', keyboard: false}).show();

      // Re-enable buttons
      enableButtons();
    });
  });
});
