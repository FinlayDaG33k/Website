document.addEventListener('DOMContentLoaded', async () => {
  // Set some variables we need later
  var headerBorder = 7;
  var headerHeight = document.querySelector(`header`).offsetHeight;
  var currentState = false;
  let scrollTimer;
  const button = document.querySelector('a.scroll-up');

  // Add a local function to hide or show the topButton
  let topButton = function() {
    // check if we scrolled beyond the header
    if(window.scrollY >= (headerHeight + headerBorder) && currentState == false) {
      button.classList.add('visible');
      currentState = true;
      return;
    }

    // check if we scrolled back to the header minus the border (for debouncing)
    if(window.scrollY <= headerHeight && currentState == true) {
      button.classList.remove('visible');
      currentState = false;
      return;
    }
  }

  // Hide or show the top button on load
  topButton();

  // Hide or show the top button on scroll
  window.addEventListener('scroll', () => {
    clearTimeout('scrollTimer');
    scrollTimer = setTimeout(function() {
      topButton();
    }, 100);
  });
});