var notyf = new Notyf({
  position: {x: 'right', y: 'top'}
});

document.addEventListener('DOMContentLoaded', async () => {
  // Initialize all the Bootstrap components
  //$('[data-toggle="tooltip"]').tooltip();
  //$('#accordion.collapse').collapse();

  // Listen for clicks on data-toggles
  // Block them from unfolding card-headers
  const switches = document.querySelectorAll('.card .card-header label.switch');
  Array.from(switches).forEach((switcher) => {
    switcher.addEventListener('click', (e) => {
      e.stopPropagation();
    });
  });
});