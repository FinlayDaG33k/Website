Handlebars.registerHelper('nl2br', function(text) {
  text = Handlebars.Utils.escapeExpression(text);
  var nl2br = (text + '').replace(/([^>\r\n]?)(\r\n|\n\r|\r|\n)/g, '$1' + '<br>' + '$2');
  return new Handlebars.SafeString(nl2br);
});

Handlebars.registerHelper('momentFormat', function(date, options) {
  return new Handlebars.SafeString(
    dayjs(date).format(options.hash.format)
  );
});

Handlebars.registerHelper('momentExpired', function(date, options) {
  const diff = dayjs().diff(date);
  const formatted = dayjs(diff).format(options.hash.format);
  
  return new Handlebars.SafeString(formatted);
});

Handlebars.registerHelper('switch', function(value, options) {
  this.switch_value = value;
  this.switch_break = false;
  return options.fn(this);
});

Handlebars.registerHelper('case', function(value, options) {
  if (value == this.switch_value) {
    this.switch_break = true;
    return options.fn(this);
  }
});

Handlebars.registerHelper('default', function(value, options) {
  if (this.switch_break == false) {
    return value;
  }
});

Handlebars.registerHelper('checkProjectThumbnail', function(value) {
  if(!value) return false;
  if(value === "") return false;
  if(value === "#") return false;
  return true;
});