document.addEventListener('DOMContentLoaded', async () => {
  // Find our cards div
  const cardsDiv = document.getElementById('projects-cards');

  // Check if our DOM has our cards div
  // If not, exit here
  if(cardsDiv === null) return;

  // Initialize a new GraphQL query
  const gql = new GraphQL('/graphql');

  // Add our query
  gql.setQuery(`query($limit: Int!) {
    projects(limit: $limit) {
      id
      name
      description
      thumbnail
    }
  }`);

  // Set the limit to 3
  gql.addVariable('limit', 3);

  // Execute the query
  const res = await gql.execute();

  // Check whether we the required data
  if(!res) return;
  if(!res.data) return;
  if(!res.data.projects) return;

  // Load our handlebars template
  const templateResp = await new HandlebarsLoader('/js/handlebars-templates', '.handlebars').load('project-card');
  const template = Handlebars.compile(templateResp);

  // Loop over each project to add it
  for(let project of res.data.projects) {
    cardsDiv.innerHTML += template(project);
  }

  // Update our Lazyloader
  lazyLoadInstance.update();
});