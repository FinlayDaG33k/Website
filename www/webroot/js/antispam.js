document.addEventListener('DOMContentLoaded', async () => {
  window.antiSpam = new antispam.AntiSpam();
  document.querySelector('button[antispam]')?.addEventListener('click', async (e) => {
    // Prevent submission of the form
    e.preventDefault();
    
    // Get our button
    const button = e.currentTarget;

    // Get the amount of tokens we have
    const tokenCount = window.antiSpam.countTokens();
    if(tokenCount == 0) {
      // We've ran out of tokens!
      // Get the Csrf from the form
      const form = button.closest(`form`);
      const field = form.querySelector(`input[name="_csrfToken"]`);
      const csrf = field.value;
      
      // Set it in the class
      window.antiSpam.csrf = csrf;

      // Solve a challenge
      button.innerHTML = `Solving Challenges...`;
      await window.antiSpam.solve();
      button.innerHTML = `Done solving!`;
    }

    // Get a token and add it to the form
    let token = window.antiSpam.getTokens(1);
    let field = document.createElement('input');
    field.type = 'hidden';
    field.name = 'antispam-token';
    field.value = JSON.stringify(token[0]);
    button.form.appendChild(field);

    // Remove the token from our local storage
    await window.antiSpam.token.remove(token[0]);

    // Submit our form
    button.form.submit();
  })
});