// Listen for login form submission
document.addEventListener('DOMContentLoaded', async () => {
  let loginMethod;
  let redir;
  let username;
  const form = document.querySelector(`form[data-action="user-login"]`);

  // Check if a form was found
  // If not, exit here
  if(form === null) return;

  // Get our redir location
  // TODO: Actually implement this
  redir = '';
  if(!redir || redir == '') redir = '/my-account';

  // Get a list of all button
  // Then add eventlisteners to all
  const buttons = document.querySelectorAll(`form[data-action="user-login"] button[data-method]`);
  Array.from(buttons).forEach((button) => {
    button.addEventListener('click', async (e) => {
      e.preventDefault();
  
      // Get the method we want to use
      loginMethod = e.currentTarget.dataset.method;
  
      // Get our username
      username = form.elements.username.value;

      // Invoke the method we need for login
      let resp;
      switch(loginMethod) {
        case 'token':
          const tokenLogin = new TokenLogin();
          resp = await tokenLogin.login('/graphql', username);  
          if(!resp.data.loginToken.success) {
            notyf.error(resp.data.loginToken.message);
            break;
          }
          notyf.success(resp.data.loginToken.message);
          window.location.href = redir;
          break;
        case 'email':
          const emailLogin = new EmailLogin();
          resp = await emailLogin.getToken('/graphql', username);
          if(!resp.data.emailLogin.success) {
            notyf.error(resp.data.emailLogin.message);
            break;
          }
          notyf.success(resp.data.emailLogin.message);
          form.classList.add('d-none');
          document.querySelector(`form[data-action="email-login"]`).classList.remove('d-none');
          break;
      }
    });
  });

  // Email OTP submission
  document.querySelector(`form[data-action="email-login"]`)?.addEventListener('submit', async (e) => {
    // Prevent page refresh
    e.preventDefault();

    // Get our code
    let code = e.currentTarget.elements.code.value;
    console.log(code);

    // Check the code and login
    const emailLogin = new EmailLogin();
    let resp = await emailLogin.login('/graphql', username, code);

    // Check if authentication was a success
    // If not, show a toast
    if(!resp.data.emailLogin.success) {
      notyf.error(resp.data.emailLogin.message);
      return;
    }

    // Redirect the user
    notyf.success(resp.data.emailLogin.message);
    window.location.href = redir;
  });

  // Handle login cancelation (email)
  document.querySelector(`form[data-action="email-login"] button[type="reset"]`)?.addEventListener('click', () => {
    document.querySelector(`form[data-action="user-login"]`).classList.remove('d-none');
    document.querySelector(`form[data-action="email-login"]`).classList.add('d-none');
  });
});