var gulp = require('gulp');
var sass = require('gulp-sass')(require('node-sass'));
var sourcemaps = require('gulp-sourcemaps');
var rename = require("gulp-rename");
var path = require("path");
var gulpBrotli = require('gulp-brotli')
var zlib = require('zlib');
const purgecss = require('gulp-purgecss')

var minifyOutput = true; // set to false to disable output compression
var treeshakeOutput = true; // set to false to disable output treeshaking
var compressFiles = [
  '/www/**/webroot/**/**/*.*',
  '!/www/**/webroot/**/**/*.br',
  '!/www/**/webroot/{scss,sass}/*',
  '!/www/**/webroot/index.php',
  '!/www/**.woff2'
];

// Create a task to compile our SASS-styles
gulp.task('compile', function() {
  // Create a watch for sass/**/*.scss (any scss file in sass/*)
  let task = gulp.src('/www/**/webroot/scss/**/*.{scss,sass}', {base : '/'});

  // Define the output style
  // Log errors
  task = task.pipe(
    sass({outputStyle: (minifyOutput == true ? 'compressed' : 'expanded')})
      .on('error', sass.logError)
  );

  // Check if we're minifying output
  // If so, also treeshake the CSS
  if(treeshakeOutput) {
    task = task.pipe(purgecss({
      content: [
        '/www/**/webroot/js/**/*.handlebars',
        '/www/**/src/**/*.ctp',
        '/www/**/webroot/js/**/*.js',
        '/www/**/src/**/.php',
        '/www/config/paginator-templates.php'
      ]
    }))
  }
    
  // The output destination for our compiled *.scss file
  task = task.pipe(gulp.dest(function(file){
    let rel = path.relative(file.path, file.path.replace('/scss/','/css/'));
    let abs = path.join(path.dirname(file.path),'/webroot',rel);
    file.path = "/";
    return abs;
  }));
  
  return task;
});

// Create a task to compress all our assets
gulp.task('compress', function() {
  return gulp.src(compressFiles, {base : '/'})
    .pipe(gulpBrotli.compress({
      extension: 'br',
      skipLarger: true,
      // the options are documented at https://nodejs.org/docs/latest-v10.x/api/zlib.html#zlib_class_brotlioptions 
      params: {
        // brotli parameters are documented at https://nodejs.org/docs/latest-v10.x/api/zlib.html#zlib_brotli_constants
        [zlib.constants.BROTLI_PARAM_QUALITY]: zlib.constants.BROTLI_MAX_QUALITY,
      },
    }))
    .pipe(rename(function (path) {
      // Updates the object in-place
      path.extname = `${path.extname}.br`;
    }))
    .pipe(gulp.dest(function(file){
      return '/';
    }));
})

/**
 * Create our default task
 * - Compile the sources on start
 * - Watch for changes
 */
gulp.task('default',gulp.series('compile'), function() {
  // Watch the sass/ folder for changes
  gulp.watch('/www/**/webroot/scss/**/*.{scss,sass}',['compile']);
});
