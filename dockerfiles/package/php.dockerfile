ARG BASE_IMAGE=registry.gitlab.com/finlaydag33k/website/base/php:latest
FROM $BASE_IMAGE

# Add the PHP configs
COPY ./config/php/opcache.ini $PHP_INI_DIR/conf.d/

# Add the app
COPY ./www /var/www

# Copy the app config
COPY ./www/config/app.default.php /var/www/config/app.php

# Add the init script and set it as the command
COPY ./config/php/init.sh /usr/local/bin/init.sh
CMD ["/bin/sh", "/usr/local/bin/init.sh"]