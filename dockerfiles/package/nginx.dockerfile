ARG BASE_IMAGE=registry.gitlab.com/finlaydag33k/website/base/nginx:latest
FROM $BASE_IMAGE


# Add the config
COPY ./config/nginx/default.conf /etc/nginx/conf.d/default.conf
COPY ./config/nginx/nginx.conf /etc/nginx/nginx.conf
COPY ./config/nginx/settings.conf /etc/nginx/settings.conf

# Add the app
COPY ./www /var/www