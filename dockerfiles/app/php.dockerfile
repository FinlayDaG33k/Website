ARG PHP_VERSION=7.4.14
FROM php:${PHP_VERSION}-fpm-buster
ARG DEBUG=false

# Update the apt
RUN apt-get update

# Add intl
RUN apt-get install -y \
libicu-dev
RUN docker-php-ext-configure intl
RUN docker-php-ext-install intl

# Add PDO
RUN docker-php-ext-install pdo pdo_mysql

# Add ImageMagick
RUN apt-get install -y \
    libmagickwand-dev --no-install-recommends \
    && pecl install imagick \
	&& docker-php-ext-enable imagick

# Add APCu
RUN pecl install apcu && docker-php-ext-enable apcu

# Add OPcache
RUN docker-php-ext-install opcache

# Add bcmath
RUN docker-php-ext-install bcmath

# Add XDebug if needed
RUN if [ "$DEBUG" = "true" ] ; then pecl install xdebug && docker-php-ext-enable xdebug ; else echo 'Skipping XDebug install' ; fi
