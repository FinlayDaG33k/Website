FROM mhart/alpine-node:16

WORKDIR /app
RUN apk add --no-cache make gcc g++ python3
RUN npm install --unsafe-perm -g node-sass gulp gulp-sass gulp-sourcemaps gulp-rename gulp-brotli gulp-purgecss
RUN npm link gulp@3.9.1 gulp-sass gulp-sourcemaps gulp-rename gulp-brotli gulp-purgecss node-sass
RUN touch /app/Gulpfile.js

CMD ["gulp"]
